package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

public class Login extends BasePage {

    public Login(WebDriver driver) {
        super(driver);
    }

    private By gdprHeader = By.cssSelector(".gdpr-notice p");
    private By loginHeader = By.xpath("//h2[text()='Sign in using your Archant account...']");
    private By loginForm = By.cssSelector(".login-and-register");
    private By janrainFacebookButton = By.cssSelector("#janrain-facebook");
    private By janrainGoogleButton = By.cssSelector("#janrain-googleplus");
    private By janrainYahooButton = By.cssSelector("#janrain-yahoo");
    private By janrainMicrosoftButton = By.cssSelector("#janrain-microsoftaccount");
    private By emailLabel = By.cssSelector("label[for=frm-login-email-address]");
    private By emailEntry = By.id("frm-login-email-address");
    private By passwordLabel = By.cssSelector("label[for=frm-login-password]");
    private By passwordEntry = By.id("frm-login-password");
    private By loginButton = By.cssSelector(".col-sm-offset-2.col-sm-10 button");
    private By janrainHeader = By.cssSelector("#janrainView div div");

    public void waitForLoginForm() {
        waitForElement(loginForm);
    }

    public String getGDPRHeader() {
        return getText(gdprHeader);
    }

    public Boolean checkGDPRHeader() {
        try {
            isDisplayed(gdprHeader);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("GDPR heading not visible");
            return false;
        }
    }

    public String getLoginHeader() {
        return getText(loginHeader);
    }

    public Boolean checkLoginHeaderIsDisplayed() {
        try {
            isDisplayed(loginHeader);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Login header not visible");
            return false;
        }
    }

    public String getEmailLabel() {
        return getText(emailLabel);
    }

    public Boolean checkEmailLabelIsDisplayed() {
        try {
            isDisplayed(emailLabel);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Email label not visible");
            return false;
        }
    }

    public Boolean checkEmailEntryIsDisplayed() {
        try {
            isDisplayed(emailEntry);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Email entry not visible");
            return false;
        }
    }

    public String getPasswordLabel() {
        return getText(passwordLabel);
    }

    public Boolean checkPasswordLabelIsDisplayed() {
        try {
            isDisplayed(passwordLabel);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Password label not visible");
            return false;
        }
    }

    public Boolean checkPasswordEntryIsDisplayed() {
        try {
            isDisplayed(passwordEntry);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Password entry not visible");
            return false;
        }
    }

    public String getLoginButton() {
        return getText(loginButton);
    }

    public Boolean checkLoginButtonIsDisplayed() {
        try {
            isDisplayed(loginButton);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Login button not visible");
            return false;
        }
    }


    public String getJanrainHeader() {
        return getText(janrainHeader);
    }

    public Boolean checkJanrainFacebookButtonIsDisplayed() {
        try {
            isDisplayed(janrainFacebookButton);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Janrain Facebook button not visible");
            return false;
        }
    }

    public Boolean checkJanrainGoogleButtonIsDisplayed() {
        try {
            isDisplayed(janrainGoogleButton);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Janrain Google button not visible");
            return false;
        }
    }

    public Boolean checkJanrainYahooButtonIsDisplayed() {
        try {
            isDisplayed(janrainYahooButton);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Janrain Yahoo button not visible");
            return false;
        }
    }

    public Boolean checkJanrainMicrosoftIsDisplayed() {
        try {
            isDisplayed(janrainMicrosoftButton);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Janrain Microsoft button not visible");
            return false;
        }
    }

    public Boolean checkJanrainHeaderIsDisplayed() {
        try {
            isDisplayed(janrainHeader);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Janrain header not visible");
            return false;
        }
    }
}
