package pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.By;

import java.util.List;

public class HotTopics extends BasePage {

    public HotTopics(WebDriver driver) {
        super(driver);
    }

    private By header = By.cssSelector(".content-a.col-md-8 div div h1");
    private List<WebElement> articleQueue = driver.findElements(By.cssSelector("story-list "));
    private By hotTopicsContent = By.xpath("//div[@class='story-list ']");
    private By hotTopicsLinks = By.cssSelector(".results-gt-inner-2.lp_toplinks span a");


    //public WebElement hotTopicsHeaderText = driver.findElement(By.cssSelector(".teaser-title h2 a"));
    //public WebElement hotTopicsImage = driver.findElement(By.cssSelector(".teaser-image div img"));
    //public WebElement hotTopicsSummary = driver.findElement(By.cssSelector(".teaser-content p:nth-child(2)"));
    public By hotTopicsQueueElement = By.cssSelector(".medium2col-teaser-item.teaser.clearfix");


    public Boolean checkHeaderIsDisplayed() {
        try {
            waitForElement(header);
            isDisplayed(header);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Hot Topics header not visible");
            return false;
        }
    }

    public String getHeader() {
        return getText(header).toLowerCase();
    }

    public Boolean checkHotTopicQueueHeaders() {
        try {
            for (WebElement articleHeader : articleQueue) {
                articleHeader = driver.findElement(By.cssSelector(".teaser-title h2 a"));
                articleHeader.isDisplayed();
            }
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Author queue headers not visible");
        }
        return false;
    }

    public Boolean checkHotTopicQueueImages() {
        try {
            for (WebElement image : articleQueue) {
                image = driver.findElement(By.cssSelector(".teaser-image div img"));
                image.isDisplayed();
            }
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Author queue images not visible");
        }
        return false;
    }

    public Boolean checkHotTopicQueueSummaries() {
        try {
            for (WebElement summary : articleQueue) {
                summary = driver.findElement(By.cssSelector(".teaser-title h2 a"));
                summary.isDisplayed();
            }
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Author queue summary not visible");
        }
        return false;
    }
}


//    public static int topicTagCount;
//    public static int loopCount = 3;
//
//    public static By hotTopicsBar = By.xpath("//div[contains(@class, 'results-gt-2')]");
//    public static By hotTopicsBarTop = By.xpath("//div[contains(@class, 'results-gt-2')]");
//    public static By hotTopicsText = By.xpath("//span[contains(@class, 'boldred')]");
//    public static By header = By.xpath("//div[contains(@class, 'results-gt-2')]/div/span[contains(@class, 'boldred')]");
//    public static By hotTopicsLinks = By.xpath("//div[contains(@class, 'results-gt-2')]/div/span");
//    public static By hotTopicsLinks_First = By.xpath("//div[contains(@class, 'results-gt-2')]/div/span[2]/a");
//    public static By topicsPageHeader = By.xpath("//h1[contains (@class, 'inpage')]");
//    public static By hotTopicsHeaderText_First = By.xpath(".//div[contains(@class, 'teaser-title')]/h2/a" + "[1]");
//    public static By hotTopicsImage = By.xpath(".//div[contains(@class, 'teaser-image')]/a");
//    public static By hotTopicsImage_First = By.xpath(".//div[contains(@class, 'teaser-image')]/a" + "[1]");
//    public static By hotTopicsSummary = By.xpath(".//div[contains(@class, 'teaser-content')]/p[2]");
//
//    public static List<WebElement> hotTopicsArticleQueue_Images = driver.findElements(By.xpath("//div[contains(@class, 'teaser-image')]/a"));
//
//    public static List<By> hotTopicsArticleQueue_Header() {
//        return null;
//    }
//
//    public static List<WebElement> articleQueue = driver.findElements(By.xpath("//div[contains(@class, 'medium2col-teaser-item teaser clearfix')]/div/h2"));
//
//    public static List<WebElement> hotTopicsLinksNew = driver.findElements(By.xpath("//div[contains(@class, 'results-gt-inner-2 lp_toplinks')]/span/a"));
//
//    public By sportNavElement = By.xpath("//div[contains(@class, 'navbar-primary-container')]/ul/li[3]/a");
//
//    public By sendUsASportStoryElement = By.xpath("//a[contains(@class, 'sport-story')]");
//
//    public static List<WebElement> listOfTopicTags = driver.findElements(By.xpath("//li[contains (@class, 'article-tag')]/a"));
//
//    public static By topicTags_First = By.xpath("//li[contains (@class, 'article-tag')]/a[1]");
//
//    public static void checkHotTopicsLinks(List<WebElement> links) throws InterruptedException {
//        for (int i = 0; i < links.size(); i++) {
//
//            links = hotTopicsLinksNew;
//            //waitForClickableElement(hotTopicsBarTop);
//            mainWindow = driver.getWindowHandle();
//
//            listOfHotTopics = links.get(i).getText();
//            listOfHotTopics = listOfHotTopics.toLowerCase();
//
//            clickList(links.get(i));
//
//            ArrayList tabs = new ArrayList(driver.getWindowHandles());
//
//            if (tabs.size() > 1) {
//                waitForElement(genericSelector());
//                driver.switchTo().window(String.valueOf(tabs.get(1)));
//                driver.close();
//                driver.switchTo().window(mainWindow);
//            } else {
//                Thread.sleep(3000);
//                waitForElement(register.registerWebElement);
//                By topicsHeader = topics.topicsPageHeader;
//                waitForElement(topicsHeader);
//            }
//        }
//    }
//
//
////    public static void checkHotTopicsArticleQueueResponsiveness(List<By> links) {
////        for (int i = 0; i < links.size(); i++) {
////            waitForClickableElement(hotTopicsBarTop();
////            links = hotTopicsLinksNew();
////            mainWindow = driver.getWindowHandle();
////            click(links.get(i);
////
////            ArrayList tabs = new ArrayList(driver.getWindowHandles();
////
////            if (tabs.size() == 1) {
////                waitForElement(topics.topicsPageHeader();
////                checkHotTopicsContents();
////                minimizeWindow();
////                checkHotTopicsContents();
////                maximizeWindow();
////
////                break;
////            } else {
////                navigateBack();
////            }
////        }
////    }
//
////    public static void checkHotTopicsArticleQueue(List<WebElement> links) {
////        for (int i = 0; i < links.size(); i++) {
////            waitForClickableElement(hotTopicsBarTop();
////            links = hotTopicsLinksNew();
////            mainWindow = driver.getWindowHandle();
////            click(links.get(i);
////
////            ArrayList tabs = new ArrayList(driver.getWindowHandles();
////
////            if (tabs.size() == 1) {
////
////                waitForElement(article.articleContent();
////
////                // TODO: seperate the below into one for loop with a counter variable
////
////                for (By header : articleQueue()) {
////                    header = hotTopicsHeaderText();
////                    scrollToElement(header);
////                    assertTrue(header.isDisplayed();
////                }
////
////                for (By image : articleQueue()) {
////                    image = hotTopicsImage();
////                    scrollToElement(image);
////                    assertTrue(image.isDisplayed();
////                }
////
////                for (By summary : articleQueue()) {
////                    summary = hotTopicsSummary();
////                    scrollToElement(summary);
////                    assertTrue(summary.isDisplayed();
////                }
////
////                break;
////            } else {
////                navigateBack();
////            }
////        }
////    }
//
//    public static void checkHotTopicsAreDisplayed() {
//
//        try {
//            //isDisplayed(topics.hotTopicsText);
//            //isDisplayed(topics.hotTopicsBar);
//            //isDisplayed(topics.header);
//            //isDisplayed(topics.hotTopicsLinks);
//        } catch (NoSuchElementException e) {
//            System.out.println("Hot Topics are not visible, when they should be");
//        }
//    }
//
//    public static void checkHotTopicsClickFunctionality(List<WebElement> links) {
//    loopCount = 3;
//        for (int i = 0, j = i + 1; i < loopCount; i++) {
//            //waitForClickableElement(hotTopicsBarTop();
//            List<WebElement> topicsLinks = hotTopicsLinksNew;
//            mainWindow = driver.getWindowHandle();
//            clickList(topicsLinks.get(i));
//
//            //waitForClickableElement(register.registerWebElement);
//
//            links = driver.findElements(By.xpath("//div[contains(@class, 'teaser-image')]/a"));
//            clickList(links.get(j));
//
//            waitForElement(article.articleContent);
//
//            navigateBack();
//
//        }
//    }
//
//    public static void checkHotTopicsClickFunctionality_Header(List<By> links) {
//        click(hotTopicsLinks_First);
//
//        int loopCount = 3;
//        for (int i = 0; i < loopCount; i++) {
//
//            waitForClickableElement(hotTopicsBarTop);
//            links = topics.hotTopicsArticleQueue_Header();
//
//            click(links.get(i));
//
//            waitForElement(article.articleContent);
//
//            navigateBack();
//
//        }
//    }
//
//    public static void checkingTopicTags(List<By> links) {
//        click(topics.sportNavElement);
//
//        waitForElement(register.registerWebElement);
//
//        int loopCount = 10;
//        for (int i = 0; i < loopCount; i++) {
//
//            waitForClickableElement(hotTopicsBarTop);
//            links = topics.hotTopicsArticleQueue_Header();
//
//            click(links.get(i));
//
//            waitForElement(article.articleContent);
//
//
//            try {
//                // see if topic tags visible
//                //isDisplayed(topicTags_First);
//                scrollToElement(topicTags_First);
//
//                topicTagSummary = driver.findElement(topicTags_First).getText();
//                topicTagSummary = topicTagSummary.toLowerCase();
//
//                click(topicTags_First);
//
//                waitForElement(topicsPageHeader);
//
//                int queueLoopCount = 3;
//                for (int j = 0; j < queueLoopCount; i++) {
//                    // click on a article at random
//                    int i1 = randomNumber(articleQueue.size());
//                    clickList(articleQueue.get(i));
//
//                    scrollToElement(topicTags_First);
//
//                    if (listOfTopicTags.size() == 1) {
//                        topicTagDetail = driver.findElement(topicTags_First).getText();
//                        topicTagDetail = topicTagDetail.toLowerCase();
//
//                        assertEquals(topicTagSummary, topicTagDetail);
//
//                    } else {
//                        List<String> listOfTopicTags = new ArrayList<String>();
//
//                        for (int k = 0; k < listOfTopicTags.size(); k++) {
//                            listOfTopicTags.add(listOfTopicTags.get(k).getText());
//                            topicTagDetail = listOfTopicTags.get(k).getText();
//                            topicTagDetail = topicTagDetail.toLowerCase();
//
//                            try {
//                                assertEquals(topicTagSummary,topicTagDetail);
//                                topicTagCount++;
//                                if (topicTagCount == 3) {
//                                    break;
//                                }
//                            } catch (NoSuchElementException e) {
//                                continue;
//                            }
//                        }
//                    }
//                }
//            } catch (NoSuchElementException e) {
//                navigateBack();
//                continue;
//            }
//        }
//    }
//
//    public static void checkingTopicTagsQueueMatch(List<By> links) {
//
//        waitForElement(register.registerWebElement);
//
//        click(topics.sportNavElement);
//
//        waitForElement(register.registerWebElement);
//
//        int loopCount = 10;
//        for (int i = 0; i < loopCount; i++) {
//
//            waitForClickableElement(hotTopicsBarTop);
//            links = topics.hotTopicsArticleQueue_Header();
//
//            click(links.get(i));
//
//            waitForElement(article.articleContent);
//
//            try {
//
//                //isDisplayed(topicTags_First);
//                scrollToElement(topicTags_First);
//
//                click(topicTags_First);
//
//                waitForElement(topicsPageHeader);
//                topicTagSummary = driver.findElement(topics.topicsPageHeader).getText();
//                topicTagSummary = topicTagSummary.toLowerCase();
//
//                int queueLoopCount = 3;
//                for (int j = 0; j < queueLoopCount; i++) {
//                    // click on a article at random
//                    int i1 = randomNumber(articleQueue.size());
//                    clickList(articleQueue.get(i1));
//
//                    scrollToElement(topicTags_First);
//                    if (listOfTopicTags.size() == 1) {
//                        topicTagDetail = driver.findElement(topicTags_First).getText();
//                        topicTagDetail = topicTagDetail.toLowerCase();
//
//                        assertEquals(topicTagSummary,topicTagDetail);
//
//                    } else {
//                        List<String> listOfTopicTags = new ArrayList<String>();
//
//                        for (int k = 0; k < listOfTopicTags.size(); k++) {
//                            listOfTopicTags.add(listOfTopicTags.get(k).getText());
//                            topicTagDetail = listOfTopicTags.get(k).getText();
//                            topicTagDetail = topicTagDetail.toLowerCase();
//
//                            try {
//                                assertEquals(topicTagSummary, topicTagDetail);
//                                topicTagCount++;
//                                if (topicTagCount == 3) {
//                                    break;
//                                }
//                            } catch (NoSuchElementException e) {
//                                continue;
//                            }
//                        }
//                    }
//
//                    topicTagCount++;
//                    navigateBack();
//
//                }
//
//            } catch (NoSuchElementException e) {
//                navigateBack();
//                continue;
//            }
//        }
//    }
//}
//
//
