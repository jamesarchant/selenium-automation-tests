package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.*;

public class Search extends BasePage {

    public Search(WebDriver driver) {
        super(driver);
    }

    private By resultsMainSection = By.cssSelector(".search-results.inner-a.col-md-8");
    private By resultsEntry = By.id("frm-refine-search-term");
    private By resultsButton = By.name("resultsSearch");
    private By refineYourSearchSection = By.cssSelector(".search-filter-date");
    private By sortByText = By.xpath("//div[@class='search-results inner-a col-md-8']/p");
    private By firstPageSelector = By.xpath("(//div[@class='pagination-wrapper'])[1]");
    private By secondPageSelector = By.xpath("(//div[@class='pagination-wrapper'])[2]");
    private By firstPageSelectorNumber = By.xpath("(//ul[@class='pagination'])[1]/li[2]/a");
    private By secondPageSelectorNumber = By.xpath("(//ul[@class='pagination'])[2]/li[2]/a");
    private By advancedSearchText = By.xpath("//p[contains(@class, 'supporting-link')]/a");
    private By searchResults = By.cssSelector(".list-group li");
    private int searchResultList = driver.findElements(searchResults).size();
    private By searchHeader = By.cssSelector(".teaser-title");
    private By searchDate = By.cssSelector(".teaser-date");
    private By searchImage = By.cssSelector(".teaser-image");
    private By searchSummaryText = By.cssSelector(".teaser-post-extra ");
    private By sortByDateLink = By.xpath("//div[@class='search-results inner-a col-md-8']/p/a[1]");
    private By sortByMostRelevantLink = By.xpath("//div[@class='search-results inner-a col-md-8']/p/a[2]");
    private By filterFromDate = By.id("filterFromDate");
    private By filterToDate = By.id("filterToDate");
    private By searchButton = By.cssSelector(".absolute-date-filter.clearfix button");
    private By searchShowMoreLink = By.cssSelector(".search-show-more-categories-button");
    private By previousLink = By.xpath("(//ul[@class='pagination'])[1]/li[1]/a");
    private By nextLink = By.xpath("(//ul[@class='pagination'])[1]/li[7]/a");
    private By pageNumbers = By.xpath("(//ul[@class='pagination'])[1]/li");
    public List<WebElement> pageNumbersList = driver.findElements(pageNumbers);
    public List<WebElement> categoryFilters = driver.findElements(By.cssSelector(".search-filter-section li a"));
    public static final String[] PAGE_NUMBERS = new String[]{"1", "2", "3"};
    public static final int[] AMOUNT_OF_DAYS = new int[]{0, 7, 30, 365};

    public Search verifyPageNumbers() {
        try {
            isDisplayed(firstPageSelector);
            scrollToElement(secondPageSelector);
            isDisplayed(secondPageSelector);
        } catch (NoSuchElementException e) {
            System.out.println("Page numbers did not appear as expected");
        }
        return this;
    }

    public Search verifyCurrentPageHighlighted() {
        assertEquals(driver.findElement(firstPageSelectorNumber).getText(), "1");
        scrollToElement(secondPageSelector);
        assertEquals(driver.findElement(secondPageSelectorNumber).getText(), "1");
        return this;
    }

    public Search checkSearchResultAsset(By locator) {
        for (WebElement searchElement : driver.findElements(searchResults)) {
            searchElement = driver.findElement(locator);
            try {
                searchElement.isDisplayed();
            } catch (NoSuchElementException e) {
                System.out.println(searchElement.getText() + "was not displayed as expected");
            }
        }
        return this;
    }

    public Search searchContents() {
        isDisplayed(resultsMainSection);
        isDisplayed(resultsEntry);
        isDisplayed(resultsButton);
        isDisplayed(refineYourSearchSection);
        isDisplayed(sortByText);
        isDisplayed(firstPageSelector);
        isDisplayed(secondPageSelector);
        return this;
    }

    public List<WebElement> dateFilters = driver.findElements(By.cssSelector(".relative-date-filter ul li a"));

    public Search clickDateFilters() {
        waitForElement(refineYourSearchSection);
        int dateFilterSize = driver.findElements(By.cssSelector(".relative-date-filter ul li a")).size();
        for (int i = 0; i < dateFilterSize; i++) {
            WebElement date = driver.findElements(By.cssSelector(".relative-date-filter ul li a")).get(i);
            scrollToElement(refineYourSearchSection);
            date.click();
            waitForElement(resultsMainSection);
            checkDatesOnPage(AMOUNT_OF_DAYS[i]);
            navigateBack();
        }
        return this;
    }

    public void checkDatesOnPage(int numberOfDays) {
        List<WebElement> allDates = driver.findElements(searchDate);
        for (WebElement date : allDates) {
            String dateText = date.getText();
            Boolean fallsBetween = checkArticleDateFallsBetween(numberOfDays, dateText);
            assertTrue(fallsBetween);
        }
    }

    private Boolean checkArticleDateFallsBetween(int numberOfDays, String dateText) {
        Date articleDate = null;
        // convert passed in dateText into date
        try {
            articleDate = new SimpleDateFormat("MMMM" + " " + "dd" + ", " + "yyyy", Locale.UK).parse(dateText);
        } catch (ParseException e) {
            System.out.println("Date was invalid");
            return false;
        }

        // get todays date and minus the days
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -numberOfDays);
        Date minusDays = calendar.getTime();
        // check that article date falls between number of days selected
        if (numberOfDays == 365) {
            Date today = new Date();
            int startOfYear = today.getYear();
            int articleYear = articleDate.getYear();
            return startOfYear == articleYear;
        }

        return articleDate.after(minusDays);
    }

    public String todaysDate;

    public void getDates() {
        DateFormat dateFormat = new SimpleDateFormat("dd" + "/" + "MM" + "/" + "yyyy");
        Date date = new Date();
        todaysDate = dateFormat.format(date);
    }

    public String oneMonthAgoDate;
    public int thirtyDays;

    public void getDates30Days() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -thirtyDays);
        Date minusDays = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd" + "/" + "MM" + "/" + "yyyy");
        oneMonthAgoDate = dateFormat.format(minusDays);
    }

    public Search enterDate(By locator, String text) {
        enterText(locator, text);
        return this;
    }

    public Search clickSearchButton() {
        click(searchButton);
        waitForElement(resultsMainSection);
        return this;
    }

    public Search clickCategoryOptions() {
        for (int i = 1; i < 5; i++) {
            categoryFilters = driver.findElements(By.cssSelector(".search-filter-section li a"));
            click(searchShowMoreLink);
            int j = randomNumber(categoryFilters.size());
            WebElement category = categoryFilters.get(j);
            scrollToElement(refineYourSearchSection);
            category.click();
            waitForElement(resultsMainSection);
            navigateBack();
        }
        return this;
    }

    public void clickMostRelevantLink(By locator) {
        waitForElement(locator);
        scrollToElement(locator);
        click(locator);
        // TODO: replace below with resultsMainSection, had to seperate this out because of the stale
        waitForElementList(driver.findElement(By.cssSelector(".search-results.inner-a.col-md-8")));
        navigateBack();
    }

    public ArrayList<By> pageNumberLinks() {
        ArrayList<By> lists = new ArrayList<By>();
        By pageNumberOne = By.xpath("(//ul[@class='pagination'])[1]/li[2]/a");
        By pageNumberTwo = By.xpath("(//ul[@class='pagination'])[1]/li[3]/a");
        By pageNumberThree = By.xpath("(//ul[@class='pagination'])[1]/li[4]/a");
        lists.add(pageNumberOne);
        lists.add(pageNumberTwo);
        lists.add(pageNumberThree);
        return lists;
    }

    public void clickPageNumbers() {
        for (int i = 0; i < pageNumberLinks().size(); i++) {
            By pageNumber = pageNumberLinks().get(i);
            assertEquals(driver.findElement(pageNumber).getText(), PAGE_NUMBERS[i]);
            scrollToElement(pageNumber);
            click(pageNumber);
            waitForElement(resultsMainSection);
            assertEquals(driver.findElement(pageNumber).getText(), PAGE_NUMBERS[i]);
        }
    }

    public void clickPreviousNextLinks(By locator) {
        click(nextLink);
    }

//    int todaysDate = 1;

//    public List<WebElement> listOfElements = driver.findElements(By.xpath("//ul[@class='list-group']/li"));
//    public List<WebElement> listOfHeaders = driver.findElements(By.xpath("//div[@class='teaser-title ']"));
//    public String searchTerm_MisspeltWord = "rtgkrgokgasoregokeaekwew";
//
//    public By articleSummaryElement = By.xpath("//li[contains(@class, 'hit clearfix')][1]/div/div/h4/a");
//    public By articleSummaryElementWeb = articleSummaryElement;
//    public String articleSummaryElementText =  driver.findElement(By.xpath("//li[contains(@class, 'hit clearfix')][1]/div/div/h4/a")).getText();
//    public By articleDetailElement = By.xpath("//*[@id='article-content']/h1");
//    public String articleDetailElementText = driver.findElement(By.xpath("//*[@id='article-content']/h1")).getText();
//
//    public String firstPageNumber = driver.findElement(By.xpath("(//div[@class='pagination-wrapper'])[1]/ul/li[2]/a")).getText();
//    public String secondPageNumber = driver.findElement(By.xpath("(//div[@class='pagination-wrapper'])[2]/ul/li[2]/a")).getText();
//    public String one = "1";
//    public int ten = 10;
//
//    public static int getArticleLinks() {
//        List<WebElement> articles = driver.findElements(By.xpath("//ul[contains(@class, 'list-group')]/li"));
//        int articleLength = articles.size();
//        return articleLength;
//    }
//
//    public static List<WebElement> getArticleDates() {
//        List<WebElement> articles = driver.findElements(By.xpath("//ul[contains(@class, 'list-group')]/li/div/div[1]/h4/p/a/span"));
//        return articles;
//    }
//
//    public int numberOfArticles = 10;
//    public By teaserHeader = By.xpath("//div[contains(@class, 'teaser-title ')]/h4/a");
//    public By teaserDate = By.xpath("//span[contains(@class, 'teaser-date')]");
//    public By teaserImage = By.xpath("//div[contains(@class, 'teaser-image')]/a/img");
//    public By teaserSummaryText = By.xpath("//span[contains(@class, 'topic')]/a/p");
//    public By todayRefineLink = By.xpath("//div[contains(@class, 'relative-date-filter')]/ul/li[3]");
//    public By lastSevenDaysRefineLink = By.xpath("//div[contains(@class, 'relative-date-filter')]/ul/li[2]");
//    public By lastThirtyDaysRefineLink=  By.xpath("//div[contains(@class, 'relative-date-filter')]/ul/li[3]");
//    public By lastYearRefineLink = By.xpath("//div[contains(@class, 'relative-date-filter')]/ul/li[4]");
//    public By firstArticleDate = By.xpath("//ul[contains(@class, 'list-group')]/li[1]/div/div[1]/h4/p/a/span");
//    public By firstArticleTitle = By.xpath("//ul[contains(@class, 'list-group')]/li[1]/div/div[1]/h4/a");
//    public String searchError = "Your search for " + searchTerm + "did not match anything.";
//    public By searchErrorElement = By.xpath("//div[contains(text(),'" + searchError + "')]");
//    public By searchResultsNumberOne = By.xpath("//p[contains(text(), '1 - 10')]");
//    public By searchResultsNumberTwo = By.xpath("//p[contains(text(), '11 - 20')]");
//    public By searchResultsNumberThree = By.xpath("//p[contains(text(), '21 - 30')]");
//    public By searchResultsNumberFour = By.xpath("//p[contains(text(), '31 - 40')]");
//    public By searchResultsNumberFive = By.xpath("//p[contains(text(), '41 - 50')]");

//    public void comparingNumbers( String[] PAGE_NUMBERS) {
//        for (int i = 0; i < contentBlock.size(); i++) {
//            List<By> Bys = contentBlock.get(i);
//            for (int j = 0; j < Bys.size(); j++) {
//                By element = Bys.get(j);
//                String elementName = element.getText();
//                String expectedElementName = expectedLinkNames[i][j];
//                if (!elementName.equals(expectedElementName)) {
//                    System.out.println("Footer links did not match, element name:" + elementName + " expected element name:" + expectedElementName);
//                    driver.close();
//                }
//            }
//        }
//    }

//    public Boolean comparingArticleDates(int amountDays, String date) throws ParseException {
//
//        Date today = Calendar.getInstance().getTime();
//        System.out.println("todays date: " + today.toString());
//
//
//
//        Date articleDate = new SimpleDateFormat("MMMM" + " " + "dd" + ", " + "yyyy", Locale.UK).parse(date);
//        System.out.println("article date = " + articleDate.toString());
//
//        Calendar calendar = Calendar.getInstance();
//        calendar.add(Calendar.DATE, - amountDays);
//
//        Date minusDays = calendar.getTime();
//
//        // if amountDays = 365, change minusDays to beginning of the year, look into how this can be found
//
//        return articleDate.after(minusDays);
//    }
//
//    public void comparingRefineYourSearchResults() {

//        click(search.todayRefineLink);
//        waitForClickableElement(search.firstArticleDate);
//
//        if (!driver.findElement(search.firstArticleDate).getText().equals(todaysDate())) {
//            System.out.println("The date of the first article did not match the system date for today, actual date: " + driver.findElement(search.firstArticleDate).getText());
//        }
//
//        click(search.lastSevenDaysRefineLink);
//        waitForClickableElement(search.firstArticleDate);
//
//        // TODO: workout how to check the list of displayed dates fall against a set of allowed date ranges
//
//        click(search.lastThirtyDaysRefineLink);
//        waitForClickableElement(search.firstArticleDate);
//
//        // TODO: workout how to check the list of displayed dates fall against a set of allowed date ranges
//
//        click(search.lastYearRefineLink);
//        waitForClickableElement(search.firstArticleDate);
//
//        // TODO: workout how to check the list of displayed dates fall against a set of allowed date ranges
//    }
//
//    public static final String[] PAGE_NUMBERS = new String[]{"1 - 10", "11-20", "21-30", "31-40", "41-50",};
//
//    public static List<WebElement> searchQueue = driver.findElements(By.xpath("//div[@class='search-results inner-a col-md-8']/ul/li"));
//    public static By searchHeaderText = By.xpath("//div[@class='search-results inner-a col-md-8']/ul/li/div/div/h4/a");
//    public static By searchDateText = By.xpath("//div[@class='search-results inner-a col-md-8']/ul/li/div/div/h4/p/a/span");
//    public static By searchImage = By.xpath("//div[@class='search-results inner-a col-md-8']/ul/li/div/div[2]/a/img");
//    public static By searchSummary = By.xpath("//div[@class='search-results inner-a col-md-8']/ul/li/div/div[3]/span[2]/a/p");
}