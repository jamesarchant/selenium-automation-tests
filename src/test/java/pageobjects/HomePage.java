package pageobjects;

import net.bytebuddy.utility.RandomString;
import org.openqa.selenium.*;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.*;

public class HomePage extends BasePage {

    // header
    public final String[] TWENTY_FOUR_NAV_ITEMS = new String[]{"jobs24", "localsearch24", "familynotices24", "homes24", "drive24", "mydate24", "newsprints", "private-advertising"};
    public final String[] USEFUL_LINKS_ITEMS = new String[]{"edp24.mydate24.co.uk", "archantnorfolk.newsprints.co.uk", "archanthub.co.uk/private-advertising"};
    public static final String[] SEARCH_TERMS = new String[]{"football", "brexit", "restaurant", "NDR", "jail"};
    public static final String[] INVALID_SEARCH_TERMS = new String[]{"erlgel", "reokreogre", "wekoregoeg"};

    // navigation
    public final String[] EXPECTED_PRIMARY_LINKS = new String[]{"Home", "News", "Sport", "Business", "Going Out", "Features", "Your Town", "Opinion", "Property", "Jobs", "Motoring"};

    // sidebar
    public final String[] EXPECTED_SOCIAL_LINKS = new String[]{"facebook", "youtube", "twitter"};

    // footer
    public static final String[] NEWS_LINKS_ITEMS = new String[]{"Crime", "Politics", "Health", "Education", "Environment", "Obituaries", "Weather"};
    public static final String[] SPORT_LINKS_ITEMS = new String[]{"Norwich City", "Columnists", "Lowestoft Town FC", "Kings Lynn FC", "Cricket", "Kings Lynn Stars", "Pink 'Un"};
    public static final String[] BUSINESS_LINKS_ITEMS = new String[]{"Business News", "Farming", "Opinion", "Top100"};
    public static final String[] WHATS_ON_LINKS_ITEMS = new String[]{"Search Listings", "Submit Listing", "Eating Out or Eating In", "Music", "Comedy", "Theatre", "Art & Exhibitions", "Days Out"};
    public static final String[] USEFUL_LINKS_LINKS_ITEMS = new String[]{"About Us", "Contact Us", "How to Complain", "Terms & Conditions", "Privacy Policy", "Cookie Policy", "Manage Cookies", "Contributor's charter"};
    public static final String[] MORE_GREAT_CONTENT_ONE = new String[]{"Homes24", "Jobs24", "Drive24", "Local Dating", "Buy Photos", "Family Notices"};
    public static final String[] MORE_GREAT_CONTENT_TWO = new String[]{"Classifieds", "EDP Weddings", "Localsearch24", "Reader Travel", "EDP Cottages"};
    public static final String[] MORE_GREAT_CONTENT_THREE = new String[]{"The PinkUn Magazine", "The New European"};
    public static final String[][] EXPECTED_FOOTER_LINKS = new String[][]{NEWS_LINKS_ITEMS, SPORT_LINKS_ITEMS, BUSINESS_LINKS_ITEMS, WHATS_ON_LINKS_ITEMS, USEFUL_LINKS_LINKS_ITEMS};
    public static final String[][] EXPECTED_MORE_GREAT_CONTENT_LINKS = new String[][]{MORE_GREAT_CONTENT_ONE, MORE_GREAT_CONTENT_TWO, MORE_GREAT_CONTENT_THREE};

    public HomePage(WebDriver driver) {
        super(driver);
    }

    // home
    private By mainLogo = By.id("masthead-logo");
    private By mainNewsSection = By.xpath("(//div[@class='story-list nth-child-floats main-news'])[1]");
    private By newsletterWidget = By.cssSelector(".subscription-widget");
    private By driveWidget = By.cssSelector(".lp_drive24.text-center");
    private By mostReadQueue_Header = By.cssSelector(".story-list.most-read.teaser-counter.title-underline");
    private By featuredPagesWidget = By.cssSelector("#campaigns-slider-content-a");
    private By digitalEditionWidget = By.xpath("//div[@class='digital-edition'][1]");
    private By greatDaysOutWidget = By.xpath("//div[@class='digital-edition'][6]");
    private By planningFinderWidget = By.cssSelector("#pfWidget");
    private By localGuideWidget = By.cssSelector(".boost-local-guide-link");
    private By jobs24Widget = By.cssSelector("#jobs24Widget");
    private String summaryText;
    private By youTubeUsefulLinks = By.cssSelector(".social-link.you-tube a");
    private By twitterUsefulLinks = By.cssSelector(".social-link.twitter a");
    private By facebookUsefulLinks = By.cssSelector(".social-link.facebook a");
    private By latestFromEDPQueue = By.cssSelector(".story-list.most-read.teaser-counter.title-underline.visible-md");
    private By mainArticleElements = By.cssSelector(".teaser-title");
    private By mainContent = By.cssSelector("#main-content");

    // nav
    private By registrationAnd24LinksHeader = By.cssSelector("#verticals-header");
    private By mainNavigationHeader = By.cssSelector(".primary.nav");
    private By loginLink = By.linkText("Log in");
    private By registerLink = By.linkText("Register");
    private By logoutLink = By.linkText("Log out");
    private By jobs24Link = By.xpath("//ul[@id='verticals']/li[1]/a");
    private By localSearchLink = By.xpath("//ul[@id='verticals']/li[2]/a");
    private By familyNoticesLink = By.xpath("//ul[@id='verticals']/li[3]/a");
    private By homes24Link = By.xpath("//ul[@id='verticals']/li[4]/a");
    private By drive24Link = By.xpath("//ul[@id='verticals']/li[5]/a");
    private By usefulLinks = By.xpath("//ul[@id='useful-links']/a");
    private By myDateLink = By.xpath("//ul[@id='useful-links']/li[1]/a");
    private By youTubeLink = By.cssSelector(".social-link.you-tube a");
    private By twitterLink = By.cssSelector(".social-link.twitter a");
    private By facebookLink = By.cssSelector(".social-link.facebook a");
    public List<WebElement> twentyFourNavLinks = driver.findElements(By.cssSelector("#verticals li a"));
    public List<WebElement> usefulNavLinks = driver.findElements(By.cssSelector("#useful-links li a"));
    public List<WebElement> primaryLinks = driver.findElements(By.xpath("//ul[@class='primary nav']/li/a"));
    public List<WebElement> articleLinks = driver.findElements(By.xpath("(//div[@class='story-list nth-child-floats main-news'])[1]/div"));
    private By socialLinksElement = By.cssSelector("#social-links a");
    public List<WebElement> socialLinks = driver.findElements(socialLinksElement);
    private By mainArticle = By.xpath("(//div[@class='teaser-title'])[1]/h2/a");
    private By navBar = By.cssSelector(".collapse.navbar-collapse.navbar-ex1-collapse");
    private By burgerNavBar = By.cssSelector(".navbar-toggle");
    private By minimizedNav = By.cssSelector(".navbar-primary-container");
    private By copyrightElement = By.cssSelector(".copyright");
    private By termsAndConditionsLink = By.cssSelector(".bottom div:nth-of-type(5) ul li:nth-of-type(4) a");
    private By privacyPolicyLink = By.cssSelector(".bottom div:nth-of-type(5) ul li:nth-of-type(5) a");
    private By cookiePolicyLink = By.cssSelector(".bottom div:nth-of-type(5) ul li:nth-of-type(6) a");
    private By mainFooter = By.cssSelector(".bottom");
    private By moreGreatContentFooter = By.cssSelector("#footer-tabs");

    public By primaryNavigationLinks = By.cssSelector(".primary.nav > li");
    public List<WebElement> primaryNavigationLinksList = driver.findElements(primaryNavigationLinks);
    public List<WebElement> articleQueue = driver.findElements(By.cssSelector(".medium2col-teaser-item.teaser.clearfix"));
    public By mostReadQueue_Article = By.xpath("//div[contains(@id, 'campaigns-slider-content-a')]");

    // send us a story
    public By sendUsAStoryLink_Top = By.linkText("Send Us a Story");
    public By sendUsAStoryLink_Bottom = By.cssSelector("li[data-tab-id='send-a-story']");
    public By sendUsAStoryLink_BottomForm = By.cssSelector(".tab-content.send-a-story.current");
    public By nameText = By.xpath("//label[text()='Name']");
    public By emailText = By.id("frm-email");
    public By telephoneText = By.xpath("//label[text()='Telephone']");
    public By storyDetailsText = By.xpath("//label[text()='Story details']");
    public By briefSummaryText = By.xpath("//label[text()='Brief summary']");
    public By sendUsAStoryInput_ReCaptcha = By.cssSelector(".g-recaptcha");
    public By sendUsAStoryInput_SubmitButton = By.name("submit");
    public By termsAndConditionsCheckbox = By.name("terms");
    public By termsAndConditionsParagraph = By.cssSelector(".highlight.acceptTerms div label");
    public By sendUsAStoryInput_TermsLink = By.cssSelector(".highlight.acceptTerms div label a");
    public By gdprHeader = By.cssSelector(".gdpr-notice p");
    public By photoSection = By.cssSelector(".photo-details.col-sm-12");
    public By audioSection = By.cssSelector(".highlight.audio-details.col-sm-12");
    public By videoSection = By.cssSelector(".video-details.col-sm-12");

    // tell us about a story
    public By tellUsAboutAnEventLink = By.linkText("Submit Listing");

    // hot topics
    public By hotTopicsHeader = By.cssSelector(".results-gt-inner-2.lp_toplinks span");
    public By hotTopicsHeaderElements = By.cssSelector(".results-gt-inner-2.lp_toplinks span a");


    public List<String> listOfHotTopics = new ArrayList<String>();
    public String topicTagSummary;
    public String topicTagDetail;

    // newsletter
    private String newsletterFrame = "esp-newsletter";

    public By newsletter_Title = By.cssSelector(".subscription-widget h4");
    public By newsletterSelectionOne = By.cssSelector(".multiple-newsletters div:nth-of-type(1)");
    public By newsletterSelectionTwo = By.cssSelector(".multiple-newsletters div:nth-of-type(2)");

    public By newsletterImage = By.cssSelector("img[src='/res/archant_mkt_prod1/6116e8a9396e9fb71019074a771f7fc57b710fdd4669c512bb82f846834f3b36.png']");
    public By newsletter_Email = By.id("#email-address");
    public By newsletter_Name = By.id("subscriber-name");
    public By newsletter_CheckboxOne = By.cssSelector(".subscription-widget__form div:nth-child(7)");
    public By newsletter_CheckboxTwo = By.cssSelector(".subscription-widget__form div:nth-child(8)");
    public By newsletter_JoinButton = By.cssSelector(".btn.btn-default.subscription-widget__submit");
    public By newsletter_PrivacyLink = By.cssSelector(".text-right a");
    public By newsletter_ConfirmationText1 = By.cssSelector(".subscription-widget p:nth-of-type(1)");
    public By newsletter_ConfirmationText2 = By.cssSelector(".subscription-widget p:nth-of-type(2)");
    public String fullName = RandomString.make(4) + " " + RandomString.make(10);

    // search
    public By search_Header = By.xpath("//ul[@class='list-group']/li/div[1]/div[1]/h4/a");
    public By search_Image = By.xpath("//ul[@class='list-group']/li/div[1]/div[2]/a/img");
    public By search_Summary = By.xpath("//ul[@class='list-group']/li/div[1]/div[1]/h4/a");

    public List<String> newsletterFrequency() {
        return Arrays.asList("(Daily)", "(Twice Weekly)", "(Weekly)", "(Regular)");
    }

    public void fillInNewsletter_Home(String tempMailEmail) {
        HomePage homePage = new HomePage(driver);
        homePage.enterText(homePage.newsletter_Email, tempMailEmail);
        homePage.enterText(homePage.newsletter_Name, fullName);
    }

    public void scrollToCopyright() {
        scrollToElement(copyrightElement);
    }

    public void assertTermsMatchesText(String text) {
        assert (driver.findElement(termsAndConditionsLink).getText().equals(text));
    }

    public void waitForMainNews() {
        waitForElement(mainNewsSection);
    }

    public void clickRegisterLink() {
        waitForElement(registrationAnd24LinksHeader);
        click(registerLink);
    }

    public void clickLoginLink() {
        waitForElement(registrationAnd24LinksHeader);
        click(loginLink);
    }

    public void fillInNewsletter_Article(String tempMailEmail) {
        Article article = new Article(driver);
        //article.enterText(article.mainArticleNewsletterEmail, tempMailEmail);
    }

    public List<String> addHotTopicsToString() {
        for (int i = 0; i < driver.findElements(hotTopicsHeaderElements).size(); i++) {
            WebElement element = driver.findElements(hotTopicsHeaderElements).get(i);
            if (!element.getText().contains("|") && !element.getText().contains("Hot Topics") && !element.getText().equals("")) {
                listOfHotTopics.add(element.getText());
            }
        }
        return listOfHotTopics;
    }

    public void clickHotTopicLinks() {
        for (int i = 0; i < driver.findElements(hotTopicsHeaderElements).size(); i++) {
            mainWindow = driver.getWindowHandle();
            WebElement element = driver.findElements(hotTopicsHeaderElements).get(i);
            String hotTopicsNavigationLink = element.getText().toLowerCase();
            clickList(element);
            ArrayList tabs = new ArrayList(driver.getWindowHandles());
            HotTopics hotTopics = new HotTopics(driver);
            if (tabs.size() > 1) {
                driver.switchTo().window(String.valueOf(tabs.get(1)));
                secondWindow = driver.getWindowHandle();
                waitForElement(genericSelector());
                driver.close();
                driver.switchTo().window(mainWindow);
            } else {
                waitForElement(genericSelector());
                try {
                    hotTopics.checkHeaderIsDisplayed();
                    assertEquals(hotTopics.getHeader(), hotTopicsNavigationLink);
                    assertTrue(hotTopics.checkHotTopicQueueHeaders());
                    assertTrue(hotTopics.checkHotTopicQueueImages());
                    assertTrue(hotTopics.checkHotTopicQueueSummaries());
                } catch (NoSuchElementException e) {
                    System.out.println("No Hot Topics header for " + hotTopicsNavigationLink);
                } catch (TimeoutException e) {
                    System.out.println("No Hot Topics header for " + hotTopicsNavigationLink);
                }
                navigateBack();
            }
        }
    }

    public void checkHotTopicsResponsiveness() {
        for (int i = 0; i < driver.findElements(hotTopicsHeaderElements).size(); i++) {
            mainWindow = driver.getWindowHandle();
            WebElement element = driver.findElements(hotTopicsHeaderElements).get(i);
            String hotTopicsNavigationLink = element.getText().toLowerCase();
            clickList(element);
            ArrayList tabs = new ArrayList(driver.getWindowHandles());
            HotTopics hotTopics = new HotTopics(driver);
            if (tabs.size() > 1) {
                driver.switchTo().window(String.valueOf(tabs.get(1)));
                secondWindow = driver.getWindowHandle();
                waitForElement(genericSelector());
                driver.close();
                driver.switchTo().window(mainWindow);
            } else {
                waitForElement(genericSelector());
                try {
                    hotTopics.checkHeaderIsDisplayed();
                    assertEquals(hotTopics.getHeader(), hotTopicsNavigationLink);
                    assertTrue(hotTopics.checkHotTopicQueueHeaders());
                    assertTrue(hotTopics.checkHotTopicQueueImages());
                    assertTrue(hotTopics.checkHotTopicQueueSummaries());
                    minimizeWindow();
                    assertTrue(hotTopics.checkHotTopicQueueHeaders());
                    assertTrue(hotTopics.checkHotTopicQueueImages());
                    assertTrue(hotTopics.checkHotTopicQueueSummaries());
                } catch (NoSuchElementException e) {
                    System.out.println("No Hot Topics header for " + hotTopicsNavigationLink);
                } catch (TimeoutException e) {
                    System.out.println("No Hot Topics header for " + hotTopicsNavigationLink);
                }
                navigateBack();
            }
        }
    }

    public void clickHotTopicsHeader() {
        for (int i = 0; i < driver.findElements(hotTopicsHeaderElements).size(); i++) {
            mainWindow = driver.getWindowHandle();
            WebElement element = driver.findElements(hotTopicsHeaderElements).get(i);
            String hotTopicsNavigationLink = element.getText().toLowerCase();
            clickList(element);
            ArrayList tabs = new ArrayList(driver.getWindowHandles());
            HotTopics hotTopics = new HotTopics(driver);
            if (tabs.size() > 1) {
                driver.switchTo().window(String.valueOf(tabs.get(1)));
                secondWindow = driver.getWindowHandle();
                waitForElement(genericSelector());
                driver.close();
                driver.switchTo().window(mainWindow);
            } else {
                waitForElement(genericSelector());
                try {
                    hotTopics.checkHeaderIsDisplayed();
                    assertEquals(hotTopics.getHeader(), hotTopicsNavigationLink);
                    assertTrue(hotTopics.checkHotTopicQueueHeaders());
                    assertTrue(hotTopics.checkHotTopicQueueImages());
                    assertTrue(hotTopics.checkHotTopicQueueSummaries());

                    List<WebElement> listOfLinks = driver.findElements(By.cssSelector(".teaser-title > h2"));
                    WebElement articleElement = listOfLinks.get(randomNumber);
                    articleElement.click();

                    Article article = new Article(driver);
                    assertTrue(article.checkHeaderIsDisplayed());
                    assertTrue(article.checkLeadImageIsDisplayed());
                    assertTrue(article.checkLeadImageCaptionIsDisplayed());
                    assertTrue(article.checkSummaryIsDisplayed());
                    assertTrue(article.checkBodyTextIsDisplayed());
                } catch (NoSuchElementException e) {
                    System.out.println("No Hot Topics header for " + hotTopicsNavigationLink);
                } catch (TimeoutException e) {
                    System.out.println("No Hot Topics header for " + hotTopicsNavigationLink);
                }
                navigateBack();
            }
            if (loopCount == 3) {
                break;
            }
        }
    }

    public void clickHotTopicsImage() {
        for (int i = 0; i < driver.findElements(hotTopicsHeaderElements).size(); i++) {
            mainWindow = driver.getWindowHandle();
            WebElement element = driver.findElements(hotTopicsHeaderElements).get(i);
            String hotTopicsNavigationLink = element.getText().toLowerCase();
            clickList(element);
            ArrayList tabs = new ArrayList(driver.getWindowHandles());
            HotTopics hotTopics = new HotTopics(driver);
            if (tabs.size() > 1) {
                driver.switchTo().window(String.valueOf(tabs.get(1)));
                secondWindow = driver.getWindowHandle();
                waitForElement(genericSelector());
                driver.close();
                driver.switchTo().window(mainWindow);
            } else {
                waitForElement(genericSelector());
                try {
                    hotTopics.checkHeaderIsDisplayed();
                    assertEquals(hotTopics.getHeader(), hotTopicsNavigationLink);
                    assertTrue(hotTopics.checkHotTopicQueueHeaders());
                    assertTrue(hotTopics.checkHotTopicQueueImages());
                    assertTrue(hotTopics.checkHotTopicQueueSummaries());

                    List<WebElement> listOfLinks = driver.findElements(By.cssSelector(".teaser-image"));
                    WebElement articleElement = listOfLinks.get(randomNumber);
                    articleElement.click();

                    Article article = new Article(driver);
                    assertTrue(article.checkHeaderIsDisplayed());
                    assertTrue(article.checkLeadImageIsDisplayed());
                    assertTrue(article.checkLeadImageCaptionIsDisplayed());
                    assertTrue(article.checkSummaryIsDisplayed());
                    assertTrue(article.checkBodyTextIsDisplayed());
                } catch (NoSuchElementException e) {
                    System.out.println("No Hot Topics header for " + hotTopicsNavigationLink);
                } catch (TimeoutException e) {
                    System.out.println("No Hot Topics header for " + hotTopicsNavigationLink);
                }
                navigateBack();
            }
            if (loopCount == 3) {
                break;
            }
        }
    }

//                    scrollToElement(topicTags_First);
//                    if (listOfTopicTags.size() == 1) {
//                        topicTagDetail = driver.findElement(topicTags_First).getText();
//                        topicTagDetail = topicTagDetail.toLowerCase();
//
//                        assertEquals(topicTagSummary,topicTagDetail);
//
//                    } else {
//                        List<String> listOfTopicTags = new ArrayList<String>();
//
//                        for (int k = 0; k < listOfTopicTags.size(); k++) {
//                            listOfTopicTags.add(listOfTopicTags.get(k).getText());
//                            topicTagDetail = listOfTopicTags.get(k).getText();
//                            topicTagDetail = topicTagDetail.toLowerCase();
//
//                            try {
//                                assertEquals(topicTagSummary, topicTagDetail);
//                                topicTagCount++;
//                                if (topicTagCount == 3) {
//                                    break;
//                                }
//                            } catch (NoSuchElementException e) {
//                                continue;
//                            }
//                        }
//                    }
//
//                    topicTagCount++;
//                    navigateBack();
//
//    }

    // topicTagSummary = driver.findElement(topics.topicsPageHeader).getText();
//                topicTagSummary = topicTagSummary.toLowerCase();
//
//                int queueLoopCount = 3;
//                for (int j = 0; j < queueLoopCount; i++) {
//                    // click on a article at random
//                    int i1 = randomNumber(articleQueue.size());
//                    clickList(articleQueue.get(i1));
//
//                    scrollToElement(topicTags_First);
//                    if (listOfTopicTags.size() == 1) {
//                        topicTagDetail = driver.findElement(topicTags_First).getText();
//                        topicTagDetail = topicTagDetail.toLowerCase();
//
//                        assertEquals(topicTagSummary,topicTagDetail);
//
//                    } else {
//                        List<String> listOfTopicTags = new ArrayList<String>();
//
//                        for (int k = 0; k < listOfTopicTags.size(); k++) {
//                            listOfTopicTags.add(listOfTopicTags.get(k).getText());
//                            topicTagDetail = listOfTopicTags.get(k).getText();
//                            topicTagDetail = topicTagDetail.toLowerCase();
//
//                            try {
//                                assertEquals(topicTagSummary, topicTagDetail);
//                                topicTagCount++;
//                                if (topicTagCount == 3) {
//                                    break;
//                                }
//                            } catch (NoSuchElementException e) {
//                                continue;
//                            }
//                        }
//                    }
//
//                    topicTagCount++;
//                    navigateBack();

    public static ArrayList<By> nav = new ArrayList<>();

    {
        By home = By.xpath("//ul[@class='primary nav']/li[1]/a");
        By news = By.xpath("//ul[@class='primary nav']/li[2]/a");
        By sport = By.xpath("//ul[@class='primary nav']/li[3]/a");
        By business = By.xpath("//ul[@class='primary nav']/li[4]/a");
        By goingOut = By.xpath("//ul[@class='primary nav']/li[5]/a");
        By features = By.xpath("//ul[@class='primary nav']/li[5]/a");
        By yourTown = By.xpath("//ul[@class='primary nav']/li[6]/a");
        By opinion = By.xpath("//ul[@class='primary nav']/li[7]/a");
        nav.add(home);
        nav.add(news);
        nav.add(sport);
        nav.add(business);
        nav.add(goingOut);
        nav.add(features);
        nav.add(yourTown);
        nav.add(opinion);
    }


    public ArrayList<List<WebElement>> mainFooterLinks() {
        ArrayList<List<WebElement>> lists = new ArrayList<List<WebElement>>();
        List<WebElement> newsFooter = driver.findElements(By.cssSelector(".bottom div:nth-of-type(1) ul li a"));
        List<WebElement> sportFooter = driver.findElements(By.cssSelector(".bottom div:nth-of-type(2) ul li a"));
        List<WebElement> businessFooter = driver.findElements(By.cssSelector(".bottom div:nth-of-type(3) ul li a"));
        List<WebElement> whatsOnFooter = driver.findElements(By.cssSelector(".bottom div:nth-of-type(4) ul li a"));
        List<WebElement> usefulLinksFooter = driver.findElements(By.cssSelector(".bottom div:nth-of-type(5) ul li a"));
        lists.add(newsFooter);
        lists.add(sportFooter);
        lists.add(businessFooter);
        lists.add(whatsOnFooter);
        lists.add(usefulLinksFooter);
        return lists;
    }

    public ArrayList<By> mainFooterByLinks() {
        ArrayList<By> lists = new ArrayList<By>();
        By newsFooter = By.cssSelector(".bottom div:nth-of-type(1) ul li a");
        By sportFooter = By.cssSelector(".bottom div:nth-of-type(2) ul li a");
        By businessFooter = By.cssSelector(".bottom div:nth-of-type(3) ul li a");
        By whatsOnFooter = By.cssSelector(".bottom div:nth-of-type(4) ul li a");
        By usefulLinksFooter = By.cssSelector(".bottom div:nth-of-type(5) ul li a");
        lists.add(newsFooter);
        lists.add(sportFooter);
        lists.add(businessFooter);
        lists.add(whatsOnFooter);
        lists.add(usefulLinksFooter);
        return lists;
    }

    public void clickSecondaryNavigationLinks() {
        for (int i = 0, j = 1; i < 3; i++, j++) {

            waitForElement(mainNavigationHeader);
            mainWindow = driver.getWindowHandle();
            WebElement listItem = driver.findElements(primaryNavigationLinks).get(i);
            hoverElementList(listItem);

            List<WebElement> getSecondaryPrimaryLinks = driver.findElements(By.xpath("//ul[@class='primary nav']/li[" + j + "]/div/div/ul/li/a"));
            WebElement subNavLink = getSecondaryPrimaryLinks.get(i);
            clickHoverElementList(subNavLink);

            ArrayList tabs = new ArrayList(driver.getWindowHandles());

            if (tabs.size() > 1) {
                driver.switchTo().window(String.valueOf(tabs.get(1)));
                secondWindow = driver.getWindowHandle();
                driver.close();
                driver.switchTo().window(mainWindow);
            }
        }
    }

    public void verifyMoreGreatContentFooterLinks() {
        scrollToElement(copyrightElement);

        for (int i = 0; i < mainFooterLinks().size(); i++) {
            List<WebElement> footerLinks = mainFooterLinks().get(i);
            for (int j = 0; j < footerLinks.size(); j++) {
                WebElement element = footerLinks.get(j);
                String elementName = element.getText();
                String expectedElementName = EXPECTED_FOOTER_LINKS[i][j];
                assert (elementName.equals(expectedElementName));
            }
        }
    }

    public void clickRandomMainFooterLinks() {

        for (int i = 0; i < mainFooterLinks().size(); i++) {
            mainWindow = driver.getWindowHandle();

            for (int j = 0; j < 2; j++) {
                scrollToElement(copyrightElement);
                By listOfLinks = mainFooterByLinks().get(i);
                List<WebElement> webElements = driver.findElements(listOfLinks);
                int i1 = randomNumber(webElements.size());
                clickList(webElements.get(i1));
                ArrayList tabs = new ArrayList(driver.getWindowHandles());

                if (tabs.size() > 1) {
                    driver.switchTo().window(String.valueOf(tabs.get(1)));
                    secondWindow = driver.getWindowHandle();
                    driver.close();
                    driver.switchTo().window(mainWindow);
                } else {
                    try {
                        isDisplayed(registerLink);
                    } catch (NoSuchElementException e) {
                        navigateBack();
                    }
                }
            }
        }
    }


//    public static ArrayList<By> nav = new ArrayList<By>();
//    {
//        By home = By.xpath("//ul[@class='primary nav']/li[1]");
//        By news = By.xpath("//ul[@class='primary nav']/li[2]");
//        By sport = By.xpath("//ul[@class='primary nav']/li[3]");
//        By business = By.xpath("//ul[@class='primary nav']/li[4]");
//        By goingOut = By.xpath("//ul[@class='primary nav']/li[5]");
//        By features = By.xpath("//ul[@class='primary nav']/li[5]");
//        nav.add(home);
//        nav.add(news);
//        nav.add(sport);
//        nav.add(business);
//        nav.add(goingOut);
//        nav.add(features);
//    }

    public void clickRandomPrimaryNav() {
        waitForElement(mainLogo);
        int randomNumber = randomNumber(nav.size());
        By randomArticle = nav.get(randomNumber);
        scrollToElement(randomArticle);
        click(randomArticle);

    }

    public String clickRandomArticle() {
        waitForElement(mainArticleElements);
        int randomNumber = randomNumber(driver.findElements(mainArticleElements).size());
        WebElement randomArticle = driver.findElements(mainArticleElements).get(randomNumber);
        String articleTitle = randomArticle.getText();
        scrollToElementList(randomArticle);
        clickList(randomArticle);
        return articleTitle;
    }

    public void clickMainArticle() {
        scrollToElement(mainArticle);
        click(mainArticle);
    }

    public void clickBurgerMenu() {
        click(burgerNavBar);
        waitForElement(minimizedNav);
    }

    public void hoverUsefulLinks() {
        hoverElement(usefulLinks);
    }

    public void clickTermsLink(String url) {
        scrollToElement(copyrightElement);
        mainWindow = driver.getWindowHandle();
        click(termsAndConditionsLink);
        ArrayList tabs = new ArrayList(driver.getWindowHandles());
        waitForWindowSize(2);
        driver.switchTo().window(String.valueOf(tabs.get(1)));
        secondWindow = driver.getWindowHandle();
        assert driver.getCurrentUrl().equals(url);
        driver.close();
        waitForWindowSize(1);
        driver.switchTo().window(mainWindow);
    }

    public void clickPrivacyLink(String url) {
        scrollToElement(copyrightElement);
        mainWindow = driver.getWindowHandle();
        click(privacyPolicyLink);
        ArrayList tabs = new ArrayList(driver.getWindowHandles());
        waitForWindowSize(2);
        driver.switchTo().window(String.valueOf(tabs.get(1)));
        secondWindow = driver.getWindowHandle();
        assert driver.getCurrentUrl().equals(url);
        driver.close();
        waitForWindowSize(1);
        driver.switchTo().window(mainWindow);
    }

    public void clickCookiesLink(String url) {
        scrollToElement(copyrightElement);
        mainWindow = driver.getWindowHandle();
        click(cookiePolicyLink);
        ArrayList tabs = new ArrayList(driver.getWindowHandles());
        waitForWindowSize(2);
        driver.switchTo().window(String.valueOf(tabs.get(1)));
        secondWindow = driver.getWindowHandle();
        assert driver.getCurrentUrl().equals(url);
        driver.close();
        waitForWindowSize(1);
        driver.switchTo().window(mainWindow);
    }

    public By searchPageElement = By.id("frm-search-term");
    public By searchPageSubmitElement = By.name("Submit");
    public String searchTerm;

    public String chooseRandomSearchTerm() {
        int index = randomNumber(SEARCH_TERMS.length);
        return searchTerm = (SEARCH_TERMS[index]);
    }

    public void searchRequest() {
        enterText(searchPageElement, searchTerm);
        System.out.println("The search term used was: " + searchTerm);
        click(searchPageSubmitElement);
        Search search = new Search(driver);
        //waitForElement(search.resultsMainSection);
    }

//    public void clickAssetInSearchResults(By links) {
//        enterText(searchPageElement, searchTerm);
//        System.out.print("The search term used was: " + searchTerm);
//        click(searchPageSubmitElement);
//        Search search = new Search(driver);
//        waitForElement(search.resultsMainSection);
//
//        int randomNumber = randomNumber(driver.findElements(links).size());
//        WebElement randomArticle = driver.findElements(links).get(randomNumber);
//        summaryText = randomArticle.getText();
//        scrollToElementList(randomArticle);
//        waitForClickableElementList(randomArticle);
//        clickList(randomArticle);
//
//        Article article = new Article(driver);
//        article.waitForElement(article.articleContent);
//        article.detailText = article.storeText(article.articleHeader);
//        if (!summaryText.equals(article.detailText)) {
//            System.out.println("Detail text did not match summary text");
//            System.out.println("Summary text: " + summaryText);
//            System.out.println("Detail text: " + article.detailText);
//        }
//    }

    public By invalidSearchText = By.cssSelector(".alert.alert-info");

    public void invalidSearchRequest() {
        for (int i = 0; i < INVALID_SEARCH_TERMS.length; i++) {
            enterText(searchPageElement, INVALID_SEARCH_TERMS[i]);
            click(searchPageSubmitElement);
            Search search = new Search(driver);
            //waitForElement(search.resultsMainSection);
            assertTrue(driver.findElement(invalidSearchText).getText().contains(INVALID_SEARCH_TERMS[i]));
            navigateBack();
        }
    }

    public Boolean checkMainNewsIsDisplayed() {
        waitForElement(mainNewsSection);
        try {
            isDisplayed(mainNewsSection);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Main news section not displayed");
            return false;
        }
    }

    public Boolean checkNewsletterWidgetIsDisplayed() {
        try {
            isDisplayed(newsletterWidget);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Newsletter widget not visible");
            return false;
        }
    }

    public Boolean checkJobs24WidgetIsDisplayed() {
        try {
            isDisplayed(jobs24Widget);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Jobs24 widget not visible");
            return false;
        }
    }

    public Boolean checkDriveWidgetIsDisplayed() {
        try {
            isDisplayed(driveWidget);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Drive widget not visible");
            return false;
        }
    }

    public Boolean checkMostReadQueueIsDisplayed() {
        try {
            isDisplayed(mostReadQueue_Header);
            isDisplayed(mostReadQueue_Article);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Most Read queue widget not visible");
            return false;
        }
    }

    public Boolean checkFeaturedPagesWidgetIsDisplayed() {
        try {
            isDisplayed(featuredPagesWidget);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Featured pages widget not visible");
            return false;
        }
    }

    public Boolean checkDigitalEditionsWidgetIsDisplayed() {
        try {
            isDisplayed(digitalEditionWidget);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Featured pages widget not visible");
            return false;
        }
    }

    public Boolean checkGreatDaysOutWidgetIsDisplayed() {
        try {
            isDisplayed(greatDaysOutWidget);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Featured pages widget not visible");
            return false;
        }
    }

    public Boolean checkPlanningFinderWidgetIsDisplayed() {
        try {
            isDisplayed(planningFinderWidget);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Featured pages widget not visible");
            return false;
        }
    }

    public Boolean checkLocalGuideWidgetIsDisplayed() {
        try {
            isDisplayed(localGuideWidget);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Local guide widget not visible");
            return false;
        }
    }

    public String getLoginLink() {
        return getText(loginLink);
    }

    public Boolean checkLoginLinkIsDisplayed() {
        try {
            waitForElement(loginLink);
            isDisplayed(loginLink);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Login link not visible");
            return false;
        }
    }

    public String getRegisterLink() {
        return getText(registerLink);
    }

    public Boolean checkRegisterLinkIsDisplayed() {
        try {
            waitForElement(registerLink);
            isDisplayed(registerLink);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Register link not visible");
            return false;
        }
    }

    public String getJobs24Link() {
        return getText(jobs24Link);
    }

    public Boolean checkJobs24LinkIsDisplayed() {
        try {
            isDisplayed(jobs24Link);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Jobs24 link not visible");
            return false;
        }
    }

    public String getLocalSearchLink() {
        return getText(localSearchLink);
    }

    public Boolean checkLocalSearchLinkIsDisplayed() {
        try {
            isDisplayed(localSearchLink);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Local Search link not visible");
            return false;
        }
    }

    public String getFamilyNoticesLink() {
        return getText(familyNoticesLink);
    }

    public Boolean checkFamilyNoticesLinkIsDisplayed() {
        try {
            isDisplayed(familyNoticesLink);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Family Notices link not visible");
            return false;
        }
    }

    public String getHomes24Link() {
        return getText(homes24Link);
    }

    public Boolean checkHomes24LinkIsDisplayed() {
        try {
            isDisplayed(homes24Link);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Homes24 link not visible");
            return false;
        }
    }

    public String getDrive24Link() {
        return getText(drive24Link);
    }

    public Boolean checkDrive24LinkIsDisplayed() {
        try {
            isDisplayed(drive24Link);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Drive24 link not visible");
            return false;
        }
    }

    public String getYouTubeLink() {
        return getText(youTubeLink);
    }

    public Boolean checkYouTubeLinkIsDisplayed() {
        try {
            waitForElement(youTubeLink);
            isDisplayed(youTubeLink);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("YouTube link not visible");
            return false;
        }
    }

    public String getTwitterLink() {
        return getText(twitterLink);
    }

    public Boolean checkTwitterLinkIsDisplayed() {
        try {
            waitForElement(twitterLink);
            isDisplayed(twitterLink);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Twitter link not visible");
            return false;
        }
    }

    public String getFacebookLink() {
        return getText(facebookLink);
    }

    public Boolean checkFacebookLinkIsDisplayed() {
        try {
            waitForElement(facebookLink);
            isDisplayed(facebookLink);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Facebook link not visible");
            return false;
        }
    }

    public Boolean checkUsefulLinksAreDisplayed() {
        try {
            waitForElement(usefulLinks);
            isDisplayed(usefulLinks);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Useful links not visible");
            return false;
        }
    }

    public Boolean checkSocialLinksAreDisplayed() {
        try {
            waitForElement(socialLinksElement);
            scrollToElement(socialLinksElement);
            isDisplayed(socialLinksElement);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Social Links not visible");
            return false;
        }
    }

    public Boolean checkMainLogoIsDisplayed() {
        try {
            waitForElement(mainLogo);
            isDisplayed(mainLogo);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Social Links not visible");
            return false;
        }
    }

    public Boolean checkMainNavigationBarIsDisplayed() {
        try {
            waitForElement(navBar);
            isDisplayed(navBar);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Nav bar not visible");
            return false;
        }
    }

    public Boolean checkMinimisedavigationBarIsDisplayed() {
        try {
            waitForElement(burgerNavBar);
            isDisplayed(burgerNavBar);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Minimised bar not visible");
            return false;
        }
    }

    public String getCopyright() {
        return getText(copyrightElement);
    }

    public Boolean checkCopyrightTextIsDisplayed() {
        try {
            scrollToElement(copyrightElement);
            isDisplayed(copyrightElement);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Copyright text not visible");
            return false;
        }
    }

    public Boolean checkMainFooterIsDisplayed() {
        try {
            scrollToElement(mainFooter);
            isDisplayed(mainFooter);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Copyright text not visible");
            return false;
        }
    }

    public Boolean checkMoreGreatContentFooterIsDisplayed() {
        try {
            scrollToElement(moreGreatContentFooter);
            isDisplayed(moreGreatContentFooter);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Copyright text not visible");
            return false;
        }
    }

    public void click24Links() {
        for (int i = 0; i < twentyFourNavLinks.size(); i++) {
            waitForMainNews();

            List<WebElement> links = twentyFourNavLinks;
            mainWindow = driver.getWindowHandle();
            links.get(i).click();
            ArrayList tabs = new ArrayList(driver.getWindowHandles());
            waitForWindowSize(2);
            driver.switchTo().window(String.valueOf(tabs.get(1)));
            secondWindow = driver.getWindowHandle();
            assert (driver.getCurrentUrl().contains(TWENTY_FOUR_NAV_ITEMS[i]));

            driver.close();
            driver.switchTo().window(mainWindow);
        }
    }

    public void clickUsefulLinks() {
        for (int i = 0; i < 3; i++) {
            waitForMainNews();
            hoverElement(usefulLinks);
            List<WebElement> links = usefulNavLinks;
            mainWindow = driver.getWindowHandle();
            clickList(links.get(i));
            ArrayList tabs = new ArrayList(driver.getWindowHandles());
            waitForWindowSize(2);
            driver.switchTo().window(String.valueOf(tabs.get(1)));
            secondWindow = driver.getWindowHandle();
            assert (driver.getCurrentUrl().contains(USEFUL_LINKS_ITEMS[i]));

            driver.close();
            driver.switchTo().window(mainWindow);
        }
    }

    public void clickSocialLinks() {
        for (int i = 0; i < socialLinks.size(); i++) {
            mainWindow = driver.getWindowHandle();
            waitForMainNews();
            List<WebElement> links = socialLinks;
            mainWindow = driver.getWindowHandle();
            clickList(links.get(i));
            ArrayList tabs = new ArrayList(driver.getWindowHandles());
            waitForWindowSize(2);
            driver.switchTo().window(String.valueOf(tabs.get(1)));
            secondWindow = driver.getWindowHandle();
            assert (driver.getCurrentUrl().contains(EXPECTED_SOCIAL_LINKS[i]));

            driver.close();
            driver.switchTo().window(mainWindow);
        }
    }

    public void hoverMainNavigationLinks() {
        waitForMainNews();

        for (int i = 0; i < primaryLinks.size(); i++) {
            List<WebElement> links = primaryLinks;
            assert (links.get(i).getText().contains(EXPECTED_PRIMARY_LINKS[i]));
            hoverElementList(links.get(i));
        }
    }

    public void clickMainNavigationLinks() {
        for (int i = 0; i < driver.findElements(primaryNavigationLinks).size(); i++) {
            mainWindow = driver.getWindowHandle();
            List<WebElement> links = driver.findElements(primaryNavigationLinks);
            assert (links.get(i).getText().contains(EXPECTED_PRIMARY_LINKS[i]));
            clickList(links.get(i));
            ArrayList tabs = new ArrayList(driver.getWindowHandles());
            if (tabs.size() > 1) {
                waitForWindowSize(2);
                driver.switchTo().window(String.valueOf(tabs.get(1)));
                secondWindow = driver.getWindowHandle();

                driver.close();
                driver.switchTo().window(mainWindow);
            } else {
                try {
                    waitForElement(registerLink);
                } catch (NoSuchElementException e) {
                    navigateBack();
                } catch (TimeoutException e) {
                    navigateBack();
                }
            }
        }
    }

    public void checkArticleTitle() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();

            Article article = new Article(driver);
            assertTrue(article.checkHeaderIsDisplayed());
            loopCount++;

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkAuthorName() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();

            waitForElement(mainArticleElements);
            int randomNumber = randomNumber(driver.findElements(mainArticleElements).size());
            WebElement randomArticle = driver.findElements(mainArticleElements).get(randomNumber);
            scrollToElementList(randomArticle);
            clickList(randomArticle);

            Article article = new Article(driver);
            article.checkEitherAuthorNameIsDisplayed();

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkAuthorEmail() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();

            Article article = new Article(driver);
            article.checkAuthorEmailIsDisplayed();

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkPublicationDate() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();

            Article article = new Article(driver);
            article.checkPublicationDateIsDisplayed();

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkLeadImage() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();

            Article article = new Article(driver);
            article.checkLeadImageIsDisplayed();

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkLeadImageCaption() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();

            Article article = new Article(driver);
            article.checkLeadImageCaptionIsDisplayed();

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkSummary() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();
            int randomNumber = randomNumber(driver.findElements(mainArticleElements).size());
            WebElement randomArticle = driver.findElements(mainArticleElements).get(randomNumber);
            scrollToElementList(randomArticle);
            clickList(randomArticle);

            Article article = new Article(driver);
            article.checkSummaryIsDisplayed();

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkFirstSocialMediaIcons() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();
            int randomNumber = randomNumber(driver.findElements(mainArticleElements).size());
            WebElement randomArticle = driver.findElements(mainArticleElements).get(randomNumber);
            scrollToElementList(randomArticle);
            clickList(randomArticle);

            Article article = new Article(driver);
            article.checkFirstSetSocialMediaIconsAreDisplayed();

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkSecondSocialMediaIcons() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();
            int randomNumber = randomNumber(driver.findElements(mainArticleElements).size());
            WebElement randomArticle = driver.findElements(mainArticleElements).get(randomNumber);
            scrollToElementList(randomArticle);
            clickList(randomArticle);

            Article article = new Article(driver);
            article.checkSecondSetSocialMediaIconsAreDisplayed();

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkNewsletterWidget() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();
            int randomNumber = randomNumber(driver.findElements(mainArticleElements).size());
            WebElement randomArticle = driver.findElements(mainArticleElements).get(randomNumber);
            scrollToElementList(randomArticle);
            clickList(randomArticle);

            Article article = new Article(driver);
            article.checkNewsletterWidgetIsDisplayed();

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkMainBody() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();
            int randomNumber = randomNumber(driver.findElements(mainArticleElements).size());
            WebElement randomArticle = driver.findElements(mainArticleElements).get(randomNumber);
            scrollToElementList(randomArticle);
            clickList(randomArticle);

            Article article = new Article(driver);
            article.checkBodyTextIsDisplayed();

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkAdditionalImagesAndCaptions() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();
            int randomNumber = randomNumber(driver.findElements(mainArticleElements).size());
            WebElement randomArticle = driver.findElements(mainArticleElements).get(randomNumber);
            scrollToElementList(randomArticle);
            clickList(randomArticle);

            Article article = new Article(driver);
            article.checkAdditionalImagesAndCaptionsAreDisplayed();

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkTopicTags() {
        for (int i = 0; i < articleLoopCount; i++) {
            addHotTopicsToString();
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();
            int randomNumber = randomNumber(driver.findElements(mainArticleElements).size());
            WebElement randomArticle = driver.findElements(mainArticleElements).get(randomNumber);
            scrollToElementList(randomArticle);
            clickList(randomArticle);

            Article article = new Article(driver);
            article.checkTopicTagsAreDisplayed();

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkRightColumn() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();
            int randomNumber = randomNumber(driver.findElements(mainArticleElements).size());
            WebElement randomArticle = driver.findElements(mainArticleElements).get(randomNumber);
            scrollToElementList(randomArticle);
            clickList(randomArticle);

            Article article = new Article(driver);
            assertTrue(article.checkNewsletterWidgetIsDisplayed());
            assertTrue(article.checkDriveWidgetIsDisplayed());
            assertTrue(article.checkMostReadQueueIsDisplayed());
            assertTrue(article.checkFeaturedPagesWidgetIsDisplayed());
            assertTrue(article.checkDigitalEditionsWidgetIsDisplayed());
            assertTrue(article.checkGreatDaysOutWidgetIsDisplayed());
            assertTrue(article.checkPlanningFinderWidgetIsDisplayed());
            assertTrue(article.checkLocalGuideWidgetIsDisplayed());

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkDisqus() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();
            int randomNumber = randomNumber(driver.findElements(mainArticleElements).size());
            WebElement randomArticle = driver.findElements(mainArticleElements).get(randomNumber);
            scrollToElementList(randomArticle);
            clickList(randomArticle);

            Article article = new Article(driver);
            assertTrue(article.checkDisqusIsDisplayed());

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkArticleResponsiveness() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();
            int randomNumber = randomNumber(driver.findElements(mainArticleElements).size());
            WebElement randomArticle = driver.findElements(mainArticleElements).get(randomNumber);
            scrollToElementList(randomArticle);
            clickList(randomArticle);

            Article article = new Article(driver);
            assertTrue(article.checkHeaderIsDisplayed());
            assertTrue(article.checkLeadImageIsDisplayed());
            assertTrue(article.checkLeadImageCaptionIsDisplayed());
            assertTrue(article.checkSummaryIsDisplayed());
            assertTrue(article.checkBodyTextIsDisplayed());
            minimizeWindow();
            assertTrue(article.checkHeaderIsDisplayed());
            assertTrue(article.checkLeadImageIsDisplayed());
            assertTrue(article.checkLeadImageCaptionIsDisplayed());
            assertTrue(article.checkSummaryIsDisplayed());
            assertTrue(article.checkBodyTextIsDisplayed());

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkAuthorQueueName() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();
            int randomNumber = randomNumber(driver.findElements(mainArticleElements).size());
            WebElement randomArticle = driver.findElements(mainArticleElements).get(randomNumber);
            scrollToElementList(randomArticle);
            clickList(randomArticle);

            Article article = new Article(driver);
            assertTrue(article.checkAuthorNameIsDisplayed());
            article.clickAuthorName();

            AuthorQueue authorQueue = new AuthorQueue(driver);
            assertTrue(authorQueue.checkAuthorQueueNameIsDisplayed());

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkAuthorQueueEmailIsDisplayed() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();
            int randomNumber = randomNumber(driver.findElements(mainArticleElements).size());
            WebElement randomArticle = driver.findElements(mainArticleElements).get(randomNumber);
            scrollToElementList(randomArticle);
            clickList(randomArticle);

            Article article = new Article(driver);
            assertTrue(article.checkAuthorNameIsDisplayed());
            article.clickAuthorName();

            AuthorQueue authorQueue = new AuthorQueue(driver);
            assertTrue(authorQueue.checkAuthorQueueNameIsDisplayed());
            assertTrue(authorQueue.checkAuthorQueueEmailIsDisplayed());

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkAuthorQueueTwitterLinkFunctionality() throws InterruptedException {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();
            int randomNumber = randomNumber(driver.findElements(mainArticleElements).size());
            WebElement randomArticle = driver.findElements(mainArticleElements).get(randomNumber);
            scrollToElementList(randomArticle);
            clickList(randomArticle);

            try {
                Article article = new Article(driver);
                assertTrue(article.checkAuthorNameIsDisplayed());
                article.clickAuthorName();

                AuthorQueue authorQueue = new AuthorQueue(driver);
                assertTrue(authorQueue.checkAuthorQueueNameIsDisplayed());
                assertTrue(authorQueue.checkAuthorQueueEmailIsDisplayed());
                authorQueue.clickAuthorQueueTwitterLink();

                loopCount++;
            } catch (NoSuchElementException e) {
                continue;
            } catch (TimeoutException e) {
                continue;
            }

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkAuthorQueueSocialsFunctionality() throws InterruptedException {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();

            try {
                Article article = new Article(driver);
                assertTrue(article.checkAuthorNameIsDisplayed());
                article.clickAuthorName();

                AuthorQueue authorQueue = new AuthorQueue(driver);
                assertTrue(authorQueue.checkAuthorQueueNameIsDisplayed());
                assertTrue(authorQueue.checkAuthorQueueEmailIsDisplayed());
                authorQueue.clickAuthorQueueSocials();

                loopCount++;
            } catch (NoSuchElementException e) {
                continue;
            } catch (TimeoutException e) {
                continue;
            }
        }
    }

    public void checkAuthorQueueIsDisplayed() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();

            try {
                Article article = new Article(driver);
                assertTrue(article.checkAuthorNameIsDisplayed());
                article.clickAuthorName();

                AuthorQueue authorQueue = new AuthorQueue(driver);
                assertTrue(authorQueue.checkAuthorQueueNameIsDisplayed());
                assertTrue(authorQueue.checkAuthorQueueHeaders());
                assertTrue(authorQueue.checkAuthorQueueImages());
                assertTrue(authorQueue.checkAuthorQueueEmailIsDisplayed());
                assertTrue(authorQueue.checkAuthorQueueSummaries());
                assertTrue(authorQueue.checkAuthorQueueAuthorArticleNames());
                loopCount++;
            } catch (NoSuchElementException e) {
                continue;
            } catch (TimeoutException e) {
                continue;
            }
        }
    }

    public void checkAuthorQueueResponsiveness() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();

            try {
                Article article = new Article(driver);
                assertTrue(article.checkAuthorNameIsDisplayed());
                article.clickAuthorName();

                AuthorQueue authorQueue = new AuthorQueue(driver);
                assertTrue(authorQueue.checkAuthorQueueNameIsDisplayed());
                assertTrue(authorQueue.checkAuthorQueueHeaders());
                assertTrue(authorQueue.checkAuthorQueueImages());
                assertTrue(authorQueue.checkAuthorQueueEmailIsDisplayed());
                assertTrue(authorQueue.checkAuthorQueueSummaries());
                assertTrue(authorQueue.checkAuthorQueueAuthorArticleNames());
                minimizeWindow();
                assertTrue(authorQueue.checkAuthorQueueNameIsDisplayed());
                assertTrue(authorQueue.checkAuthorQueueHeaders());
                assertTrue(authorQueue.checkAuthorQueueImages());
                assertTrue(authorQueue.checkAuthorQueueEmailIsDisplayed());
                assertTrue(authorQueue.checkAuthorQueueSummaries());
                assertTrue(authorQueue.checkAuthorQueueAuthorArticleNames());

                loopCount++;
            } catch (NoSuchElementException e) {
                continue;
            } catch (TimeoutException e) {
                continue;
            }
        }
    }

    public void clickAuthorQueueTitle() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();

            try {
                Article article = new Article(driver);
                assertTrue(article.checkAuthorNameIsDisplayed());
                article.clickAuthorName();

                AuthorQueue authorQueue = new AuthorQueue(driver);
                assertTrue(authorQueue.checkAuthorQueueNameIsDisplayed());
                assertTrue(authorQueue.checkAuthorQueueEmailIsDisplayed());
                authorQueue.clickRandomAuthorQueueTitle();
                assertTrue(authorQueue.checkAuthorQueueNameIsDisplayed());
                String detailAuthor = authorQueue.getAuthorName();
                assert (authorQueue.summaryAuthor.equalsIgnoreCase(detailAuthor));
                authorQueue.navigateBack();
                assertTrue(authorQueue.checkAuthorQueueNameIsDisplayed());
                loopCount++;

            } catch (NoSuchElementException e) {
                continue;
            } catch (TimeoutException e) {
                continue;
            }
        }
    }

    public void clickAuthorQueueImage() {
        for (int i = 0; i < articleLoopCount; i++) {
            if (i > 0) {
                clickRandomPrimaryNav();
            }
            clickRandomArticle();

            try {
                Article article = new Article(driver);
                assertTrue(article.checkAuthorNameIsDisplayed());
                article.clickAuthorName();

                AuthorQueue authorQueue = new AuthorQueue(driver);
                assertTrue(authorQueue.checkAuthorQueueNameIsDisplayed());
                assertTrue(authorQueue.checkAuthorQueueEmailIsDisplayed());
                authorQueue.clickRandomAuthorQueueImage();
                assertTrue(authorQueue.checkAuthorQueueNameIsDisplayed());
                String detailAuthor = authorQueue.getAuthorName();
                assert (authorQueue.summaryAuthor.equalsIgnoreCase(detailAuthor));
                authorQueue.navigateBack();
                assertTrue(authorQueue.checkAuthorQueueNameIsDisplayed());
                loopCount++;

            } catch (NoSuchElementException e) {
                continue;
            } catch (TimeoutException e) {
                continue;
            }
        }
    }

    public Boolean checkHotTopicsHeaderIsDisplayed() {
        try {
            isDisplayed(hotTopicsHeader);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Hot Topics header not visible");
            return false;
        }
    }

    public String getHotTopicsHeader() {
        return getText(hotTopicsHeader);
    }
}