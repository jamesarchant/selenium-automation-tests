package pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

public abstract class BasePage {

//    public static final Logger LOGGER = new Logger("String");

    public int loopCount = 0;
    public int articleLoopCount = 10;
    private static final int TIMEOUT = 90;

    public static WebDriver driver;
    public static WebDriverWait wait;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, TIMEOUT);
    }

    public void waitForFrameAndSwitch(By locator) {
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(locator));
    }

    public void waitForFrameAndSwitch_String(String frameId) { wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameId));}

    public void switchToFrame(String frameId) { driver.switchTo().frame(frameId); }

    public void switchBackToDefaultFrame() {
        driver.switchTo().defaultContent();
    }

    public static final String CHROMEDRIVER = "webdriver.chrome.driver";
    public static final String DRIVER_LOCATION = "C:/Users/richardsonj/drivers/chromedriver.exe";
    public static final String DRIVER_LOCATION_MAC = "/Users/jamesrichardson/Development/Drivers/chromedriver";
    public static final String TEMPMAIL = "https://temp-mail.org/";
    public static final String EASTERN_DAILY_PRESS = ("https://edp24.co.uk");
    public static String tempMailWindow;
    public String mainWindow;
    public String secondWindow;
    public int randomNumber;

    public static final String EYES_API_KEY = "5THDwjkEg2InS110jkpXbER6ms19mEeUKFMC6Q103Esgq7o110";
    public static final int WINDOW_WIDTH = 1024;
    public static final int WINDOW_HEIGHT = 768;

    // Register elements
    public String tempMailEmail;
    public String password = "Hellothere123";
    
    public static boolean alreadyRun = false;

    public void isDisplayed(By locator) {
        driver.findElement(locator).isDisplayed();
    }

    public Boolean isClickable(By locator) {
        try {
            driver.findElement(locator).isEnabled();
            return true;
        } catch (org.openqa.selenium.NoSuchElementException exception) {
            System.out.println("Element + " + locator.toString() + "was not enabled as expected");
            return false;
        }
    }

    public Boolean isNotEnabled(By locator) {
        try {
            driver.findElement(locator).isEnabled();
            return false;
        } catch (org.openqa.selenium.NoSuchElementException exception) {
            System.out.println("Element + " + locator.toString() + "was not enabled as expected");
            return true;
        }
    }

    public static Boolean isSelected(By locator) {
        try {
            return driver.findElement(locator).isSelected();
        } catch (org.openqa.selenium.NoSuchElementException exception) {
            System.out.println("Element + " + locator.toString() + "was not selected as expected");
            return false;
        }
    }

    public List<String> addToString(List<WebElement> listInput, List<String> listOutput) {
        for (int i = 0; i < listInput.size(); i++) {
            WebElement element = listInput.get(i);
            listOutput.add(element.getText());
        }
        return listOutput;
    }

    public String storeText(By locator) {
        return driver.findElement(locator).getText();
    }

    public static void teardown(WebDriver driver) {
        driver.quit();
    }

    public void click(By locator) {
        waitForClickableElement(locator);
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(locator)).click().build().perform();
    }

    public void clickList(WebElement locator) {
        waitForClickableElementList(locator);
        Actions actions = new Actions(driver);
        actions.moveToElement(locator).click().build().perform();
    }

    public String getText(By locator) {
        return driver.findElement(locator).getText();
    }

    public String tempMailUrl;

    public String parseURL (String text){
        ArrayList links = new ArrayList();

        String regex = "\\(?\\b(http://|www[.])[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;@#/%=~_()|]";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(text);
        while (m.find()) {
            String urlStr = m.group();

            if (urlStr.startsWith("(") &&
            urlStr.endsWith(")"))
            {
                urlStr = urlStr.substring(1, urlStr.length() - 1);
            }
            links.add(urlStr);
        }

        tempMailUrl = "http://" + links.get(0).toString();
        return tempMailUrl;
    }

//        mainWindow = driver.getWindowHandle();
//        ArrayList tabs = new ArrayList(driver.getWindowHandles());
//        if (tabs.size() > 1) {
//            driver.switchTo().window(String.valueOf(tabs.get(1)));
//            secondWindow = driver.getWindowHandle();
//            if (driver.getCurrentUrl().contains("adylic")) {
//                driver.close();
//                driver.switchTo().window(mainWindow);
//                HomePage homePage = new HomePage(driver);
//                waitForElement(homePage.registerLink);
//                locator.click();
//            }
//        }
//    }

    public void verifyItemInArray(By locator, List array)  {
        for (int i = 0; i > array.size(); i++) {
            String arrayItem = array.get(i).toString();
            if (driver.findElement(locator).getText().contains(arrayItem)) {
                break;
            }
        }
    }

    public By genericSelector() {
        return By.cssSelector("body");
    }

    public String getElementValue(By locator) {
        return driver.findElement(locator).getAttribute("value");
    }

    public void waitForClickableElement(By locator) {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(locator));
        } catch(NoSuchElementException e) {
            System.out.println(locator + " was not displayed as expected");
        }
    }

    public void waitForClickableElementList(WebElement locator) {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(locator));
        } catch(NoSuchElementException e) {
            System.out.println(locator + " was not displayed as expected");
        }
    }

    public void waitForWindowSize(int number) {
        wait.until(ExpectedConditions.numberOfWindowsToBe(number));
    }

    public Boolean minimizeWindow() {
        try {
            Dimension d = new Dimension(1000, 1100);
            driver.manage().window().setSize(d);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("The window did not resize correctly");
            return false;
        }
    }

    public void maximizeWindow() {
        driver.manage().window().maximize();
    }

    public void fetch(String url) {
        driver.get(url);
    }

    public void waitForElement(By locator) {
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (NoSuchElementException e) {
            System.out.println(locator + " was not displayed as expected");
        }
    }

    public void waitForElementInvisibility(By locator) {
        try {
            wait.until(ExpectedConditions.invisibilityOf(driver.findElement(locator)));
        } catch (NoSuchElementException e) {
            System.out.println(locator + " was still displayed which was not expected");
        }
    }

    public void waitForElementList(WebElement locator) {
        try {
            wait.until(ExpectedConditions.visibilityOf(locator));
        } catch (NoSuchElementException e) {
            System.out.println(locator + " was not displayed as expected");
        }
    }

    public void hoverElement(By element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(element)).perform();
    }

    public void hoverElementList(WebElement element) {
        waitForClickableElementList(element);
        Actions actions = new Actions(driver);
        actions.moveToElement(element).perform();
    }

    public void clickHoverElement(By element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(element)).click().perform();
    }

    public void clickHoverElementList(WebElement element) {
        waitForClickableElementList(element);
        Actions actions = new Actions(driver);
        actions.moveToElement(element).click().perform();
    }

    public void checkTextMatches(By element, String text) {
        assertEquals(driver.findElement(element).getText(), text);
    }

    public void checkCheckbox(By locator) {
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        driver.findElement(locator).click();
    }

    public void enterText(By locator, String text) {
        driver.findElement(locator).clear();
        driver.findElement(locator).sendKeys(text);
        wait.until(ExpectedConditions.textToBePresentInElementValue(locator, text));
    }

    public void selectDropdown(By locator, String text) {
        Select titleDropdown = new Select(driver.findElement(locator));
        titleDropdown.selectByVisibleText(text);
    }

    public void scrollToElementList(WebElement locator) {
        waitForClickableElementList(locator);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", locator);
    }

    public void scrollToElement(By locator) {
        waitForElement(locator);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(locator));
    }

    public static String randomNumberAsString(int bound) {
        Random rand = new Random();
        int randomNumber = rand.nextInt(bound);
        return String.valueOf(randomNumber);
    }

    public int randomNumber(int bound) {
        Random rand = new Random();
        int randomNumber = rand.nextInt(bound);
        return randomNumber;
    }

    public void navigateBack() {
        driver.navigate().back();
    }

    public static void refreshWindow() {
        driver.navigate().refresh();
    }

    public static String todaysDate() {
        SimpleDateFormat df = new SimpleDateFormat("MMMM" + " " + "dd" + ", " + "yyyy", Locale.UK);
        String formatted = df.format(new Date());
        return formatted;
    }

    public void clickLink(By link, String url) {
        mainWindow = driver.getWindowHandle();
        click(link);
        waitForElement(genericSelector());
        ArrayList tabs = new ArrayList(driver.getWindowHandles());
        waitForWindowSize(2);
        driver.switchTo().window(String.valueOf(tabs.get(1)));
        secondWindow = driver.getWindowHandle();
        assert driver.getCurrentUrl().equals(url);
        driver.close();
        waitForWindowSize(1);
        driver.switchTo().window(mainWindow);
    }
}