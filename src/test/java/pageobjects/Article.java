package pageobjects;

import org.openqa.selenium.*;

import java.util.ArrayList;
import java.util.List;

public class Article extends BasePage {

    public Article(WebDriver driver) {
        super(driver);
    }

    private By mainLogo = By.id("masthead-logo");
    private By header = By.cssSelector("#lp_article_content h1");
    private By leadImage = By.cssSelector(".image.article-image.standard-image-630.gallery");
    private By caption = By.cssSelector(".article-image-caption");
    private By summary = By.cssSelector(".stand-first h2");
    private By socialElements = By.xpath("(//div[@class='article-share article'])[1]");
    private By secondSocialElements = By.xpath("(//div[@class='article-share article'])[2]");

    private String detailText;
    private By authorName = By.cssSelector(".journalist-name a");
    private By otherAuthorName = By.cssSelector(".byline a");
    private By additionalImage = By.xpath("(//div[@class='object-465'])[1]/img");
    private By additionalImageCaption = By.xpath("(//div[@class='object-465'])[1]/em");
    private By authorEmail = By.cssSelector(".journalist-mail a");
    private By publishDate = By.cssSelector(".updated strong:nth-of-type(1)");
    private By otherPublishDate = By.cssSelector(".publication-time");
    private By mainArticleNewsletter = By.cssSelector(".subscription-widget.subscription-widget--in-article.clearfix");
    private By newsletteriFrame = By.xpath("//iframe[@id='subscriptionIframe']");
    private By bodyText = By.xpath("//div[@id='article-content']/p[2]");
    private By driveWidget = By.cssSelector(".lp_drive24.text-center");
    private By mostReadQueue_Header = By.cssSelector(".story-list.most-read.teaser-counter.title-underline");
    private By featuredPagesWidget = By.cssSelector("#campaigns-slider-content-a");
    private By digitalEditionWidget = By.xpath("//div[@class='digital-edition'][1]");
    private By greatDaysOutWidget = By.xpath("//div[@class='digital-edition'][6]");
    private By planningFinderWidget = By.cssSelector("#pfWidget");
    private By localGuideWidget = By.cssSelector(".boost-local-guide-link");
    private By jobs24Widget = By.cssSelector("#jobs24Widget");

    public By mostReadQueue_Article = By.xpath("//div[contains(@id, 'campaigns-slider-content-a')]");

    private By disqusElement = By.cssSelector("#article-comments-disqus");
    private By jwPlayer = By.cssSelector("#jwPlayerCam");
    private By authorQueueName = By.cssSelector(".journalist-name a");
    private By authorQueueEmail = By.cssSelector(".journalist-mail a");
    private By mainArticleNewsletterWidget = By.cssSelector(".subscription-widget.subscription-widget--in-article.clearfix");
    private By mainArticleNewsletterHeading = By.cssSelector(".subscription-widget__form.form-horizontal p");
    private By mainArticleNewsletterSubmit = By.cssSelector(".subscription-widget__form.form-horizontal div div:nth-child(2) button");
    private By mainArticleNewsletterPrivacy = By.cssSelector(".text-right a");
    private By mainArticleNewsletterEmail = By.id("subscriber-email");
    private By MainArticleNewsletterSelection = By.cssSelector(".subscription-widget__form.form-horizontal p");
    private String mainArticleSubscriptionFrame = "subscriptionIframe";


    public void errorText(String locatorType) {
        System.out.println("No " + locatorType + "was found for article: " + driver.findElement(header).getText());
    }

    public List<String> listOfTopicTags = new ArrayList<String>();
    public List<WebElement> topicTagElements = driver.findElements(By.cssSelector(".article-tag a"));
    public By topicTagsSection = By.cssSelector(".article-tag a");
    public By topicTags = By.cssSelector(".nav.nav-pills.nav__pills__topicsnav.nav-pills.nav__pills__topics");

    public void clickMainLogo() {
        waitForClickableElement(mainLogo);
        driver.findElement(mainLogo).click();
    }

    public Boolean checkHeaderIsDisplayed() {
        try {
            waitForElement(header);
            isDisplayed(header);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Header not displayed");
            return false;
        }
    }

    public Boolean checkLeadImageIsDisplayed() {
        try {
            waitForElement(leadImage);
            isDisplayed(leadImage);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Lead image not displayed");
            return false;
        }
    }

    public Boolean checkLeadImageCaptionIsDisplayed() {
        try {
            waitForElement(caption);
            isDisplayed(caption);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Caption not displayed");
            return false;
        }
    }

    public Boolean checkSummaryIsDisplayed() {
        try {
            waitForElement(summary);
            isDisplayed(summary);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Summary not displayed");
            return false;
        }
    }

    public Boolean checkBodyTextIsDisplayed() {
        try {
            waitForElement(bodyText);
            isDisplayed(bodyText);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Body text not displayed");
            return false;
        }
    }

    public Boolean checkFirstSetSocialMediaIconsAreDisplayed() {
        try {
            waitForElement(socialElements);
            isDisplayed(socialElements);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Social icons are not displayed");
            return false;
        }
    }

    public Boolean checkSecondSetSocialMediaIconsAreDisplayed() {
        try {
            waitForElement(secondSocialElements);
            scrollToElement(secondSocialElements);
            isDisplayed(secondSocialElements);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Social icons are not displayed");
            return false;
        }
    }

    public Boolean checkNewsletterWidgetIsDisplayed() {
        try {
            waitForFrameAndSwitch(newsletteriFrame);
            isDisplayed(mainArticleNewsletter);
            switchBackToDefaultFrame();
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Newsletter widget not displayed");
            return false;
        }
    }

    public Boolean checkAdditionalImagesAndCaptionsAreDisplayed() {
        try {
            waitForElement(additionalImage);
            isDisplayed(additionalImage);
            isDisplayed(additionalImageCaption);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Additional image not displayed");
            return false;
        }
    }

    public Boolean checkJobs24WidgetIsDisplayed() {
        try {
            isDisplayed(jobs24Widget);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Jobs24 widget not visible");
            return false;
        }
    }

    public Boolean checkDriveWidgetIsDisplayed() {
        try {
            isDisplayed(driveWidget);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Drive widget not visible");
            return false;
        }
    }

    public Boolean checkMostReadQueueIsDisplayed() {
        try {
            isDisplayed(mostReadQueue_Header);
            isDisplayed(mostReadQueue_Article);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Most Read queue widget not visible");
            return false;
        }
    }

    public Boolean checkFeaturedPagesWidgetIsDisplayed() {
        try {
            isDisplayed(featuredPagesWidget);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Featured pages widget not visible");
            return false;
        }
    }

    public Boolean checkDigitalEditionsWidgetIsDisplayed() {
        try {
            isDisplayed(digitalEditionWidget);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Featured pages widget not visible");
            return false;
        }
    }

    public Boolean checkGreatDaysOutWidgetIsDisplayed() {
        try {
            isDisplayed(greatDaysOutWidget);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Featured pages widget not visible");
            return false;
        }
    }

    public Boolean checkPlanningFinderWidgetIsDisplayed() {
        try {
            isDisplayed(planningFinderWidget);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Featured pages widget not visible");
            return false;
        }
    }

    public Boolean checkLocalGuideWidgetIsDisplayed() {
        try {
            isDisplayed(localGuideWidget);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Local guide widget not visible");
            return false;
        }
    }

    public void waitForArticle() {
        waitForElement(header);
    }

    public Boolean checkEitherAuthorNameIsDisplayed() {
        try {
            try {
                waitForElement(authorName);
                isDisplayed(authorName);
            } catch (NoSuchElementException e) {
                waitForElement(otherAuthorName);
                isDisplayed(otherAuthorName);
                System.out.println("Author name not displayed");
            } catch (TimeoutException e) {
                waitForElement(otherAuthorName);
                isDisplayed(otherAuthorName);
                System.out.println("Author name not displayed");
            }
            return true;
        } catch (java.util.NoSuchElementException e) {
            System.out.println("Neither author name was found");
            return false;
        }
    }

    public Boolean checkAuthorNameIsDisplayed() {
        try {
            waitForElement(authorName);
            isDisplayed(authorName);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Author name not displayed");
        }
        return false;
    }

    public void clickAuthorName() {
        click(authorName);
    }

    public Boolean checkAuthorEmailIsDisplayed() {
        try {
            waitForElement(authorEmail);
            isDisplayed(authorEmail);
            return true;
        } catch (java.util.NoSuchElementException e) {
            System.out.println("Author email was not found");
        }
        return false;
    }

    public Boolean checkPublicationDateIsDisplayed() {
        try {
            Article article = new Article(driver);
            try {
                article.isDisplayed(publishDate);
                loopCount++;
            } catch (NoSuchElementException e) {
                article.isDisplayed(otherPublishDate);
                loopCount++;
            }
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Neither publish date was found");
        }
        return false;
    }

    public Boolean checkTopicTagsAreDisplayed() {
        Article article = new Article(driver);
        try {
            scrollToElement(topicTags);
            article.isDisplayed(topicTags);
            loopCount++;
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Topic Tags were found");
        }
        return false;
    }

    public Boolean checkDisqusIsDisplayed() {
        Article article = new Article(driver);
        try {
            waitForElement(secondSocialElements);
            scrollToElement(secondSocialElements);
            waitForElement(disqusElement);
            article.isDisplayed(disqusElement);
            loopCount++;
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Disqus element was found");
        }
        return false;
    }

    public String getArticleHeader() {
        return getText(header);
    }

    public void checkArticleElement(By locator) {
        for (int i = 0; i < articleLoopCount; i++) {
            HomePage homePage = new HomePage(driver);
            if (i > 0) {
                homePage.clickRandomPrimaryNav();
            }
            //homePage.clickRandomArticle(By.cssSelector(".teaser-title"));

            Article article = new Article(driver);
            try {
                article.scrollToElement(locator);
                article.isDisplayed(locator);
                loopCount++;
            } catch (NoSuchElementException e) {
                continue;
            }
            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkNewsletter() {
        for (int i = 0; i < articleLoopCount; i++) {
            HomePage homePage = new HomePage(driver);
            if (i > 0) {
                homePage.clickRandomPrimaryNav();
            }
            //homePage.clickRandomArticle(By.cssSelector(".teaser-title"));

            Article article = new Article(driver);
            try {
                article.waitForElement(By.cssSelector("#lp_article_content"));
                article.waitForFrameAndSwitch(newsletteriFrame);
                article.scrollToElement(mainArticleNewsletter);
                article.isDisplayed(article.mainArticleNewsletter);
                article.switchBackToDefaultFrame();
                loopCount++;
            } catch (NoSuchElementException e) {
                continue;
            }
            if (loopCount == 3) {
                break;
            }
        }
    }


    public void checkOneOfTwoElements(By locator, By locatorFallback) {
        for (int i = 0; i < articleLoopCount; i++) {
            HomePage homePage = new HomePage(driver);
            if (i > 0) {
                homePage.clickRandomPrimaryNav();
            }
            //homePage.clickRandomArticle(By.cssSelector(".teaser-title"));
            Article article = new Article(driver);
            try {
                try {
                    article.isDisplayed(locator);
                    loopCount++;

                } catch (NoSuchElementException e) {
                    article.isDisplayed(locatorFallback);
                    loopCount++;
                }
            } catch (NoSuchElementException e) {
                errorText("neither locator was found");
            }
            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkArticleLeadImage() {
        for (int i = 0; i < articleLoopCount; i++) {
            HomePage homePage = new HomePage(driver);
            if (i > 0) {
                homePage.clickRandomPrimaryNav();
            }
            //homePage.clickRandomArticle(By.cssSelector(".teaser-title"));
            Article article = new Article(driver);

            article.isDisplayed(article.leadImage);
            try {
                try {
                    isDisplayed(publishDate);
                    loopCount++;
                } catch (NoSuchElementException e) {
                    isDisplayed(article.otherPublishDate);
                    loopCount++;
                }
            } catch (NoSuchElementException e) {
                errorText("publication date");
            }

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkTwoArticleElements(By locator, By otherLocator) {
        for (int i = 0; i < articleLoopCount; i++) {
            HomePage homePage = new HomePage(driver);
            if (i > 0) {
                homePage.clickRandomPrimaryNav();
            }
            //homePage.clickRandomArticle(By.cssSelector(".teaser-title"));
            Article article = new Article(driver);
            try {
                scrollToElement(locator);
                article.isDisplayed(locator);
                waitForElement(otherLocator);
                article.isDisplayed(otherLocator);
                loopCount++;

            } catch (NoSuchElementException e) {
                System.out.println("Locator was not found");
            }

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkDisqus(By locator, By otherLocator) {
        for (int i = 0; i < articleLoopCount; i++) {
            HomePage homePage = new HomePage(driver);
            if (i > 0) {
                homePage.clickRandomPrimaryNav();
            }
            //homePage.clickRandomArticle(By.cssSelector(".teaser-title"));
            Article article = new Article(driver);
            try {
                scrollToElement(locator);
                article.isDisplayed(locator);
                //scrollToElement(homePage.jobs24Widget);
                waitForElement(otherLocator);
                article.isDisplayed(otherLocator);
                loopCount++;

            } catch (NoSuchElementException e) {
                article.errorText("caption");
            }

            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkSidebarContents() {

        for (int i = 0; i < articleLoopCount; i++) {
            HomePage homePage = new HomePage(driver);
            if (i > 0) {
                homePage.clickRandomPrimaryNav();
            }
            //homePage.clickRandomArticle(By.cssSelector(".teaser-title"));
            try {
                //scrollToElement(homePage.latestFromEDPQueue);
                //isDisplayed(homePage.latestFromEDPQueue);
            } catch (NoSuchElementException e) {
                System.out.println("No Most Read queue");
            }
            //isDisplayed(homePage.driveWidget);
            //isDisplayed(homePage.jobs24Widget);
            //isDisplayed(homePage.planningFinderWidget);
            loopCount++;
            if (loopCount == 3) {
                break;
            }
        }
    }

    public void checkArticleResponsiveness(By locator) {
        for (int i = 0; i < articleLoopCount; i++) {
            HomePage homePage = new HomePage(driver);
            if (i > 0) {
                homePage.clickRandomPrimaryNav();
            }
            //homePage.clickRandomArticle(By.cssSelector(".teaser-title"));

            Article article = new Article(driver);
            try {
                article.waitForElement(locator);
                article.isDisplayed(locator);
                article.minimizeWindow();
                article.isDisplayed(locator);
                article.maximizeWindow();
                loopCount++;
            } catch (NoSuchElementException e) {
                System.out.println(getText(locator) + " was not visible");
            }
            if (loopCount == 3) {
                break;
            }
        }
    }

//    public void checkTopicTags() {
//        for (int i = 0; i < articleLoopCount; i++) {
//            HomePage homePage = new HomePage(driver);
//
//            if (i == 0) {
//                homePage.addHotTopicsToString();
//            } else if (i > 0) {
//                homePage.clickRandomPrimaryNav();
//            }
//
//            //homePage.clickRandomArticle(By.cssSelector(".teaser-title"));
//
//            Article article = new Article(driver);
//            try {
//                isDisplayed(article.topicTagsSection);
//                article.scrollToElement(article.topicTagsSection);
//                List<String> topicTags = addToString(article.topicTagElements, article.listOfTopicTags);
//                assert (!topicTags.contains(homePage.listOfHotTopics));
//                article.isDisplayed(article.topicTags);
//                loopCount++;
//            } catch (NoSuchElementException e) {
//                continue;
//            }
//            if (loopCount == 1) {
//                break;
//            }
//        }
//    }

    public void clickTopicTags() {
        for (int i = 0; i < articleLoopCount; i++) {
            HomePage homePage = new HomePage(driver);
            if (i == 0) {
                homePage.addHotTopicsToString();
            } else if (i > 0) {
                homePage.clickRandomPrimaryNav();
            }
            //homePage.clickRandomArticle(By.cssSelector(".teaser-title"));

            Article article = new Article(driver);
            try {
                isDisplayed(article.topicTagsSection);
                article.scrollToElement(article.topicTagsSection);
                List<String> topicTags = addToString(article.topicTagElements, article.listOfTopicTags);
                assert (!topicTags.contains(homePage.listOfHotTopics));
                article.isDisplayed(article.topicTags);
                article.scrollToElement(article.topicTags);
                for (int j = 0; j < topicTagElements.size(); j++) {
                    int randomNumber = randomNumber(topicTagElements.size());
                    WebElement topicTag = topicTagElements.get(randomNumber);
                    String topicTagSummary = topicTag.getText();
                    clickList(topicTag);

                    HotTopics topics = new HotTopics(driver);
                    //topics.waitForElement(topics.hotTopicsQueue);
                    //String topicTagDetail = topics.getText(topics.hotTopicsHeader);
                    //if (!topicTagSummary.equals(topicTagDetail)) {
                    //    System.out.println("Summary did not match detail. Summary " + topicTagSummary);
                    //    System.out.println("Detail: " + topicTagDetail);
                    //}
                    navigateBack();
                }

            } catch (NoSuchElementException e) {
                continue;
            }
            if (loopCount == 1) {
                break;
            }
        }
    }
}

//    public By sportLink = By.xpath("//ul[contains(@class, 'primary nav')]/li[3]/a");
//
//    // Main article elements
//    public By mainArticleHeaderHome = By.xpath("//div[contains(@class, 'small-teaser-item teaser clearfix')][1]");
//    public By mainArticleSummaryHome = By.xpath("//div[contains(@class, 'large-teaser-item teaser clearfix')]/div[4]/p[2]");
//
//    public By mainArticleAuthor = By.xpath("//li[contains(@class, 'journalist-name')]/a");
//    public By mainArticleAuthorFallback = By.xpath("//p[contains(@class, 'byline')]/a");
//    public By mainArticleAuthorTwitter = By.xpath("//li[contains(@class, 'journalist-twitter')]");
//    public By mainArticleSocialsFacebook = By.xpath("//div[contains(@class, 'article-share article')][1]/div/div[1]");
//    public By mainArticleSocialsTwitter =  By.xpath("//div[contains(@class, 'article-share article')][1]/div/div[2]");
//    public By mainArticleSocialsReddit = By.xpath("//div[contains(@class, 'article-share article')][1]/div/div[3]");
//    public By mainArticleSocialsEmail =  By.xpath("//div[contains(@class, 'article-share article')][1]/div/div[4]");
//
//    public By mainArticleNewsletter_Frame = By.xpath("//iframe[@id='subscriptionIframe']");
//    public By articleListElements = By.xpath("*//div[contains(@class, 'small-teaser-item teaser clearfix')]/div[1]/h3/a");
//    public By authorQueueAuthorName = By.xpath("//article[contains(@class, 'journalist')]/h1");
//
//    public By authorQueueAuthorEmail = By.xpath("//li[contains(@class, 'journalist-mail')]/a");
//    public By authorQueueAuthorTwitter = By.xpath("//li[contains(@class, 'journalist-twitter')]/a");
//    public By authorQueueArticles = By.xpath("//div[contains(@class, 'medium2col-teaser-item teaser clearfix')]");
//    public By authorQueue_TitlesList = By.xpath("//div[contains(@class, 'teaser-title')]/h2/a");
//    public By authorQueue_PublishDate = By.xpath("//div[contains(@class, 'teaser-extras')]/span[1]");
//    public By authorQueue_AuthorName = By.xpath("//div[contains(@class, 'teaser-extras')]/span[2]");
//    public By authorQueue_Summary = By.xpath("//div[contains(@class, 'teaser-content')]/p[2]");
//    public By authorQueue_Image = By.xpath("//div[contains(@class, 'teaser-image')][1]/a");
//    public By authorQueue_Header = By.xpath("//div[contains(@class, 'teaser-title')][1]/h2/a");
//    public By alternateAuthorQueueArticles = By.xpath("//div[contains(@class, 'content-a col-md-8')]/div");
//
//    // looping for article elements
//    public By header = By.xpath("//div[@class='inner-c col-md-12 article']/article/div/h1");

//    public By mainArticleImageHome = By.xpath("//div[contains(@class, 'large-teaser-item teaser clearfix')]/div[3]/a");
//    public By secondSocialElements = By.xpath("//div[@class='ad-experiences--bottom-slot']/div/div");
//    public By listOfTopicTags = By..article-image-captionxpath("//ul[contains(@class, 'nav nav-pills nav__pills__topics')]/li[1]");
//
//
//    public List<WebElement> articleLinks = driver.findElements(articleListElements);
//    public List<WebElement> businessLinks = driver.findElements(By.xpath("//div[contains(@class, 'teaser-title')]"));
//    public List<WebElement> authorQueueArticles_List = driver.findElements(authorQueue_TitlesList);
//    public List<WebElement> alternateListAuthorQueueArticles = driver.findElements(alternateAuthorQueueArticles);
//    public List<WebElement> authorQueue_ArticleContents = driver.findElements(By.xpath("medium2col-teaser-item teaser clearfix"));
//
//    public static ArrayList<By> articleList = new ArrayList<By>(); {
//        By list = By.xpath("//div[contains(@class, 'small-teaser-item teaser clearfix')]/div[1]/h3/a");
//        articleList.add(list);
//    }
//
//
//    public static ArrayList<By> articleSocialLinks = new ArrayList<By>(); {
//        By socialLinksElement = By.xpath("//*[@id='article-top']/div/div[1]/div/div");
//        articleSocialLinks.add(socialLinksElement);
//    }
//
//    public int randomNumber;
//
//
//    public List<By> links;
//
//
//    public void articleLoopSetup(int i) {
//        click(sportLink);
//        HomePage homePage = new HomePage(driver);
//        waitForElement(homeregisterWebElement);
//        By firstElement = links.get(i);
//        waitForClickableElement(firstElement);
//        scrollToElement(firstElement);
//        click(firstElement);
//        waitForElement(article.mainArticleHeaderDetail);
//    }
//
//    public void articleLoopContinue() {
//
//        By randomArticle = nav.get(randomNumber);
//        click(randomArticle);
//        waitForElement(register.registerWebElement);
//    }
//
//    public void verifyArticle_Title() {
//        for (int i = 0; i < articleLoopCount; i++) {
//            articleLoopSetup(i);
//
//            try {
//                waitForElement(article.mainArticleHeaderDetail);
//                isDisplayed(article.header);
//                articleElement++;
//            } catch (NoSuchElementException e) {
//                errorText("Title");
//            }
//
//            if (articleElement == 3) {
//                break;
//            } else {
//                articleLoopContinue();
//            }
//        }
//    }
//
//    public void verifyArticle_Author() {
//        for (int i = 0; i < articleLoopCount; i++) {
//
//            articleLoopSetup(i);
//
//            try {
//                try {
//                    isDisplayed(article.authorName);
//                    articleElement++;
//                } catch (NoSuchElementException e) {
//                    isDisplayed(article.otherAuthorName);
//                    articleElement++;
//                }
//
//            } catch (NoSuchElementException e) {
//                errorText("Author queue");
//            }
//
//            if (articleElement == 3) {
//                break;
//            } else {
//                articleLoopContinue();
//            }
//        }
//    }
//
//    public void verifyArticle_AuthorEmail() {
//        for (int i = 0; i < articleLoopCount; i++) {
//
//            articleLoopSetup(i);
//
//            try {
//                isDisplayed(article.authorEmail);
//                articleElement++;
//            } catch (NoSuchElementException e) {
//                errorText("author email");
//            }
//
//            if (articleElement == 3) {
//                break;
//            } else {
//                articleLoopContinue();
//            }
//        }
//    }
//
//
//    public void verifyArticle_LeadImage() {
//        for (int i = 0; i < articleLoopCount; i++) {
//
//            articleLoopSetup(i);
//
//            try {
//                try {
//                    isDisplayed(article.leadImage);
//                    isDisplayed(article.publishDate);
//                    articleElement++;
//
//                } catch (NoSuchElementException e) {
//                    isDisplayed(article.leadImage);
//                    isDisplayed(article.otherPublishDate);
//                    articleElement++;
//                }
//            } catch (NoSuchElementException e) {
//                errorText("lead image");
//            }
//
//            if (articleElement == 3) {
//                break;
//            } else {
//                articleLoopContinue();
//            }
//        }
//    }
//
//    public void verifyArticle_LeadImageCaption() {
//        for (int i = 0; i < articleLoopCount; i++) {
//
//            articleLoopSetup(i);
//
//            try {
//                isDisplayed(article.leadImage);
//                isDisplayed(article.mainArticleCaption);
//                articleElement++;
//
//            } catch (NoSuchElementException e) {
//                errorText("caption");
//            }
//
//            if (articleElement == 3) {
//                break;
//            } else {
//                articleLoopContinue();
//            }
//        }
//    }
//
//    public void verifyArticle_SummaryLeadImage() {
//        for (int i = 0; i < articleLoopCount; i++) {
//
//            articleLoopSetup(i);
//
//            try {
//                isDisplayed(article.leadImage);
//                isDisplayed(article.mainArticleSummary);
//                articleElement++;
//
//            } catch (NoSuchElementException e) {
//                errorText("summary");
//            }
//
//            if (articleElement == 3) {
//                break;
//            } else {
//                articleLoopContinue();
//            }
//        }
//    }
//
//    public void verifyArticle_SocialsFirstSet() {
//        for (int i = 0; i < articleLoopCount; i++) {
//
//            articleLoopSetup(i);
//
//            try {
//                isDisplayed(article.mainArticleSummary);
//                isDisplayed(article.firstSocialElements);
//                articleElement++;
//
//            } catch (NoSuchElementException e) {
//                errorText("first set of socials");
//            }
//
//            if (articleElement == 3) {
//                break;
//            } else {
//                articleLoopContinue();
//            }
//        }
//    }
//
//    public void verifyArticle_Newsletter() {
//        for (int i = 0; i < articleLoopCount; i++) {
//
//            articleLoopSetup(i);
//
//            try {
//                isDisplayed(firstSocialElements);
//                switchToFrame_Element(article.mainArticleNewsletter_Frame);
//                isDisplayed(article.mainArticleNewsletter);
//                articleElement++;
//
//            } catch (NoSuchElementException e) {
//                errorText("newsletter");
//            }
//
//            if (articleElement == 3) {
//                break;
//            } else {
//                articleLoopContinue();
//            }
//        }
//    }
//
//    public void verifyArticle_MainBody() {
//        for (int i = 0; i < articleLoopCount; i++) {
//
//            if (alreadyRun == false) {
//                articleLoopSetup(i);
//            }
//
//            isDisplayed(article.mainArticleSummary);
//            isDisplayed(article.bodyText);
//            articleElement++;
//
//            if (articleElement == 3) {
//                break;
//            } else {
//                alreadyRun = true;
//
//                articleLoopContinue();
//            }
//        }
//    }
//
//    public void verifyArticle_AdditionalImagesCaptions() {
//        for (int i = 0; i < articleLoopCount; i++) {
//
//            if (alreadyRun == false) {
//                articleLoopSetup(i);
//            }
//
//            try {
//                scrollToElement(article.additionalImage);
//                isDisplayed(article.additionalImage);
//                isDisplayed(article.additionalImageCaption);
//                articleElement++;
//            } catch (NoSuchElementException e) {
//                errorText("additional image");
//            }
//
//            if (articleElement == 3) {
//                break;
//            } else {
//                alreadyRun = true;
//
//                articleLoopContinue();
//            }
//        }
//    }
//
//    public void verifyArticle_TopicTags() {
//        for (int i = 0; i < articleLoopCount; i++) {
//
//            if (alreadyRun == false) {
//                articleLoopSetup(i);
//            }
//
//            try {
//                scrollToElement(article.secondSocialElements);
//                isDisplayed(article.listOfTopicTags);
//                articleElement++;
//
//            } catch (NoSuchElementException e) {
//                errorText("topic tags");
//            }
//
//            if (articleElement == 3) {
//                break;
//            } else {
//                alreadyRun = true;
//
//                articleLoopContinue();
//            }
//        }
//    }
//
//    public void verifyArticle_SocialsSecondSet() {
//        for (int i = 0; i < articleLoopCount; i++) {
//
//            if (alreadyRun == false) {
//                articleLoopSetup(i);
//            }
//
//            try {
//                scrollToElement(article.secondSocialElements);
//                isDisplayed(article.secondSocialElements);
//                articleElement++;
//            } catch (NoSuchElementException e) {
//                errorText("additional social links");
//            }
//
//            if (articleElement == 3) {
//                break;
//            } else {
//                alreadyRun = true;
//
//                articleLoopContinue();
//            }
//        }
//    }
//
//    public void verifyArticle_Disqus() throws InterruptedException {
//        for (int i = 0; i < articleLoopCount; i++) {
//
//            if (alreadyRun == false) {
//                articleLoopSetup(i);
//            }
//
//            scrollToElement(homePage.jobs24Widget);
//            Thread.sleep(5000);
//
//            try {
//                waitForElement(article.disqusElement);
//                switchToFrame_Element(By.xpath("//iframe[contains(@id, 'dsq-app')]"));
//                isDisplayed(article.disqusElement);
//                articleElement++;
//
//            } catch (NoSuchElementException e) {
//                errorText("comments");
//            }
//
//            if (articleElement == 3) {
//                break;
//            } else {
//                alreadyRun = true;
//
//                articleLoopContinue();
//            }
//        }
//    }
//
//    public void clickSocialButtons() throws InterruptedException {
//        for (int i = 0; i < loopCount; i++) {
//
//            if (alreadyRun == false) {
//                articleLoopSetup(i);
//            }
//
//            List<WebElement> socialLinksElement = driver.findElements(By.xpath("//*[@id='article-top']/div/div[1]/div/div"));
//            for (int j = 0; j < socialLinksElement.size(); j++) {
//                mainWindow = driver.getWindowHandle();
//                clickList(socialLinksElement.get(i));
//                Thread.sleep(2000);
//                ArrayList tabs = new ArrayList(driver.getWindowHandles());
//                driver.switchTo().window(String.valueOf(tabs.get(1)));
//                secondWindow = driver.getWindowHandle();
//                waitForElement(genericSelector());
//                driver.close();
//                driver.switchTo().window(mainWindow);
//                articleElement++;
//            }
//
//        if (articleElement == 3) {
//            break;
//        } else {
//            alreadyRun = true;
//
//            articleLoopContinue();
//            }
//        }
//    }
//
//    public void clickAuthorEmailLink() {
//        for (int i = 0; i < articleLoopCount; i++) {
//
//            articleLoopSetup(i);
//
//            try {
//                isDisplayed(mainArticleAuthor);
//                click(mainArticleAuthor);
//
//                waitForElement(register.registerWebElement);
//                isClickable(authorEmail);
//                articleElement++;
//
//            } catch (NoSuchElementException e) {
//                System.out.println("No author queue was found for article: ");
//            }
//
//            if (articleElement == 3) {
//                break;
//            } else {
//                articleLoopContinue();
//            }
//        }
//    }
//
//    public void checkAuthorQueueResponsiveness() {
//
//        isDisplayed(article.authorQueueAuthorName);
//        isDisplayed(article.authorQueueAuthorEmail);
//        isDisplayed(article.authorQueueAuthorTwitter);
//
//        minimizeWindow();
//
//        isDisplayed(article.authorQueueAuthorName);
//        isDisplayed(article.authorQueueAuthorEmail);
//        isDisplayed(article.authorQueueAuthorTwitter);
//
//    }
//
//    public void clickAuthorTwitterLink() throws InterruptedException {
//        for (int i = 0; i < loopCount; i++) {
//
//            articleLoopSetup(i);
//
//            try {
//                waitForElement(mainArticleSocialsTwitter);
//                click(mainArticleSocialsTwitter);
//
//                Thread.sleep(2000);
//
//                ArrayList tabs = new ArrayList(driver.getWindowHandles());
//                driver.switchTo().window(String.valueOf(tabs.get(1)));
//                secondWindow = driver.getWindowHandle();
//
//                waitForElement(genericSelector());
//
//                driver.close();
//                driver.switchTo().window(mainWindow);
//                articleElement++;
//
//            } catch (org.openqa.selenium.NoSuchElementException e) {
//                System.out.println("Author did not have a Twitter link");
//            }
//
//            if (articleElement == 3) {
//                break;
//            } else {
//                articleLoopContinue();
//            }
//        }
//    }
//
//    public void authorQueueVerifyAsset(By locator) {
//        waitForClickableElement(locator);
//
//        summaryText = driver.findElement(article.authorQueue_Header).getText();
//        click(locator);
//
//        waitForElement(article.mainArticleHeaderDetail);
//
//        detailText = driver.findElement(article.mainArticleHeaderDetail).getText();
//
//        assertEquals(summaryText, detailText);
//    }
//
//    public By mostReadQueue() {
//        return By.xpath("//div[@class='story-list most-read teaser-counter title-underline visible-md']");
//    }
//
//    public By sidebarAdvert1() {
//        return By.xpath("//div[@class='advert desktop advert-autosize'][2]");
//    }
//
//    public By sidebarDriver24Widget() {
//        return By.xpath("//div[@class='lp_drive24 text-center']");
//    }
//
//    public By obstructingAd() {
//        return By.xpath("//div[@class='jpx-cf-close']");
//    }
//
//    public By planningFinderWidget() {
//        return By.xpath("//div[@id='pfWidget']");
//    }
//
//
//    public void authorQueueClickTwitterLink() throws InterruptedException {
//        for (int i = 0; i < articleLoopCount; i++) {
//
//            articleLoopSetup(i);
//
//            try {
//                isDisplayed(mainArticleAuthor);
//                click(mainArticleAuthor);
//
//                waitForElement(register.registerWebElement);
//                mainWindow = driver.getWindowHandle();
//
//                waitForClickableElement(authorQueueAuthorTwitter);
//                scrollToElement(authorQueueAuthorTwitter);
//                click(authorQueueAuthorTwitter);
//
//                Thread.sleep(2000);
//
//                ArrayList tabs = new ArrayList(driver.getWindowHandles());
//                driver.switchTo().window(String.valueOf(tabs.get(1)));
//                secondWindow = driver.getWindowHandle();
//
//                waitForElement(genericSelector());
//
//                driver.close();
//                driver.switchTo().window(mainWindow);
//                articleElement++;
//
//            } catch (NoSuchElementException e) {
//                System.out.println("No author queue was found");
//                alreadyRun = true;
//                By randomArticle = nav.get(randomNumber);
//                click(randomArticle);
//                waitForElement(register.registerWebElement);
//            }
//
//            if (articleElement == 3) {
//                break;
//            } else {
//                articleLoopContinue();
//            }
//        }