package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SendUsAStory extends BasePage {

    public SendUsAStory(WebDriver driver) {
        super(driver);
    }

    public By form_Top = By.cssSelector("#send-a-story-wrap");
    public By sendUsAStoryHeaderLink = By.xpath("//h1[text()='Send Us a Story ']");
    public By nameText = By.xpath("//label[text()='Name']");
    public By emailText = By.id("frm-email");
    public By telephoneText = By.xpath("//label[text()='Telephone']");
    public By storyDetailsText = By.xpath("//label[text()='Story details']");
    public By briefSummaryText = By.xpath("//label[text()='Brief summary']");
    public By sendUsAStoryInput_ReCaptcha = By.cssSelector(".g-recaptcha");
    public By sendUsAStoryInput_SubmitButton = By.name("submit");
    public By termsAndConditionsCheckbox = By.name("terms");
    public By termsAndConditionsParagraph = By.cssSelector(".highlight.acceptTerms div label");
    public By sendUsAStoryInput_TermsLink = By.cssSelector(".highlight.acceptTerms div label a");
    public By gdprHeader = By.cssSelector(".gdpr-notice p");
    public By photoSection = By.cssSelector(".photo-details.col-sm-12");
    public By audioSection = By.cssSelector(".highlight.audio-details.col-sm-12");
    public By videoSection = By.cssSelector(".video-details.col-sm-12");

}