package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TellUsAboutAnEvent extends BasePage {

    public TellUsAboutAnEvent(WebDriver driver) {
        super(driver);
    }

    public By emailText = By.id("frm-email");
    public By passwordText = By.id("frm-password");
    public By submitButton = By.name("submit");
    public By headerText = By.xpath("//h1[text()='Tell us about your event']");
    public By loginLabel = By.xpath("//div[@class='introduction col-md-12']/div/p");

    By eventNameLabel = By.xpath("//label[@for='frm-event-title']");
    By eventNameText = By.xpath("//input[@name='eventName']");
    By datesLabel = By.xpath("//label[@for='frm-event-dates']");
    By datesText = By.xpath("//input[@name='dates']");
    By startTimeLabel = By.xpath("//label[text()='Start time']");
    By startTimeHrsText = By.id("startTimeHrs");
    By startTimeMinsText = By.id("startTimeMins");
    By endTimeLabel = By.xpath("//label[@for='frm-time-from'][1]");
    By endTimeHrsText = By.id("endTimeHrs");
    By endTimeMinsText = By.id("endTimeMins");
    By eventTypeLabel = By.xpath("//label[@for='frm-genre']");
    By eventTypeDropdown = By.id("frm-category");
    By pricingLabel = By.xpath("//label[@for='frm-price']");
    By pricingDropdown = By.id("frm-ticketType1");
    By venueLabel = By.xpath("//label[@for='frm-venue']");
    By venueText = By.id("frm-venue");
    By descriptonLabel = By.xpath("//label[@for='frm-event-description']");
    By description = By.id("frm-event-description");
    By eventContactNameLabel = By.xpath("//label[@for='frm-contact-name']");
    By eventContactNameText = By.id("frm-contactName");
    By emailLabel_Form = By.id("frm-contactEmail");
    By emailText_Form = By.xpath("//label[@for='frm-contactEmail']");
    By phoneNumberLabel = By.xpath("//label[@for='frm-contactNumber']");
    By phoneNumberText = By.id("frm-contactNumber");
    By websiteLabel = By.xpath("//label[@for='frm-website']");
    By websiteText = By.id("frm-website");

    public void entireForm() {
            isDisplayed(eventNameLabel);
            isDisplayed(eventNameText);
            isDisplayed(datesLabel);
            isDisplayed(datesText);
            isDisplayed(startTimeLabel);
            isDisplayed(startTimeHrsText);
            isDisplayed(startTimeMinsText);
            isDisplayed(endTimeLabel);
            isDisplayed(endTimeHrsText);
            isDisplayed(endTimeMinsText);
            isDisplayed(eventTypeLabel);
            isDisplayed(eventTypeDropdown);
            isDisplayed(pricingLabel);
            isDisplayed(pricingDropdown);
            isDisplayed(pricingLabel);
            isDisplayed(venueLabel);
            isDisplayed(venueText);
            isDisplayed(descriptonLabel);
            isDisplayed(description);
            isDisplayed(eventContactNameLabel);
            isDisplayed(eventContactNameText);
            isDisplayed(phoneNumberLabel);
            isDisplayed(phoneNumberText);
            isDisplayed(emailLabel_Form);
            isDisplayed(emailText_Form);
            isDisplayed(websiteLabel);
            isDisplayed(websiteText);
    }

    public void fillInForm(String tempMailEmail) {
        enterText(emailText, tempMailEmail);
        Register register = new Register(driver);
        enterText(passwordText, register.password);
    }
}
