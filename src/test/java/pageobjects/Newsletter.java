package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

public class Newsletter extends BasePage {

    public Newsletter(WebDriver driver) {
        super(driver);
    }

    private By newsletterWidget = By.xpath("//div[@class='subscription-widget']");
    private By newsletter_PrivacyLink = By.xpath("//a[text()='Our Privacy Policy']");
    private By newsletterSubjectHeader = By.linkText("Eastern Daily Press");
    private By newsletterEmailBody = By.xpath("//div[@class='inbox-data-content-intro']/div/div/a");
    private By mainArticleNewsletter_Heading = By.xpath("//form_Top[@class='subscription-widget__form form_Top-horizontal']/p");
    private By mainArticleNewsletter_SubmitButton = By.xpath("//button[@class='btn btn-default subscription-widget__submit']");

}
