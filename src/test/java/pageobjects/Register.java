package pageobjects;

import net.bytebuddy.utility.RandomString;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

public class Register extends BasePage {

    public Register(WebDriver driver) {
        super(driver);
    }

    private By gdprHeader = By.cssSelector(".gdpr-notice p");
    private By registerForm = By.id("registration-form");
    private By registerButton = By.name("submit");
    private By errorHeading = By.cssSelector(".errors p");
    private By firstError = By.cssSelector(".errors ul li:nth-of-type(1)");
    private By secondError = By.cssSelector(".errors ul li:nth-of-type(2)");
    private By thirdError = By.cssSelector(".errors ul li:nth-of-type(3)");
    private By fourthError = By.cssSelector(".errors ul li:nth-of-type(4)");
    private By fifthError = By.cssSelector(".errors ul li:nth-of-type(5)");
    private By sixthError = By.cssSelector(".errors ul li:nth-of-type(6)");
    private By seventhError = By.cssSelector(".errors ul li:nth-of-type(7)");
    private By eightError = By.cssSelector(".errors ul li:nth-of-type(8)");
    private By ninthError = By.cssSelector(".errors ul li:nth-of-type(9)");
    private By tenthError = By.cssSelector(".errors ul li:nth-of-type(10)");
    private final String[] TITLE = new String[]{"Mr", "Miss", "Mrs", "Ms", "Dr"};
    private final String[] FIRST_NAME = new String[]{"James", "Sophie", "Adam", "Louise", "Tom"};
    private final String[] SURNAME = new String[]{"Richardson", "Smith", "Jenkins", "Larssen", "Williamson"};
    private final String[] SCREEN_NAME = new String[]{"42", "10", "22", "15", "59"};

    private String title;
    private String firstName;
    private String surname;
    private String screenName;

    public String getRandomTitle() {
        int index = randomNumber(TITLE.length);
        return title = (TITLE[index]);
    }

    public String getRandomFirstName() {
        int index = randomNumber(FIRST_NAME.length);
        return firstName = (FIRST_NAME[index]);
    }

    public String getRandomLastName() {
        int index = randomNumber(SURNAME.length);
        return surname = (SURNAME[index]);
    }

    public String getRandomScreenName() {
        int index = randomNumber(SCREEN_NAME.length);
        return firstName + (screenName = (SCREEN_NAME[index]));
    }

    public String getInvalidRandomScreenName() {
        int index = randomNumber(SCREEN_NAME.length);
        return "!" + firstName + " " + (screenName = (SCREEN_NAME[index]));
    }

//    private String title = "Mr";
//    private String firstName = RandomString.make(4);
//    private String Surname = RandomString.make(10);
//    private String screenName = RandomString.make(5);
//    private String emailAddress = RandomString.make(5) + "@" + RandomString.make(5) + ".com";
//    private String invalidScreenName = "!" + RandomString.make(5);
    private By personalDetailsText = By.cssSelector(".personal-details.col-sm-12.col-md-6 h2");
    private By emailAndPasswordText = By.cssSelector(".email-password-details.col-sm-12.col-md-6 h2");
    private By titleLabel = By.cssSelector("label[for=frm-register-title]");
    private By titleEntry = By.id("frm-register-title");
    private By firstNameLabel = By.cssSelector("label[for=frm-register-firstname]");
    private By firstNameEntry = By.id("frm-register-firstname");
    private By surnameLabel = By.cssSelector("label[for=frm-register-surname]");
    private By surnameEntry = By.id("frm-register-surname");
    private By screenNameLabel = By.cssSelector("label[for=frm-register-screenname]");
    private By screenNameEntry = By.id("frm-register-screenname");
    private By screenNameHelperText = By.xpath("//p[text()='Screen names can only contain alphanumeric characters, combined with either the _ or - character. Your screen name will be used to identify you publicly, for example next to comments']");
    private By emailAddressLabel = By.cssSelector("label[for=frm-register-email]");
    private By emailAddressEntry = By.id("frm-register-email");
    private By passwordLabel = By.cssSelector("label[for=frm-register-password]");
    private By passwordEntry = By.id("frm-register-password");
    private By confirmPasswordLabel = By.cssSelector("label[for=frm-register-confirmpassword]");
    private By confirmPasswordEntry = By.id("frm-register-confirmpassword");
    private By newsletterLabel = By.cssSelector("label[for=frm-recive-email]");
    private By newsletterCheckbox = By.id("frm-recive-email");
    private By promotionsLabel = By.cssSelector("label[for=frm-recive-post]");
    private By promotionsCheckbox = By.id("frm-recive-post");
    private By thirdPartiesLabel = By.cssSelector("label[for=frm-recieve-email-trusted");
    private By thirdPartiesCheckbox = By.id("frm-recieve-email-trusted");
    private By termsAndConditionsLabel = By.cssSelector("label[for=frm-register-terms]");
    private By termsAndConditionsCheckbox = By.id("frm-register-terms");
    private By registerSuccessElement = By.cssSelector(".message-panel h2");
    private By privacyLink = By.cssSelector(".gdpr-notice a:nth-of-type(1)");
    private By termsLink = By.cssSelector("#acceptTerms a");
    private By emailWebElement = By.id("frm-login-email-address");
    private By passwordWebElement = By.id("frm-login-password");
    private By loginButtonWebElement = By.xpath("(//div[contains(@class, 'col-sm-offset-2 col-sm-10')]/button)");
    private By loginWebElement = By.xpath("//div[contains(@id, 'login-links')]/a[1]");
    private By registerWebElement = By.xpath("//div[contains(@id, 'login-links')]/a[2]");
    private String registerUrl = "register";
    private By termsAndConditionsErrorText = By.xpath("//div[contains(@id, 'ClientValidationSummary')]/ul/li[1]/a");
    private String registerSuccessText = "Thanks for registering";
    private By logoutText = By.linkText("Log out");
    private By tempMailCookiesPopup = By.xpath("//div[contains(@class, 'cc_banner cc_container cc_container--open')]/a[1]");
    private By registerConfirmationElement = By.xpath("//h2[contains(text(), 'Congratulations! Your account is now active')]");
    private By titleBy = By.id("frm-register-title");

    public void waitForErrorText() {
        waitForElement(errorHeading);
    }

    public void waitForRegisterForm() {
        waitForElement(registerForm);
    }

    public void clickRegisterButton() {
        click(registerButton);
    }

    public void clickTermsLink(String url) {
        mainWindow = driver.getWindowHandle();
        click(termsLink);
        ArrayList tabs = new ArrayList(driver.getWindowHandles());
        waitForWindowSize(2);
        driver.switchTo().window(String.valueOf(tabs.get(1)));
        secondWindow = driver.getWindowHandle();
        assert driver.getCurrentUrl().equals(url);
        driver.close();
        waitForWindowSize(1);
        driver.switchTo().window(mainWindow);
    }

    public void clickPrivacyLink(String url) {
        mainWindow = driver.getWindowHandle();
        click(privacyLink);
        ArrayList tabs = new ArrayList(driver.getWindowHandles());
        waitForWindowSize(2);
        driver.switchTo().window(String.valueOf(tabs.get(1)));
        secondWindow = driver.getWindowHandle();
        assert driver.getCurrentUrl().equals(url);
        driver.close();
        waitForWindowSize(1);
        driver.switchTo().window(mainWindow);
    }

    public void setTitle(String text) {
        selectDropdown(titleEntry, text);
    }

    public void setFirstName(String text) {
        enterText(firstNameEntry, text);
    }

    public void setSurname(String text) {
        enterText(surnameEntry, text);
    }

    public void setScreenName(String text) {
        enterText(screenNameEntry, text);
    }

    public void setEmail(String text) {
        enterText(emailAddressEntry, text);
    }

    public void setPassword(String text) {
        enterText(passwordEntry, text);
    }

    public void setConfirmPassword(String text) {
        enterText(confirmPasswordEntry, text);
    }

    public void checkNewsletterCheckbox() {
        click(newsletterCheckbox);
    }

    public void checkPromotionsCheckbox() {
        click(promotionsCheckbox);
    }

    public void checkThirdPartiesCheckbox() {
        click(thirdPartiesCheckbox);
    }

    public void checkTermsCheckbox() {
        click(termsAndConditionsCheckbox);
    }

    public Boolean checkGDPRHeaderIsDisplayed() {
        try {
            isDisplayed(gdprHeader);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("GDPR Header not visible");
            return false;
        }
    }

    public String getGDPRHeaderText() {
        return getText(gdprHeader);
    }

    public Boolean checkPersonDetailsHeaderIsDisplayed() {
        try {
            isDisplayed(personalDetailsText);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Personal Details text not visible");
            return false;
        }
    }

    public String getPersonalDetailsHeaderText() {
        return getText(personalDetailsText);
    }

    public Boolean checkEmailAndPasswordHeaderIsDisplayed() {
        try {
            isDisplayed(emailAndPasswordText);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Email and password text not visible");
            return false;
        }
    }

    public String getEmailAndPasswordText() {
        return getText(emailAndPasswordText);
    }

    public Boolean checkTitleLabelIsDisplayed() {
        try {
            isDisplayed(titleLabel);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Title header text not visible");
            return false;
        }
    }

    public String getTitleLabel() {
        return getText(titleLabel);
    }

    public Boolean checkTitleIsDisplayed() {
        try {
            isDisplayed(titleEntry);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Email and password text not visible");
            return false;
        }
    }

    public Boolean checkFirstNameLabelIsDisplayed() {
        try {
            isDisplayed(firstNameLabel);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Title header text not visible");
            return false;
        }
    }

    public String getFirstNameLabel() {
        return getText(firstNameLabel);
    }

    public Boolean checkFirstNameIsDisplayed() {
        try {
            isDisplayed(firstNameEntry);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("First name text field not visible");
            return false;
        }
    }

    public Boolean checkSurnameLabelIsDisplayed() {
        try {
            isDisplayed(surnameLabel);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Surname header not visible");
            return false;
        }
    }

    public String getSurnameLabel() {
        return getText(surnameLabel);
    }

    public Boolean checkSurnameIsDisplayed() {
        try {
            isDisplayed(surnameEntry);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Surname text field not visible");
            return false;
        }
    }

    public Boolean checkScreenNameLabelIsDisplayed() {
        try {
            isDisplayed(screenNameLabel);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Screen name header not visible");
            return false;
        }
    }

    public String getScreenName() {
        return getText(screenNameLabel);
    }

    public Boolean checkScreenNameIsDisplayed() {
        try {
            isDisplayed(screenNameEntry);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Surname text field not visible");
            return false;
        }
    }

    public String getScreeNameHelperText() {
        return getText(screenNameHelperText);
    }

    public Boolean checkScreenNameHelperTextIsDisplayed() {
        try {
            isDisplayed(screenNameHelperText);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Screen name helper text not visible");
            return false;
        }
    }

    public Boolean checkEmailAddressTextIsDisplayed() {
        try {
            isDisplayed(emailAddressLabel);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Email address header not visible");
            return false;
        }
    }

    public String getEmailAddress() {
        return getText(emailAddressLabel);
    }

    public Boolean checkEmailAddressIsDisplayed() {
        try {
            isDisplayed(emailAddressEntry);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Email address field not visible");
            return false;
        }
    }

    public Boolean checkPasswordTextIsDisplayed() {
        try {
            isDisplayed(passwordLabel);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Password header not visible");
            return false;
        }
    }

    public String getPassword() {
        return getText(passwordLabel);
    }

    public Boolean checkPasswordIsDisplayed() {
        try {
            isDisplayed(passwordEntry);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Email address field not visible");
            return false;
        }
    }

    public Boolean checkConfirmPasswordTextIsDisplayed() {
        try {
            isDisplayed(confirmPasswordLabel);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Confirm password header not visible");
            return false;
        }
    }

    public String getConfirmPassword() {
        return getText(confirmPasswordLabel);
    }

    public Boolean checkConfirmPasswordIsDisplayed() {
        try {
            isDisplayed(confirmPasswordEntry);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Confirm password field not visible");
            return false;
        }
    }

    public Boolean checkNewsletterLabelIsDisplayed() {
        try {
            isDisplayed(newsletterLabel);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Newsletter label not visible");
            return false;
        }
    }

    public String getNewsletterLabelText() {
        return getText(newsletterLabel);
    }

    public Boolean checkNewsletterCheckboxIsDisplayed() {
        try {
            isDisplayed(newsletterCheckbox);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Confirm password field not visible");
            return false;
        }
    }

    public Boolean checkThirdPartiesLabelIsDisplayed() {
        try {
            isDisplayed(thirdPartiesLabel);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Third parties label not visible");
            return false;
        }
    }

    public String getThirdPartiesLabelText() {
        return getText(thirdPartiesLabel);
    }

    public Boolean checkThirdPartiesCheckboxIsDisplayed() {
        try {
            isDisplayed(thirdPartiesCheckbox);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Third parties checkbox field not visible");
            return false;
        }
    }

    public Boolean checkPromotionsLabelIsDisplayed() {
        try {
            isDisplayed(promotionsLabel);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Promotions label not visible");
            return false;
        }
    }

    public String getPromotionsLabelText() {
        return getText(promotionsLabel);
    }

    public Boolean checkPromotionsCheckboxIsDisplayed() {
        try {
            isDisplayed(promotionsCheckbox);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Promotions checkbox field not visible");
            return false;
        }
    }

    public Boolean checkTermsAndConditionsLabelIsDisplayed() {
        try {
            isDisplayed(termsAndConditionsLabel);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Terms and conditions label not visible");
            return false;
        }
    }

    public String getTermsAndConditionsLabelText() {
        return getText(termsAndConditionsLabel);
    }

    public Boolean checkTermsAndConditionsCheckboxIsDisplayed() {
        try {
            isDisplayed(termsAndConditionsCheckbox);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Terms and conditions checkbox field not visible");
            return false;
        }
    }

    public String getRegisterButtonText() {
        return getText(registerButton);
    }

    public Boolean checkRegisterButtonIsDisplayed() {
        try {
            isDisplayed(registerButton);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Register button not visible");
            return false;
        }
    }

    public String getErrorHeaderText() {
        return getText(errorHeading);
    }

    public Boolean checkErrorHeadingTextIsDisplayed() {
        try {
            isDisplayed(errorHeading);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Error heading not visible");
            return false;
        }
    }

    public String getFirstError() {
        return getText(firstError);
    }

    public Boolean checkFirstErrorIsDisplayed() {
        try {
            isDisplayed(firstError);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Error heading not visible");
            return false;
        }
    }

    public String getSecondError() {
        return getText(secondError);
    }

    public Boolean checkSecondErrorIsDisplayed() {
        try {
            isDisplayed(secondError);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Error heading not visible");
            return false;
        }
    }

    public String getThirdError() {
        return getText(thirdError);
    }

    public Boolean checkThirdErrorIsDisplayed() {
        try {
            isDisplayed(thirdError);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Error heading not visible");
            return false;
        }
    }

    public String getFourthError() {
        return getText(fourthError);
    }

    public Boolean checkFourthErrorIsDisplayed() {
        try {
            isDisplayed(fourthError);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Error heading not visible");
            return false;
        }
    }

    public String getFifthError() {
        return getText(fifthError);
    }

    public Boolean checkFifthErrorIsDisplayed() {
        try {
            isDisplayed(fifthError);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Error heading not visible");
            return false;
        }
    }

    public String getSixthError() {
        return getText(sixthError);
    }

    public Boolean checkSixthErrorIsDisplayed() {
        try {
            isDisplayed(sixthError);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Error heading not visible");
            return false;
        }
    }

    public String getSeventhError() {
        return getText(seventhError);
    }

    public Boolean checkSeventhErrorIsDisplayed() {
        try {
            isDisplayed(seventhError);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Error heading not visible");
            return false;
        }
    }

    public String getEightError() {
        return getText(eightError);
    }

    public Boolean checkEighthErrorIsDisplayed() {
        try {
            isDisplayed(eightError);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Error heading not visible");
            return false;
        }
    }

    public String getNinthError() {
        return getText(ninthError);
    }

    public Boolean checkNinthErrorIsDisplayed() {
        try {
            isDisplayed(ninthError);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Error heading not visible");
            return false;
        }
    }

    public String getTenthError() {
        return getText(tenthError);
    }

    public Boolean checkTenthErrorIsDisplayed() {
        try {
            isDisplayed(tenthError);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Error heading not visible");
            return false;
        }
    }

    public String getScreenNameError() {
        return getText(firstError);
    }

    public Boolean checkScreenNameErrorText() {
        try {
            waitForElement(firstError);
            isDisplayed(firstError);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Error heading not visible");
            return false;
        }
    }

    public String getTermsError() {
        return getText(firstError);
    }

    public Boolean checkTermsErrorText() {
        try {
            waitForElement(firstError);
            isDisplayed(firstError);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Error heading not visible");
            return false;
        }
    }
}