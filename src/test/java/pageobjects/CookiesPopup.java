package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

public class CookiesPopup extends BasePage {

    public CookiesPopup(WebDriver driver) {
        super(driver);
    }

    private By headerText = By.xpath("//div[text()='Cookie Policy']");
    private By iAcceptButton = By.xpath("//button[text()='I Accept']");
    private By showOptionsLink = By.xpath("//button[text()='Show Options']");
    private By bodyText = By.xpath("//p[text()='By enabling cookies you will help us to fund the journalism you enjoy, safeguarding it for the future and personalising your content across all Archant Community Media Ltd owned brands.  Thank you for your support. ']");
    private By cookiesFrame = By.xpath("//iframe[contains(@src,'sourcepoint')]");
    private By cancelButton = By.id("tab-cancel");

    public void loadPage() {
        fetch(EASTERN_DAILY_PRESS);
        waitForElement(headerText);
    }

    public Boolean checkHeaderTextIsDisplayed() {
        try {
            isDisplayed(headerText);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Header text not visible");
        }
        return false;
    }

    public Boolean checkBodyTextIsDisplayed() {
        try {
            isDisplayed(bodyText);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Main text not visible");
        }
        return false;
    }

    public Boolean checkAcceptButtonIsDisplayed() {
        try {
            isDisplayed(iAcceptButton);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Accept button not visible");
        }
        return false;
    }

    public Boolean checkShowOptionsTextIsDisplayed() {
        try {
            isDisplayed(showOptionsLink);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Show options link not visible");
        }
        return false;
    }

    public void clickShowOptions() {
        click(showOptionsLink);
        waitForFrameAndSwitch(cookiesFrame);
        waitForElement(cancelButton);
    }

    public void clickCancelButton() {
        click(cancelButton);
    }

    public void clickAcceptButton() {
        click(iAcceptButton);
    }
}