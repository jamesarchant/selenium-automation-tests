package pageobjects;

import org.openqa.selenium.*;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class AuthorQueue extends BasePage {

    public AuthorQueue(WebDriver driver) {
        super(driver);
    }

    private By authorQueueSection = By.cssSelector(".journalist");
    private By authorQueueTwitter = By.cssSelector(".journalist-twitter a");
    private By authorQueueSocials = By.cssSelector(".utilities.clearfix");
    private By authorQueueName = By.cssSelector(".journalist h1");
    private List<WebElement> articleQueue = driver.findElements(By.cssSelector(".medium2col-teaser-item.teaser.clearfix"));
    private By authorQueueEmail = By.cssSelector(".journalist-mail a");
    private By authorQueueHeadings = By.cssSelector(".teaser-title");
    private By authorQueueImages = By.cssSelector(".teaser-images");
    private By authorQueueArticleName = By.cssSelector(".byline a");

    public static final String[] AUTHOR_QUEUE_URLS = new String[]{"https://www.facebook.com/", "https://twitter.com/", "https://www.reddit.com/"};

    public String summaryAuthor;

    public Boolean checkAuthorQueueNameIsDisplayed() {
        try {
            waitForElement(authorQueueName);
            isDisplayed(authorQueueName);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Author queue name not displayed");
        }
        return false;
    }

    public Boolean checkAuthorQueueEmailIsDisplayed() {
        try {
            waitForClickableElement(authorQueueEmail);
            isDisplayed(authorQueueEmail);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Author queue email not displayed");
        }
        return false;
    }

    public Boolean checkAuthorQueueImages() {
        try {
            for (WebElement image : articleQueue) {
                image = driver.findElement(By.cssSelector(".teaser-image div img"));
                image.isDisplayed();
            }
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Author queue headers not visible");
        }
        return false;
    }

    public Boolean checkAuthorQueueHeaders() {
        try {
            for (WebElement header : articleQueue) {
                header = driver.findElement(By.cssSelector(".teaser-title h2 a"));
                header.isDisplayed();
            }
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Author queue headers not visible");
        }
        return false;
    }

    public Boolean checkAuthorQueueAuthorArticleNames() {
        try {
            for (WebElement authorName : articleQueue) {
                authorName = driver.findElement(By.cssSelector(".byline a"));
                authorName.isDisplayed();
            }
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Author queue headers not visible");
        }
        return false;
    }

    public Boolean checkAuthorQueueSummaries() {
        try {
            for (WebElement summary : articleQueue) {
                summary = driver.findElement(By.cssSelector(".teaser-content p:nth-child(2)"));
                summary.isDisplayed();
            }
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Author queue headers not visible");
        }
        return false;
    }

    public String getAuthorName() {
        return getText(authorQueueName);
    }

    public void clickAuthorQueueTwitterLink() throws InterruptedException {
        Thread.sleep(2000);
        click(authorQueueTwitter);
        mainWindow = driver.getWindowHandle();
        Thread.sleep(2000);
        ArrayList tabs = new ArrayList(driver.getWindowHandles());
        driver.switchTo().window(String.valueOf(tabs.get(1)));

        driver.switchTo().window(driver.getWindowHandle());
        assert (driver.getCurrentUrl().contains("https://twitter.com/"));
        driver.close();
        driver.switchTo().window(mainWindow);
    }

    public void clickAuthorQueueSocials() throws InterruptedException {
        for (int j = 0; j < 3; j++) {
            scrollToElement(authorQueueSocials);
            List<WebElement> authorQueueSocialElements = driver.findElements(By.cssSelector(".utilities.clearfix div a"));
            WebElement socialElement = authorQueueSocialElements.get(j);
            scrollToElementList(socialElement);
            mainWindow = driver.getWindowHandle();
            socialElement.click();
            Thread.sleep(2000);
            ArrayList tabs = new ArrayList(driver.getWindowHandles());
            driver.switchTo().window(String.valueOf(tabs.get(1)));
            secondWindow = driver.getWindowHandle();
            driver.switchTo().window(secondWindow);
            assert (driver.getCurrentUrl().contains(AUTHOR_QUEUE_URLS[j]));
            driver.close();
            driver.switchTo().window(mainWindow);
            loopCount++;
        }
    }

    public String clickRandomAuthorQueueTitle() {
        randomNumber = randomNumber(driver.findElements(authorQueueHeadings).size());
        WebElement randomArticle = driver.findElements(authorQueueHeadings).get(randomNumber);
        WebElement randomAuthor = driver.findElements(authorQueueArticleName).get(randomNumber);
        summaryAuthor = randomAuthor.getText();
        scrollToElementList(randomArticle);
        clickList(randomArticle);
        Article article = new Article(driver);
        assertTrue(article.checkHeaderIsDisplayed());
        assertTrue(article.checkLeadImageIsDisplayed());
        assertTrue(article.checkLeadImageCaptionIsDisplayed());
        assertTrue(article.checkSummaryIsDisplayed());
        assertTrue(article.checkBodyTextIsDisplayed());
        return summaryAuthor;
    }

    public void clickRandomAuthorQueueImage() {
        randomNumber = randomNumber(driver.findElements(authorQueueImages).size());
        WebElement randomImage = driver.findElements(authorQueueImages).get(randomNumber);
        WebElement randomAuthor = driver.findElements(authorQueueArticleName).get(randomNumber);
        summaryAuthor = randomAuthor.getText();
        scrollToElementList(randomImage);
        clickList(randomImage);
        Article article = new Article(driver);
        assertTrue(article.checkHeaderIsDisplayed());
        assertTrue(article.checkLeadImageIsDisplayed());
        assertTrue(article.checkLeadImageCaptionIsDisplayed());
        assertTrue(article.checkSummaryIsDisplayed());
        assertTrue(article.checkBodyTextIsDisplayed());
    }
}