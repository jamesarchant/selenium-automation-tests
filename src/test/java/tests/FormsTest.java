package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Allure;
import io.qameta.allure.junit4.DisplayName;
import org.apache.commons.io.FileUtils;
import org.junit.*;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pageobjects.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static pageobjects.BasePage.*;

public class FormsTest extends BaseTests{

    @Test
    @DisplayName("Confirm that the Send Us A Story section loads correctly, when clicked from the top navigation")
    public void navigateToForm_Top() {
        HomePage homePage = new HomePage(driver);
        homePage.click(homePage.sendUsAStoryLink_Top);

        SendUsAStory sendUsAStory = new SendUsAStory(driver);
        sendUsAStory.waitForElement(sendUsAStory.sendUsAStoryHeaderLink);
    }

    @Test
    @DisplayName("Verify the presence of the privacy message on the Send Us A Story section, when clicked from the top navigation")
    public void checkPrivacyMessageVisible_Top() {
        HomePage homePage = new HomePage(driver);
        homePage.click(homePage.sendUsAStoryLink_Top);

        SendUsAStory sendUsAStory = new SendUsAStory(driver);
        sendUsAStory.waitForElement(sendUsAStory.sendUsAStoryHeaderLink);
        assert (sendUsAStory.storeText(sendUsAStory.gdprHeader).startsWith("Archant Community Media Limited will only use your data"));
    }

    @Test
    @DisplayName("Verify the presence of the top input boxes on Send Us A Story, when clicked from the top navigation")
    public void checkInputBoxesVisible_Top() {
        HomePage homePage = new HomePage(driver);
        homePage.click(homePage.sendUsAStoryLink_Top);

        SendUsAStory sendUsAStory = new SendUsAStory(driver);
        sendUsAStory.waitForElement(sendUsAStory.sendUsAStoryHeaderLink);
        sendUsAStory.isDisplayed(sendUsAStory.nameText);
        sendUsAStory.isDisplayed(sendUsAStory.emailText);
        sendUsAStory.isDisplayed(sendUsAStory.telephoneText);
        sendUsAStory.isDisplayed(sendUsAStory.briefSummaryText);
        sendUsAStory.isDisplayed(sendUsAStory.storyDetailsText);
    }

    @Test
    @DisplayName("Verify the presence of the media upload buttons on Send Us A Story, when clicked from the top navigation")
    public void checkMediaUploadButtonsVisible_Top() {
        HomePage homePage = new HomePage(driver);
        homePage.click(homePage.sendUsAStoryLink_Top);

        SendUsAStory sendUsAStory = new SendUsAStory(driver);
        sendUsAStory.waitForElement(sendUsAStory.sendUsAStoryHeaderLink);
        sendUsAStory.isDisplayed(sendUsAStory.photoSection);
        sendUsAStory.isDisplayed(sendUsAStory.audioSection);
        sendUsAStory.isDisplayed(sendUsAStory.videoSection);
    }

    @Test
    @DisplayName("Verify the presence of the terms and conditions on Send Us A Story, when clicked from the top navigation")
    public void checkTermsAndConditionsVisible_Top() {
        HomePage homePage = new HomePage(driver);
        homePage.click(homePage.sendUsAStoryLink_Top);

        SendUsAStory sendUsAStory = new SendUsAStory(driver);
        sendUsAStory.waitForElement(sendUsAStory.sendUsAStoryHeaderLink);
        sendUsAStory.isDisplayed(sendUsAStory.termsAndConditionsParagraph);
        sendUsAStory.isDisplayed(sendUsAStory.termsAndConditionsCheckbox);
    }

    @Test
    @DisplayName("Verify the click functionality of the terms and conditions link on Send Us A Story, when clicked from the top navigation")
    public void checkTermsAndConditionsLinkIsClickable_Top() {
        HomePage homePage = new HomePage(driver);
        homePage.click(homePage.sendUsAStoryLink_Top);

        SendUsAStory sendUsAStory = new SendUsAStory(driver);
        sendUsAStory.isClickable(sendUsAStory.sendUsAStoryInput_TermsLink);
    }

    @Test
    @DisplayName("Verify the presence of the media upload buttons on Send Us A Story, when clicked from the top navigation")
    public void checkRecaptchaVisible_Top() {
        HomePage homePage = new HomePage(driver);
        homePage.click(homePage.sendUsAStoryLink_Top);

        SendUsAStory sendUsAStory = new SendUsAStory(driver);
        sendUsAStory.isDisplayed(sendUsAStory.sendUsAStoryInput_ReCaptcha);
    }

    @Test
    @DisplayName("Verify the presence of submit button on Send Us A Story, when clicked from the top navigation")
    public void checkSubmitButtonVisible_Top() {
        HomePage homePage = new HomePage(driver);
        homePage.click(homePage.sendUsAStoryLink_Top);

        SendUsAStory sendUsAStory = new SendUsAStory(driver);
        sendUsAStory.isDisplayed(sendUsAStory.sendUsAStoryInput_SubmitButton);
    }

    @Test
    @DisplayName("Verify the presence of the media upload buttons on Send Us A Story, when clicked from the top navigation")
    public void checkFormResponsiveness_Top() {
        HomePage homePage = new HomePage(driver);
        homePage.click(homePage.sendUsAStoryLink_Top);

        SendUsAStory sendUsAStory = new SendUsAStory(driver);
        sendUsAStory.waitForElement(sendUsAStory.form_Top);
        sendUsAStory.isDisplayed(sendUsAStory.nameText);
        sendUsAStory.isDisplayed(sendUsAStory.emailText);
        sendUsAStory.isDisplayed(sendUsAStory.telephoneText);
        sendUsAStory.isDisplayed(sendUsAStory.briefSummaryText);
        sendUsAStory.scrollToElement(sendUsAStory.storyDetailsText);
        sendUsAStory.isDisplayed(sendUsAStory.storyDetailsText);
        sendUsAStory.isDisplayed(sendUsAStory.photoSection);
        sendUsAStory.isDisplayed(sendUsAStory.audioSection);
        sendUsAStory.isDisplayed(sendUsAStory.videoSection);
        sendUsAStory.isDisplayed(sendUsAStory.termsAndConditionsParagraph);
        sendUsAStory.isDisplayed(sendUsAStory.termsAndConditionsCheckbox);
        sendUsAStory.isDisplayed(sendUsAStory.sendUsAStoryInput_ReCaptcha);
        sendUsAStory.isDisplayed(sendUsAStory.sendUsAStoryInput_SubmitButton);

        sendUsAStory.minimizeWindow();

        sendUsAStory.isDisplayed(sendUsAStory.nameText);
        sendUsAStory.isDisplayed(sendUsAStory.emailText);
        sendUsAStory.isDisplayed(sendUsAStory.telephoneText);
        sendUsAStory.isDisplayed(sendUsAStory.briefSummaryText);
        sendUsAStory.scrollToElement(sendUsAStory.storyDetailsText);
        sendUsAStory.isDisplayed(sendUsAStory.storyDetailsText);
        sendUsAStory.isDisplayed(sendUsAStory.photoSection);
        sendUsAStory.isDisplayed(sendUsAStory.audioSection);
        sendUsAStory.isDisplayed(sendUsAStory.videoSection);
        sendUsAStory.isDisplayed(sendUsAStory.termsAndConditionsParagraph);
        sendUsAStory.isDisplayed(sendUsAStory.termsAndConditionsCheckbox);
        sendUsAStory.isDisplayed(sendUsAStory.sendUsAStoryInput_ReCaptcha);
        sendUsAStory.isDisplayed(sendUsAStory.sendUsAStoryInput_SubmitButton);
    }

//    @Test
//    @DisplayName("Verify the click functionality of the terms and conditions link")
//    public void clickTermsAndConditionsLink_Top() {
//        HomePage homePage = new HomePage(driver);
//        homePage.click(homePage.sendUsAStoryLink_Top);
//
//        SendUsAStory sendUsAStory = new SendUsAStory(driver);
//        mainWindow = driver.getWindowHandle();
//        sendUsAStory.click(sendUsAStory.sendUsAStoryInput_TermsLink);
//        sendUsAStory.waitForElement(sendUsAStory.genericSelector());
//        ArrayList tabs = new ArrayList(driver.getWindowHandles());
//        driver.switchTo().window(String.valueOf(tabs.get(1)));
//        assert(driver.getCurrentUrl().equals("https://www.archant.co.uk/articles/terms-conditions/"));
//        secondWindow = driver.getWindowHandle();
//        driver.close();
//        driver.switchTo().window(mainWindow);
//    }

    @Test
    @DisplayName("Verify that the top tell us about an event page has a login form showing")
    public void checkFormFunctionalityUnlogged_Top() {
        HomePage homePage = new HomePage(driver);
        //homePage.isDisplayed(homePage.loginLink);
        //homePage.scrollToElement(homePage.copyrightElement);
        homePage.click(homePage.tellUsAboutAnEventLink);

        TellUsAboutAnEvent tellUsAboutAnEvent = new TellUsAboutAnEvent(driver);
        tellUsAboutAnEvent.waitForElement(tellUsAboutAnEvent.headerText);
        tellUsAboutAnEvent.isDisplayed(tellUsAboutAnEvent.loginLabel);
        assert(tellUsAboutAnEvent.storeText(tellUsAboutAnEvent.loginLabel).equals("You must log in to view this page. Please enter your email address and password, or use one of our partners to login"));
    }

//    @Test
//    @DisplayName("Check that the tell us about an event loads correctly after entering correct login details")
//    public void checkFormFunctionalityLogged_Top() throws InterruptedException {
//        TempMail tempMail = new TempMail(driver);
//        tempMail.fetchTempMailEmail();
//
//        HomePage homePage = new HomePage(driver);
//        homePage.fetch(EASTERN_DAILY_PRESS);
//        homePage.waitForElement(homePage.registrationAnd24LinksHeader);
//        homePage.click(homePage.registerLink);
//
//        Register register = new Register(driver);
//        register.waitForElement(register.registerForm);
//        register.fillInRegisterForm(tempMail.tempMailEmail);
//        register.click(register.registerButton);
//        try {
//            homePage.waitForElement(register.registerSuccessElement);
//            register.checkTextMatches(register.registerSuccessElement, "Thanks for registering");
//        } catch (NoSuchElementException e) {
//            System.out.println("Registration was not successfull");
//        }
//
//        tempMail.fetch(TEMPMAIL);
//
//        Thread.sleep(5000);
//        tempMail.waitForElement(tempMail.emailSubjectLinkText);
//        tempMail.scrollToElement(tempMail.emailSubjectLinkText);
//        tempMail.click(tempMail.emailSubjectLinkText);
//        mainWindow = "";
//        tempMailWindow = driver.getWindowHandle();
//        Thread.sleep(2000);
//        tempMail.waitForElement(tempMail.emailBodyLink);
//        tempMail.scrollToElement(tempMail.emailBodyLink);
//        tempMail.click(tempMail.emailBodyLink);
//        Thread.sleep(2000);
//        ArrayList tabs = new ArrayList(driver.getWindowHandles());
//        driver.switchTo().window(String.valueOf(tabs.get(1)));
//        mainWindow = driver.getWindowHandle();
//        homePage.waitForElement(register.registerConfirmationElement);
//        driver.switchTo().window(tempMailWindow);
//        driver.close();
//        tempMailWindow = "";
//        driver.switchTo().window(mainWindow);
//
//        homePage.scrollToElement(homePage.copyrightElement);
//        homePage.click(homePage.tellUsAboutAnEventLink);
//
//        TellUsAboutAnEvent tellUsAboutAnEvent = new TellUsAboutAnEvent(driver);
//        tellUsAboutAnEvent.waitForElement(tellUsAboutAnEvent.headerText);
//        tellUsAboutAnEvent.fillInForm(tempMail.tempMailEmail);
//        tellUsAboutAnEvent.click(tellUsAboutAnEvent.submitButton);
//        tellUsAboutAnEvent.waitForElement(tellUsAboutAnEvent.headerText);
//        tellUsAboutAnEvent.entireForm();
//    }

    @Test
    @DisplayName("Verify that the bottom send us a story form loads when clicked")
    public void navigateToForm_Bottom() {
        HomePage homePage = new HomePage(driver);
        //homePage.scrollToElement(homePage.copyrightElement);
        homePage.click(homePage.sendUsAStoryLink_Bottom);
        homePage.waitForElement(homePage.sendUsAStoryLink_BottomForm);
    }

    @Test
    @DisplayName("Verify that the privacy message is visible when the bottom send us a story form is clicked")
    public void checkPrivacyMessageVisible_Bottom() {
        HomePage homePage = new HomePage(driver);
        //homePage.scrollToElement(homePage.copyrightElement);
        homePage.click(homePage.sendUsAStoryLink_Bottom);
        homePage.waitForElement(homePage.sendUsAStoryLink_BottomForm);
        assert (homePage.storeText(homePage.gdprHeader).startsWith("Archant Community Media Limited will only use your data"));
    }

    @Test
    @DisplayName("Verify that the privacy message is visible when the bottom send us a story form is clicked")
    public void checkInputBoxesVisible_Bottom() {
        HomePage homePage = new HomePage(driver);
        //homePage.scrollToElement(homePage.copyrightElement);
        homePage.click(homePage.sendUsAStoryLink_Bottom);
        homePage.waitForElement(homePage.sendUsAStoryLink_BottomForm);
        homePage.isDisplayed(homePage.nameText);
        homePage.isDisplayed(homePage.emailText);
        homePage.isDisplayed(homePage.telephoneText);
        homePage.isDisplayed(homePage.briefSummaryText);
        homePage.isDisplayed(homePage.storyDetailsText);
    }

    @Test
    @DisplayName("Verify that the media upload sections are visible when the bottom send us a story form is clicked")
    public void checkMediaUploadButtonsVisible_Bottom() {
        HomePage homePage = new HomePage(driver);
        //homePage.scrollToElement(homePage.copyrightElement);
        homePage.click(homePage.sendUsAStoryLink_Bottom);
        homePage.waitForElement(homePage.sendUsAStoryLink_BottomForm);
        homePage.isDisplayed(homePage.photoSection);
        homePage.isDisplayed(homePage.audioSection);
        homePage.isDisplayed(homePage.videoSection);
    }

    @Test
    @DisplayName("Verify the presence of the terms and conditions link when the bottom send us a story form is clicked")
    public void checkTermsAndConditionsVisible_Bottom() {
        HomePage homePage = new HomePage(driver);
        //homePage.scrollToElement(homePage.copyrightElement);
        homePage.click(homePage.sendUsAStoryLink_Bottom);
        homePage.waitForElement(homePage.sendUsAStoryLink_BottomForm);
        homePage.isDisplayed(homePage.termsAndConditionsParagraph);
        homePage.isDisplayed(homePage.termsAndConditionsCheckbox);
    }

    @Test
    @DisplayName("Verify the click functionality of the terms and conditions link when the bottom send us a story form is clicked")
    public void checkTermsAndConditionsLinkIsClickable_Bottom() {
        HomePage homePage = new HomePage(driver);
        //homePage.scrollToElement(homePage.copyrightElement);
        homePage.click(homePage.sendUsAStoryLink_Bottom);
        homePage.waitForElement(homePage.sendUsAStoryLink_BottomForm);
        homePage.isClickable(homePage.sendUsAStoryInput_TermsLink);
    }

    @Test
    @DisplayName("Verify the presence of the recaptcha tickbox when the bottom send us a story form is clicked")
    public void checkRecaptchaVisible_Bottom() {
        HomePage homePage = new HomePage(driver);
        //homePage.scrollToElement(homePage.copyrightElement);
        homePage.click(homePage.sendUsAStoryLink_Bottom);
        homePage.waitForElement(homePage.sendUsAStoryLink_BottomForm);
        homePage.waitForElement(homePage.sendUsAStoryInput_ReCaptcha);
        homePage.isDisplayed(homePage.sendUsAStoryInput_ReCaptcha);
    }

    @Test
    @DisplayName("Verify the presence of the submit button when the bottom send us a story form is clicked")
    public void checkSubmitButtonVisible_Bottom() {
        HomePage homePage = new HomePage(driver);
        //homePage.scrollToElement(homePage.copyrightElement);
        homePage.click(homePage.sendUsAStoryLink_Bottom);
        homePage.waitForElement(homePage.sendUsAStoryLink_BottomForm);
        homePage.isDisplayed(homePage.sendUsAStoryInput_SubmitButton);
    }

    @Test
    @DisplayName("Verify the responsiveness of the bottom send us a story form")
    public void checkFormResponsiveness_Bottom() {
        HomePage homePage = new HomePage(driver);
        homePage.click(homePage.sendUsAStoryLink_Bottom);
        homePage.waitForElement(homePage.sendUsAStoryLink_BottomForm);
        homePage.isDisplayed(homePage.nameText);
        homePage.isDisplayed(homePage.emailText);
        homePage.isDisplayed(homePage.telephoneText);
        homePage.isDisplayed(homePage.briefSummaryText);
        homePage.scrollToElement(homePage.storyDetailsText);
        homePage.isDisplayed(homePage.storyDetailsText);
        homePage.isDisplayed(homePage.photoSection);
        homePage.isDisplayed(homePage.audioSection);
        homePage.isDisplayed(homePage.videoSection);
        homePage.isDisplayed(homePage.termsAndConditionsParagraph);
        homePage.isDisplayed(homePage.termsAndConditionsCheckbox);
        homePage.isDisplayed(homePage.sendUsAStoryInput_ReCaptcha);
        homePage.isDisplayed(homePage.sendUsAStoryInput_SubmitButton);
        homePage.minimizeWindow();
        homePage.isDisplayed(homePage.nameText);
        homePage.isDisplayed(homePage.emailText);
        homePage.isDisplayed(homePage.telephoneText);
        homePage.isDisplayed(homePage.briefSummaryText);
        homePage.scrollToElement(homePage.storyDetailsText);
        homePage.isDisplayed(homePage.storyDetailsText);
        homePage.isDisplayed(homePage.photoSection);
        homePage.isDisplayed(homePage.audioSection);
        homePage.isDisplayed(homePage.videoSection);
        homePage.isDisplayed(homePage.termsAndConditionsParagraph);
        homePage.isDisplayed(homePage.termsAndConditionsCheckbox);
        homePage.isDisplayed(homePage.sendUsAStoryInput_ReCaptcha);
        homePage.isDisplayed(homePage.sendUsAStoryInput_SubmitButton);
    }

    @Test
    @DisplayName("Verify the presence of the submit button when the bottom send us a story form is clicked")
    public void clickTermsAndConditionsLink_Bottom() {
        HomePage homePage = new HomePage(driver);
        //homePage.scrollToElement(homePage.copyrightElement);
        homePage.click(homePage.sendUsAStoryLink_Bottom);
        homePage.waitForElement(homePage.sendUsAStoryLink_BottomForm);
        homePage.mainWindow = driver.getWindowHandle();
        homePage.click(homePage.sendUsAStoryInput_TermsLink);
        homePage.waitForElement(homePage.genericSelector());
        assert(driver.getCurrentUrl().equals("https://www.archant.co.uk/articles/terms-conditions/"));
        homePage.secondWindow = driver.getWindowHandle();
        homePage.navigateBack();
        driver.switchTo().window(homePage.mainWindow);
    }

    @Test
    @DisplayName("Verify the login form is visible when clicking the tell us an about event link")
    public void tellUsAboutAnEvent_Unlogged() {
        HomePage homePage = new HomePage(driver);
        //homePage.isDisplayed(homePage.loginLink);
        //homePage.scrollToElement(homePage.copyrightElement);
        homePage.click(homePage.tellUsAboutAnEventLink);

        TellUsAboutAnEvent tellUsAboutAnEvent = new TellUsAboutAnEvent(driver);
        tellUsAboutAnEvent.waitForElement(tellUsAboutAnEvent.headerText);
        tellUsAboutAnEvent.isDisplayed(tellUsAboutAnEvent.loginLabel);
        assert(tellUsAboutAnEvent.storeText(tellUsAboutAnEvent.loginLabel).equals("You must log in to view this page. Please enter your email address and password, or use one of our partners to login"));
    }

//    @Test
//    @DisplayName("Verify the login form is visible after logging in then clicking the tell us about an event link")
//    public void tellUsAboutAnEvent_Logged() throws InterruptedException {
//
//        TempMail tempMail = new TempMail(driver);
//        tempMail.fetchTempMailEmail();
//
//        HomePage homePage = new HomePage(driver);
//        homePage.fetch(EASTERN_DAILY_PRESS);
//        homePage.waitForElement(homePage.registrationAnd24LinksHeader);
//        homePage.click(homePage.registerLink);
//
//        Register register = new Register(driver);
//        register.waitForElement(register.registerForm);
//        register.fillInRegisterForm(tempMail.tempMailEmail);
//        register.click(register.registerButton);
//        try {
//            homePage.waitForElement(register.registerSuccessElement);
//            register.checkTextMatches(register.registerSuccessElement, "Thanks for registering");
//        } catch (NoSuchElementException e) {
//            System.out.println("Registration was not successfull");
//        }
//
//        // 3.05	Open the corresponding email inbox and confirm that the activation email has been received.
//        tempMail.fetch(TEMPMAIL);
//
//        // 3.06	Open the email and click on the activation link.
//        Thread.sleep(5000);
//        tempMail.scrollToElement(tempMail.emailSubjectLinkText);
//        tempMail.click(tempMail.emailSubjectLinkText);
//        Thread.sleep(2000);
//        tempMail.scrollToElement(tempMail.emailBodyLink);
//        tempMail.click(tempMail.emailBodyLink);
//        Thread.sleep(2000);
//        homePage.waitForElement(register.registerConfirmationElement);
//
//        // 3.07	Re-enter the registration section.
//        homePage.waitForElement(homePage.registrationAnd24LinksHeader);
//        homePage.click(homePage.registerLink);
//
//        register.waitForElement(register.registerForm);
//
//        homePage.scrollToElement(homePage.copyrightElement);
//        homePage.click(homePage.tellUsAboutAnEventLink);
//
//        TellUsAboutAnEvent tellUsAboutAnEvent = new TellUsAboutAnEvent(driver);
//        tellUsAboutAnEvent.waitForElement(tellUsAboutAnEvent.headerText);
//        tellUsAboutAnEvent.fillInForm(tempMail.tempMailEmail);
//        tellUsAboutAnEvent.click(tellUsAboutAnEvent.submitButton);
//        tellUsAboutAnEvent.waitForElement(tellUsAboutAnEvent.headerText);
//        tellUsAboutAnEvent.entireForm();
//    }
}