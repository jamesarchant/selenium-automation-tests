package tests;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit4.DisplayName;
import org.junit.*;
import pageobjects.*;

import static org.testng.Assert.*;

public class NavigationTest extends BaseTests {

    @Test
    @DisplayName("Verify the presence of the login and register links")
    public void verifyLoginAndRegisterLinks() {
        HomePage homePage = new HomePage(driver);
        assertTrue(homePage.checkLoginLinkIsDisplayed());
        assertEquals(homePage.getLoginLink(), "Log in");
        assertTrue(homePage.checkRegisterLinkIsDisplayed());
        assertEquals(homePage.getRegisterLink(), "Register");
    }

    @Test
    @DisplayName("Verify the presence of the various 24 links on the top navigation")
    public void verify24Links() {
        HomePage homePage = new HomePage(driver);
        assertTrue(homePage.checkJobs24LinkIsDisplayed());
        assertEquals(homePage.getJobs24Link(), "jobs24");
        assertTrue(homePage.checkLocalSearchLinkIsDisplayed());
        assertEquals(homePage.getLocalSearchLink(), "localsearch24");
        assertTrue(homePage.checkFamilyNoticesLinkIsDisplayed());
        assertEquals(homePage.getFamilyNoticesLink(), "familynotices24");
        assertTrue(homePage.checkHomes24LinkIsDisplayed());
        assertEquals(homePage.getHomes24Link(), "homes24");
        assertTrue(homePage.checkDrive24LinkIsDisplayed());
        assertEquals(homePage.getDrive24Link(), "drive24");
    }

    @Test
    @DisplayName("Verify the presence of the useful links section")
    public void verifyUsefulLinks() {
        HomePage homePage = new HomePage(driver);
        homePage.checkUsefulLinksAreDisplayed();
    }

    @Test
    @DisplayName("Verify the presence of 3 additional social links when the page is minimized")
    public void verifyUsefulLinksAdditionalSocials() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.minimizeWindow();
        homePage.hoverUsefulLinks();
        assertTrue(homePage.checkYouTubeLinkIsDisplayed());
        assertEquals(homePage.getYouTubeLink(), "youtube");
        assertTrue(homePage.checkTwitterLinkIsDisplayed());
        assertEquals(homePage.getTwitterLink(), "twitter");
        assertTrue(homePage.checkFacebookLinkIsDisplayed());
        assertEquals(homePage.getFacebookLink(), "facebook");
    }

    @Test
    @DisplayName("Verify the responsiveness of the 24 links")
    public void verify24LinksResponsiveness() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        assertTrue(homePage.checkJobs24LinkIsDisplayed());
        assertEquals(homePage.getJobs24Link(), "jobs24");
        assertTrue(homePage.checkLocalSearchLinkIsDisplayed());
        assertEquals(homePage.getLocalSearchLink(), "localsearch24");
        assertTrue(homePage.checkFamilyNoticesLinkIsDisplayed());
        assertEquals(homePage.getFamilyNoticesLink(), "familynotices24");
        assertTrue(homePage.checkHomes24LinkIsDisplayed());
        assertEquals(homePage.getHomes24Link(), "homes24");
        assertTrue(homePage.checkDrive24LinkIsDisplayed());
        assertEquals(homePage.getDrive24Link(), "drive24");
        homePage.minimizeWindow();
        assertTrue(homePage.checkJobs24LinkIsDisplayed());
        assertEquals(homePage.getJobs24Link(), "jobs24");
        assertTrue(homePage.checkLocalSearchLinkIsDisplayed());
        assertEquals(homePage.getLocalSearchLink(), "localsearch24");
        assertTrue(homePage.checkFamilyNoticesLinkIsDisplayed());
        assertEquals(homePage.getFamilyNoticesLink(), "familynotices24");
        assertTrue(homePage.checkHomes24LinkIsDisplayed());
        assertEquals(homePage.getHomes24Link(), "homes24");
        assertTrue(homePage.checkDrive24LinkIsDisplayed());
        assertEquals(homePage.getDrive24Link(), "drive24");
    }

    @Test
    @DisplayName("Click the login link")
    public void clickLogin() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickLoginLink();

        Login login = new Login(driver);
        login.waitForLoginForm();
        assertTrue(login.checkGDPRHeader());
        assertEquals(login.getGDPRHeader(), "Archant Community Media Limited will only use your data for the purpose of the services you are signing up to and the marketing consent you’ve given using the check boxes. We do not share you data with third parties unless you have given us consent to do so. Your data will be used in accordance with our privacy policy which can be found here. You can remove your permission for our use of your data at any time by emailing dpo@archant.co.uk or remove marketing permission by clicking on the unsubscribe link(s) in our communications.");
        assertTrue(login.checkLoginHeaderIsDisplayed());
        assertEquals(login.getLoginHeader(), "Sign in using your Archant account...");
        assertTrue(login.checkEmailLabelIsDisplayed());
        assertEquals(login.getEmailLabel(), "Email");
        assertTrue(login.checkEmailEntryIsDisplayed());
        assertTrue(login.checkPasswordLabelIsDisplayed());
        assertEquals(login.getPasswordLabel(), "Password");
        assertTrue(login.checkPasswordEntryIsDisplayed());
        assertTrue(login.checkLoginButtonIsDisplayed());
        assertEquals(login.getLoginButton(), "Login");
        assertTrue(login.checkJanrainHeaderIsDisplayed());
        assertEquals(login.getJanrainHeader(), "Sign in using your account with...");
        assertTrue(login.checkJanrainFacebookButtonIsDisplayed());
        assertTrue(login.checkJanrainGoogleButtonIsDisplayed());
        assertTrue(login.checkJanrainYahooButtonIsDisplayed());
        assertTrue(login.checkJanrainMicrosoftIsDisplayed());
    }

    @Test
    @DisplayName("Click the register link")
    public void clickRegister() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickRegisterLink();

        Register register = new Register(driver);
        register.waitForRegisterForm();
        assertTrue(register.checkGDPRHeaderIsDisplayed());
        assertEquals(register.getGDPRHeaderText(), "Archant Community Media Limited will only use your data for the purpose of the services you are signing up to and the marketing consent you’ve given using the check boxes. We do not share you data with third parties unless you have given us consent to do so. Your data will be used in accordance with our privacy policy which can be found here. You can remove your permission for our use of your data at any time by emailing dpo@archant.co.uk or remove marketing permission by clicking on the unsubscribe link(s) in our communications.");
        assertTrue(register.checkPersonDetailsHeaderIsDisplayed());
        assertEquals(register.getPersonalDetailsHeaderText(), "Personal details");
        assertTrue(register.checkEmailAndPasswordHeaderIsDisplayed());
        assertEquals(register.getEmailAndPasswordText(), "Email and password");
        assertTrue(register.checkTitleLabelIsDisplayed());
        assertEquals(register.getTitleLabel(), "Title*");
        assertTrue(register.checkTitleIsDisplayed());
        assertTrue(register.checkFirstNameLabelIsDisplayed());
        assertEquals(register.getFirstNameLabel(), "First name*");
        assertTrue(register.checkFirstNameIsDisplayed());
        assertTrue(register.checkSurnameLabelIsDisplayed());
        assertEquals(register.getSurnameLabel(), "Surname*");
        assertTrue(register.checkSurnameIsDisplayed());
        assertTrue(register.checkScreenNameLabelIsDisplayed());
        assertEquals(register.getScreenName(), "Screen name*");
        assertTrue(register.checkScreenNameIsDisplayed());
        assertTrue(register.checkScreenNameHelperTextIsDisplayed());
        assertEquals(register.getScreeNameHelperText(), "Screen names can only contain alphanumeric characters, combined with either the _ or - character. Your screen name will be used to identify you publicly, for example next to comments");
        assertTrue(register.checkEmailAddressTextIsDisplayed());
        assertEquals(register.getEmailAddress(), "Email address*");
        assertTrue(register.checkEmailAddressIsDisplayed());
        assertTrue(register.checkPasswordTextIsDisplayed());
        assertEquals(register.getPassword(), "Password*");
        assertTrue(register.checkPasswordIsDisplayed());
        assertTrue(register.checkConfirmPasswordTextIsDisplayed());
        assertEquals(register.getConfirmPassword(), "Confirm password*");
        assertTrue(register.checkConfirmPasswordIsDisplayed());
        assertTrue(register.checkNewsletterLabelIsDisplayed());
        assertEquals(register.getNewsletterLabelText(), "Sign up to receive our regular email newsletter");
        assertTrue(register.checkNewsletterCheckboxIsDisplayed());
        assertTrue(register.checkPromotionsLabelIsDisplayed());
        assertEquals(register.getPromotionsLabelText(), "Keep me up to date on special promotions, products and services from Archant Community Media Limited");
        assertTrue(register.checkPromotionsCheckboxIsDisplayed());
        assertTrue(register.checkThirdPartiesLabelIsDisplayed());
        assertEquals(register.getThirdPartiesLabelText(), "I am happy for Archant Community Media Ltd to contact me on behalf of third parties (we will not share your information with these third parties unless you give us consent to do so)");
        assertTrue(register.checkThirdPartiesCheckboxIsDisplayed());
        assertTrue(register.checkTermsAndConditionsLabelIsDisplayed());
        assertEquals(register.getTermsAndConditionsLabelText(), "I have read and accept the terms and conditions");
        assertTrue(register.checkTermsAndConditionsCheckboxIsDisplayed());
        assertTrue(register.checkRegisterButtonIsDisplayed());
        assertEquals(register.getRegisterButtonText(), "Register now");
    }

    @Test
    @DisplayName("Click the 24 links")
    public void click24Links() {
        HomePage homePage = new HomePage(driver);
        homePage.click24Links();
    }

    @Test
    @DisplayName("Click each of the Useful links")
    public void clickUsefulLinks() {
        HomePage homePage = new HomePage(driver);
        homePage.clickUsefulLinks();
    }

    @Test
    @DisplayName("Ensuring that you are not on the homepage, click the main logo")
    public void clickMainLogo() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickMainArticle();

        Article article = new Article(driver);
        article.waitForArticle();
        article.clickMainLogo();

        homePage.waitForMainNews();
    }

    @Test
    @DisplayName("Verify the presence of the social media icons")
    public void verifySocialLinks() {
        HomePage homePage = new HomePage(driver);
        homePage.checkSocialLinksAreDisplayed();
    }

    @Test
    @DisplayName("Verify the click functionality of the social media icons")
    public void clickSocialLinks() {
        HomePage homePage = new HomePage(driver);
        homePage.clickSocialLinks();
    }

    @Test
    @DisplayName("Verify the responsiveness of the main logo")
    public void verifyMainLogoResponsiveness() {
        HomePage homePage = new HomePage(driver);
        assertTrue(homePage.checkMainLogoIsDisplayed());
        homePage.minimizeWindow();
        assertTrue(homePage.checkMainLogoIsDisplayed());
    }

    @Test
    @DisplayName("Verify the presence of the sub menus after each of the main navigation links are hovered over")
    public void verifyMainNavHover() {
        HomePage homePage = new HomePage(driver);
        homePage.hoverMainNavigationLinks();
    }

    @Test
    @DisplayName("Verify the click functionality of the main navigation links work")
    public void clickPrimaryNavLinks() {
        HomePage homePage = new HomePage(driver);
        homePage.clickMainNavigationLinks();
    }

    @Test
    @DisplayName("Verify the click functionality of the secondary navigation links work")
    public void clickSecondaryNavLinks() {
        HomePage homePage = new HomePage(driver);
        homePage.clickSecondaryNavigationLinks();
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Verify the responsiveness of the top navigation links")
    public void verifyTopNavResponsiveness() {
        HomePage homePage = new HomePage(driver);
        assertTrue(homePage.checkMainNavigationBarIsDisplayed());
        homePage.minimizeWindow();
        assertTrue(homePage.checkMinimisedavigationBarIsDisplayed());
    }


    @Test
    @DisplayName("Verify the click functionality of the burger nav when the page is minimized")
    public void clickBurgerMenu() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.minimizeWindow();
        homePage.clickBurgerMenu();
    }

    @Test
    @DisplayName("Verify the copyright text is visible in the footer of the page")
    public void verifyCopyrightText() {
        HomePage homePage = new HomePage(driver);
        assertTrue(homePage.checkCopyrightTextIsDisplayed());
        assertEquals(homePage.getCopyright(), "© 2019 Archant Community Media Ltd");
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Verify the click functionality of the terms and conditions footer link")
    public void clickTermsFooterLink() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickTermsLink("https://www.archant.co.uk/articles/terms-conditions/");
    }

    @Test
    @DisplayName("Verify the click functionality of the privacy footer link")
    public void verifyAndClickPrivacyFooterLink() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickPrivacyLink("https://www.archant.co.uk/articles/privacy-policy/");
    }

    @Test
    @DisplayName("Verify the click functionality of the cookies footer link")
    public void verifyAndClickCookiesFooterLink() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickCookiesLink("https://www.archant.co.uk/articles/cookies/");
    }

    @Test
    @DisplayName("Verify all main footer links are displayed appropriately")
    public void verifyMainFooterLinks() {
        HomePage homePage = new HomePage(driver);
        homePage.verifyMoreGreatContentFooterLinks();
    }

    @Test
    @DisplayName("Verify the click functionality of two links at random from each category on the bottom navigation")
    public void clickRandomMainFooterLinks() {
        HomePage homePage = new HomePage(driver);
        homePage.clickRandomMainFooterLinks();
    }

    @Test
    @DisplayName("Verify the More Great Content navigation links")
    public void verifyMoreGreatContentFooterLinks() {
        HomePage homePage = new HomePage(driver);
        homePage.verifyMoreGreatContentFooterLinks();
    }

    @Test
    @DisplayName("Verify the footer section is responsive")
    public void verifyFooterResponsiveness() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        assertTrue(homePage.checkMainFooterIsDisplayed());
        assertTrue(homePage.checkMoreGreatContentFooterIsDisplayed());
        homePage.minimizeWindow();
        assertTrue(homePage.checkMainFooterIsDisplayed());
        assertTrue(homePage.checkMoreGreatContentFooterIsDisplayed());
    }
}