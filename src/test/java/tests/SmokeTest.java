package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Allure;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit4.DisplayName;
import org.apache.commons.io.FileUtils;
import org.junit.*;

import static org.testng.Assert.assertTrue;
import static pageobjects.BasePage.*;

import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pageobjects.Article;
import pageobjects.CookiesPopup;
import pageobjects.HomePage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

public class SmokeTest {

    private static boolean shouldStopRestOfSuite = false;

    @Before
    public void before() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("headless");
        options.addArguments("window-size=1200x600");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Verify the presence of the cookies popup")
    public void verifyCookiesPopup() {
        CookiesPopup cookiesPopup = new CookiesPopup(driver);
        cookiesPopup.loadPage();
        assertTrue(cookiesPopup.checkHeaderTextIsDisplayed());
        assertTrue(cookiesPopup.checkBodyTextIsDisplayed());
        assertTrue(cookiesPopup.checkAcceptButtonIsDisplayed());
        assertTrue(cookiesPopup.checkShowOptionsTextIsDisplayed());
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Verify that an article loads correctly when clicked")
    public void verifyArticleLoads() {
        CookiesPopup cookiesPopup = new CookiesPopup(driver);
        cookiesPopup.loadPage();
        cookiesPopup.clickAcceptButton();

        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickRandomArticle();

        Article article = new Article(driver);
        assertTrue(article.checkHeaderIsDisplayed());
        assertTrue(article.checkLeadImageIsDisplayed());
        assertTrue(article.checkLeadImageCaptionIsDisplayed());
        assertTrue(article.checkSummaryIsDisplayed());
        assertTrue(article.checkBodyTextIsDisplayed());
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Verify the top navigation is responsive")
    public void verifyTopNavResponsiveness() {
        CookiesPopup cookiesPopup = new CookiesPopup(driver);
        cookiesPopup.loadPage();
        cookiesPopup.clickAcceptButton();

        HomePage homePage = new HomePage(driver);
        assertTrue(homePage.checkMainNavigationBarIsDisplayed());
        homePage.minimizeWindow();
        assertTrue(homePage.checkMinimisedavigationBarIsDisplayed());
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Verify the click functionality of the terms and conditions footer link")
    public void clickTermsFooterLink() {
        CookiesPopup cookiesPopup = new CookiesPopup(driver);
        cookiesPopup.loadPage();
        cookiesPopup.clickAcceptButton();

        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickTermsLink("https://www.archant.co.uk/articles/terms-conditions/");
    }

    @After
    public void after() {
        if (shouldStopRestOfSuite == true) {
            System.out.println("Test suite stopped because of an error");
            driver.quit();
            System.exit(1);
        }
    }

    @Rule
    public TestRule watcher = new TestWatcher() {
        @Override
        protected void failed(Throwable throwable, Description description) {
            File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            try {
                FileUtils.copyFile(srcFile, new File("./screenshots/"
                        + description.getClassName()
                        + "_" + description.getMethodName()
                        + ".png"));
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(srcFile));
                Allure.addAttachment(description.getClassName() + "_" + description.getMethodName(), byteArrayInputStream);
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }

        @Override
        public void finished(Description description) {
            driver.quit();
        }
    };
}