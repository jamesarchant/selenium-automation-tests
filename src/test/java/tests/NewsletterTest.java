package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Allure;
import io.qameta.allure.junit4.DisplayName;
import org.apache.commons.io.FileUtils;
import org.junit.*;

import static org.junit.Assert.assertEquals;
import static pageobjects.BasePage.*;

import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import pageobjects.Article;
import pageobjects.CookiesPopup;
import pageobjects.HomePage;
import pageobjects.BasePage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class NewsletterTest extends BaseTests {

    @Test
    @DisplayName("Verify the presence of the newsletter widget on the homepage")
    public void verifyNewsletterWidgetVisible() {
        HomePage homePage = new HomePage(driver);
        //homePage.isDisplayed(homePage.newsletterWidget);
    }

    @Test
    @DisplayName("Verify the contents of the newsletter widget on the homepage")
    public void verifyNewsletterWidgetContents() {
        HomePage homePage = new HomePage(driver);
        homePage.isDisplayed(homePage.newsletter_Title);
        homePage.isDisplayed(homePage.newsletterSelectionOne);
        homePage.isDisplayed(homePage.newsletterSelectionTwo);

    }

    @Test
    @DisplayName("Verify the newsletter frequencies that are displayed are one of the available options on the homepage")
    public void verifyNewsletterFrequency() {
        HomePage homePage = new HomePage(driver);
        homePage.verifyItemInArray(homePage.newsletterSelectionOne, homePage.newsletterFrequency());
        homePage.verifyItemInArray(homePage.newsletterSelectionTwo, homePage.newsletterFrequency());
    }

    @Test
    @DisplayName("Verify the presence of the email and name fields on the homepage newsletter widget")
    public void verifyNewsletterEmailAndName() {
        HomePage homePage = new HomePage(driver);
        homePage.isDisplayed(homePage.newsletter_Email);
        homePage.isDisplayed(homePage.newsletter_Name);
    }

    @Test
    @DisplayName("Verify the presence of the two checkboxes on the homepage newsletter widget")
    public void verifyNewsletterCheckboxes() {
        HomePage homePage = new HomePage(driver);
        homePage.isDisplayed(homePage.newsletter_CheckboxOne);
        homePage.isDisplayed(homePage.newsletter_CheckboxTwo);
    }

    @Test
    @DisplayName("Verify the presence of the join button on the homepage newsletter widget")
    public void verifyNewsletterButton() {
        HomePage homePage = new HomePage(driver);
        homePage.isDisplayed(homePage.newsletter_JoinButton);
    }

    @Test
    @DisplayName("Verify the presence of the privacy policy on the homepage newsletter widget")
    public void verifyPrivacyPolicy() {
        HomePage homePage = new HomePage(driver);
        homePage.isDisplayed(homePage.newsletter_PrivacyLink);
    }

    @Test
    @DisplayName("Verify the click functionality of the privacy link on the homepage newsletter widget")
    public void clickPrivacyPolicyLink() {
        HomePage homePage = new HomePage(driver);
        homePage.mainWindow = driver.getWindowHandle();
        homePage.scrollToElement(homePage.newsletter_PrivacyLink);
        homePage.click(homePage.newsletter_PrivacyLink);
        homePage.waitForElement(homePage.genericSelector());
        ArrayList tabs = new ArrayList(driver.getWindowHandles());
        driver.switchTo().window(String.valueOf(tabs.get(1)));
        homePage.secondWindow = driver.getWindowHandle();
        assert driver.getCurrentUrl().equals("https://www.archant.co.uk/articles/privacy-policy/");
        driver.close();
        driver.switchTo().window(homePage.mainWindow);
    }

    @Test
    @DisplayName("Verify an error message appears when the join button is clicked with nothing entered on the homepage widget")
    public void checkJoinButton_NothingEntered() {
        HomePage homePage = new HomePage(driver);
        homePage.click(homePage.newsletter_JoinButton);
        String errorMessage = wait.until(ExpectedConditions.alertIsPresent()).getText();
        String errorMessageExpectedText = "Please select one of the newsletters to sign up to";
        assert (errorMessage.equals(errorMessageExpectedText));
        driver.switchTo().alert().dismiss();
    }

//    @Test
//    @DisplayName("Verify a user is able to sign up sucesfully to the homepage newsletter")
//    public void newsletterSuccessful() throws InterruptedException {
//
//        TempMail tempMail = new TempMail(driver);
//        tempMail.fetchTempMailEmail();
//
//        HomePage homePage = new HomePage(driver);
//        homePage.fetch(EASTERN_DAILY_PRESS);
//        homePage.checkCheckbox(homePage.newsletterSelectionOne);
//        homePage.checkCheckbox(homePage.newsletterSelectionTwo);
//        homePage.fillInNewsletter_Home(tempMail.tempMailEmail);
//        homePage.checkCheckbox(homePage.newsletter_CheckboxOne);
//        homePage.checkCheckbox(homePage.newsletter_CheckboxTwo);
//        homePage.click(homePage.newsletter_CheckboxOne);
//        homePage.click(homePage.newsletter_JoinButton);
//        homePage.isDisplayed(homePage.newsletter_ConfirmationText1);
//        homePage.checkTextMatches(homePage.newsletter_ConfirmationText1, "Thank you for your subscription to this newsletter, an email with the link has been sent to your registered address.");
//        homePage.checkTextMatches(homePage.newsletter_ConfirmationText2, "If the email doesn't appear within the next few minutes, please check your junk mail filters in case it has been misplaced.");
//
//        homePage.fetch(TEMPMAIL);
//
//        Thread.sleep(5000);
//        tempMail.waitForElement(tempMail.newsletterSubjectHeader);
//        tempMail.scrollToElement(tempMail.newsletterSubjectHeader);
//        tempMail.click(tempMail.newsletterSubjectHeader);
//        Thread.sleep(2000);
//        tempMail.waitForElement(tempMail.newsletterEmailBody);
//        tempMail.isDisplayed(tempMail.newsletterEmailBody);
//    }

//    @Test
//    @DisplayName("Verify that the newsletter widget is visible when an article is clicked at random")
//    public void newsletterArticleLoads() {
//        HomePage homePage = new HomePage(driver);
//        // homePage.clickRandomArticle(homePage.mainArticleElements);
//
//        Article article = new Article(driver);
//        article.waitForElement(article.articleContent);
//        article.waitForFrameAndSwitch_String(article.mainArticleSubscriptionFrame);
//        article.isDisplayed(article.mainArticleNewsletterWidget);
//    }
//
//    @Test
//    @DisplayName("Verify that the heading is visible on the newsletter widget on the article page")
//    public void newsletterArticle_Heading() {
//        HomePage homePage = new HomePage(driver);
//        //homePage.clickRandomArticle(homePage.mainArticleElements);
//
//        Article article = new Article(driver);
//        article.waitForElement(article.articleContent);
//        article.waitForFrameAndSwitch_String(article.mainArticleSubscriptionFrame);
//        article.isDisplayed(article.mainArticleNewsletterHeading);
//    }
//
//    @Test
//    @DisplayName("Verify the presence of the frequency on the newsletter widget on the article page")
//    public void newsletterArticle_Frequency() {
//        HomePage homePage = new HomePage(driver);
//        //homePage.clickRandomArticle(homePage.mainArticleElements);
//
//        Article article = new Article(driver);
//        article.waitForElement(article.articleContent);
//        article.waitForFrameAndSwitch_String(article.mainArticleSubscriptionFrame);
//        article.isDisplayed(article.mainArticleNewsletterHeading);
//        homePage.verifyItemInArray(article.MainArticleNewsletterSelection, homePage.newsletterFrequency());
//    }
//
//    @Test
//    @DisplayName("Verify the presence of the email field on the newsletter widget on the article page")
//    public void newsletterArticle_Email() {
//        HomePage homePage = new HomePage(driver);
//        //homePage.clickRandomArticle(homePage.mainArticleElements);
//
//        Article article = new Article(driver);
//        article.waitForElement(article.articleContent);
//        article.waitForFrameAndSwitch_String(article.mainArticleSubscriptionFrame);
//        article.isDisplayed(article.mainArticleNewsletterHeading);
//        article.isDisplayed(article.mainArticleNewsletterEmail);
//    }
//
//    @Test
//    @DisplayName("Verify the presence of the subscribe button on the newsletter widget on the article page")
//    public void newsletterArticle_SubscribeButton() {
//        HomePage homePage = new HomePage(driver);
//        //homePage.clickRandomArticle(homePage.mainArticleElements);
//
//        Article article = new Article(driver);
//        article.waitForElement(article.articleContent);
//        article.waitForFrameAndSwitch_String(article.mainArticleSubscriptionFrame);
//        article.isDisplayed(article.mainArticleNewsletterSubmit);
//    }
//
//    @Test
//    @DisplayName("Verify the presence of the privacy link on the newsletter widget on the article page")
//    public void newsletterArticlePrivacyPolicy() {
//        HomePage homePage = new HomePage(driver);
//       // homePage.clickRandomArticle(homePage.mainArticleElements);
//
//        Article article = new Article(driver);
//        article.waitForElement(article.articleContent);
//        article.waitForFrameAndSwitch_String(article.mainArticleSubscriptionFrame);
//        article.isDisplayed(article.mainArticleNewsletterPrivacy);
//    }
//
//    @Test
//    @DisplayName("Verify the click functionality of the privacy policy link on the newsletter widget on the article page")
//    public void newsletterArticlePrivacyPolicyClick() {
//        HomePage homePage = new HomePage(driver);
//       // homePage.clickRandomArticle(homePage.mainArticleElements);
//
//        Article article = new Article(driver);
//        article.waitForElement(article.articleContent);
//        article.waitForFrameAndSwitch_String(article.mainArticleSubscriptionFrame);
//        article.isDisplayed(article.mainArticleNewsletterPrivacy);
//        article.click(article.mainArticleNewsletterPrivacy);
//        homePage.mainWindow = driver.getWindowHandle();
//        article.waitForElement(homePage.genericSelector());
//        ArrayList tabs = new ArrayList(driver.getWindowHandles());
//        driver.switchTo().window(String.valueOf(tabs.get(1)));
//        homePage.secondWindow = driver.getWindowHandle();
//        assert driver.getCurrentUrl().equals("https://www.archant.co.uk/articles/privacy-policy/");
//        driver.close();
//        driver.switchTo().window(homePage.mainWindow);
//    }
//
//    @Test
//    @DisplayName("Verify an error appears when submitting the newsletter widget on an article page with nothing entered")
//    public void newsletterArticleNothingEntered() {
//        HomePage homePage = new HomePage(driver);
//        //homePage.clickRandomArticle(homePage.mainArticleElements);
//
//        Article article = new Article(driver);
//        article.waitForElement(article.articleContent);
//        article.waitForFrameAndSwitch_String(article.mainArticleSubscriptionFrame);
//        article.isDisplayed(article.mainArticleNewsletterWidget);
//        article.click(article.mainArticleNewsletterSubmit);
//        String errorMessage = driver.switchTo().alert().getText();
//        String errorMessageExpectedText = "Please provide a valid email address";
//        assertEquals(errorMessage, errorMessageExpectedText);
//        driver.switchTo().alert().dismiss();
//    }

//    @Test
//    @DisplayName("Verify a user is able to sign up sucesfully to the newsletter on an article page")
//    public void newsletterArticleSuccessfulForm() throws InterruptedException {
//
//        TempMail tempMail = new TempMail(driver);
//        tempMail.fetchTempMailEmail();
//
//        HomePage homePage = new HomePage(driver);
//        homePage.fetch(EASTERN_DAILY_PRESS);
//        homePage.clickRandomArticle(By.cssSelector(".teaser-title"));
//
//        Article article = new Article(driver);
//        article.waitForElement(article.articleContent);
//        article.waitForFrameAndSwitch_String(article.mainArticleSubscriptionFrame);
//        article.isDisplayed(article.mainArticleNewsletterWidget);
//
//        homePage.fillInNewsletter_Article(tempMail.tempMailEmail);
//        homePage.click(homePage.newsletter_JoinButton);
//        homePage.isDisplayed(homePage.newsletter_ConfirmationText1);
//        homePage.checkTextMatches(homePage.newsletter_ConfirmationText1, "Thank you for your subscription to this newsletter, an email with the link has been sent to your registered address.");
//        homePage.checkTextMatches(homePage.newsletter_ConfirmationText2, "If the email doesn't appear within the next few minutes, please check your junk mail filters in case it has been misplaced.");
//
//        homePage.fetch(TEMPMAIL);
//
//        Thread.sleep(4000);
//        tempMail.waitForElement(tempMail.newsletterSubjectHeader);
//        tempMail.scrollToElement(tempMail.newsletterSubjectHeader);
//        tempMail.click(tempMail.newsletterSubjectHeader);
//        Thread.sleep(2000);
//        tempMail.waitForElement(tempMail.newsletterEmailBody);
//        tempMail.isDisplayed(tempMail.newsletterEmailBody);
//
//    }
}
