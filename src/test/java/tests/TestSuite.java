package tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
        SmokeTest.class,
        HomePageTest.class,
        RegisterTest.class,
        NavigationTest.class,
        ArticleTest.class,
        HotTopicsTest.class,
        SearchTest.class,
        FormsTest.class,
        NewsletterTest.class
})

public class TestSuite {
    // the class remains empty,
    // used only as a holder for the above annotations
}
