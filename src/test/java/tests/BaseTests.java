package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Allure;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pageobjects.CookiesPopup;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

public class BaseTests {

    public WebDriver driver;

    @Before
    public void before() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("headless");
        options.addArguments("window-size=1200x600");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        CookiesPopup cookiesPopup = new CookiesPopup(driver);
        cookiesPopup.loadPage();
        cookiesPopup.clickAcceptButton();
    }

    @Rule
    public TestRule watcher = new TestWatcher() {
        @Override
        protected void failed(Throwable throwable, Description description) {
            File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            try {
                FileUtils.copyFile(srcFile, new File("./screenshots/"
                        + description.getClassName()
                        + "_" + description.getMethodName()
                        + ".png"));
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(srcFile));
                Allure.addAttachment(description.getClassName() + "_" + description.getMethodName(), byteArrayInputStream);
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }

        @Override
        public void finished(Description description) {
            driver.quit();
        }
    };
}