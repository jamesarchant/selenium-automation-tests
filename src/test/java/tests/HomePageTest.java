package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Allure;
import io.qameta.allure.Flaky;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit4.DisplayName;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import static org.testng.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.chrome.ChromeOptions;
import pageobjects.CookiesPopup;
import pageobjects.HomePage;

public class HomePageTest {

    private WebDriver driver;

    @Before
    public void before() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("headless");
        options.addArguments("window-size=1200x600");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Verify the presence of the cookies popup")
    public void verifyCookiesPopup() {
        CookiesPopup cookiesPopup = new CookiesPopup(driver);
        cookiesPopup.loadPage();
        assertTrue(cookiesPopup.checkHeaderTextIsDisplayed());
        assertTrue(cookiesPopup.checkBodyTextIsDisplayed());
        assertTrue(cookiesPopup.checkAcceptButtonIsDisplayed());
        assertTrue(cookiesPopup.checkShowOptionsTextIsDisplayed());
    }

    @Test
    @DisplayName("Confirm the cookies popup is responsive")
    public void verifyCookiesPopupResponsiveness() {
        CookiesPopup cookiesPopup = new CookiesPopup(driver);
        cookiesPopup.loadPage();
        assertTrue(cookiesPopup.checkHeaderTextIsDisplayed());
        assertTrue(cookiesPopup.checkBodyTextIsDisplayed());
        assertTrue(cookiesPopup.checkAcceptButtonIsDisplayed());
        assertTrue(cookiesPopup.checkShowOptionsTextIsDisplayed());
        cookiesPopup.minimizeWindow();
        assertTrue(cookiesPopup.checkHeaderTextIsDisplayed());
        assertTrue(cookiesPopup.checkBodyTextIsDisplayed());
        assertTrue(cookiesPopup.checkAcceptButtonIsDisplayed());
        assertTrue(cookiesPopup.checkShowOptionsTextIsDisplayed());
    }

    @Test
    @DisplayName("Confirm that the cookies popup click functionality works as expected")
    public void verifyCookiesPopupClickFunctionality() {
        CookiesPopup cookiesPopup = new CookiesPopup(driver);
        cookiesPopup.loadPage();
        assertTrue(cookiesPopup.checkHeaderTextIsDisplayed());
        assertTrue(cookiesPopup.checkBodyTextIsDisplayed());
        assertTrue(cookiesPopup.checkAcceptButtonIsDisplayed());
        assertTrue(cookiesPopup.checkShowOptionsTextIsDisplayed());
        cookiesPopup.clickShowOptions();
        cookiesPopup.clickCancelButton();
        cookiesPopup.clickAcceptButton();
    }

    @Test
    @DisplayName("Verify the presence of the main content news section")
    public void verifyPageLayout() {
        CookiesPopup cookiesPopup = new CookiesPopup(driver);
        cookiesPopup.loadPage();
        assertTrue(cookiesPopup.checkHeaderTextIsDisplayed());
        assertTrue(cookiesPopup.checkBodyTextIsDisplayed());
        assertTrue(cookiesPopup.checkAcceptButtonIsDisplayed());
        assertTrue(cookiesPopup.checkShowOptionsTextIsDisplayed());
        cookiesPopup.clickAcceptButton();

        HomePage homePage = new HomePage(driver);
        assertTrue(homePage.checkMainNewsIsDisplayed());

    }

    @Test
    @DisplayName("Confirm that the main page layout is responsive")
    public void verifyPageResponsiveness() {
        CookiesPopup cookiesPopup = new CookiesPopup(driver);
        cookiesPopup.loadPage();
        assertTrue(cookiesPopup.checkHeaderTextIsDisplayed());
        assertTrue(cookiesPopup.checkBodyTextIsDisplayed());
        assertTrue(cookiesPopup.checkAcceptButtonIsDisplayed());
        assertTrue(cookiesPopup.checkShowOptionsTextIsDisplayed());
        cookiesPopup.clickAcceptButton();

        HomePage homePage = new HomePage(driver);
        assertTrue(homePage.checkMainNewsIsDisplayed());
        homePage.minimizeWindow();
        assertTrue(homePage.checkMainNewsIsDisplayed());
    }


    @Test
    @Flaky
    @DisplayName("Verify the presence of the side widgets, including the Newsletter, Jobs, and Drive Widget. The Most Read queue may or may not appear")
    public void verifySideWidgets() {
        CookiesPopup cookiesPopup = new CookiesPopup(driver);
        cookiesPopup.loadPage();
        cookiesPopup.clickAcceptButton();

        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        assertTrue(homePage.checkNewsletterWidgetIsDisplayed());
        assertTrue(homePage.checkJobs24WidgetIsDisplayed());
        assertTrue(homePage.checkDriveWidgetIsDisplayed());
        assertTrue(homePage.checkMostReadQueueIsDisplayed());
        assertTrue(homePage.checkFeaturedPagesWidgetIsDisplayed());
        assertTrue(homePage.checkDigitalEditionsWidgetIsDisplayed());
        assertTrue(homePage.checkGreatDaysOutWidgetIsDisplayed());
        assertTrue(homePage.checkPlanningFinderWidgetIsDisplayed());
        assertTrue(homePage.checkLocalGuideWidgetIsDisplayed());
    }

    @Test
    @DisplayName("Verify that the side widgets are responsive")
    public void verifyHomePageSideWidgetsResponsiveness() {
        CookiesPopup cookiesPopup = new CookiesPopup(driver);
        cookiesPopup.loadPage();
        cookiesPopup.clickAcceptButton();

        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        assertTrue(homePage.checkNewsletterWidgetIsDisplayed());
        assertTrue(homePage.checkDriveWidgetIsDisplayed());
        assertTrue(homePage.checkMostReadQueueIsDisplayed());
        assertTrue(homePage.checkFeaturedPagesWidgetIsDisplayed());
        assertTrue(homePage.checkDigitalEditionsWidgetIsDisplayed());
        assertTrue(homePage.checkGreatDaysOutWidgetIsDisplayed());
        assertTrue(homePage.checkPlanningFinderWidgetIsDisplayed());
        assertTrue(homePage.checkLocalGuideWidgetIsDisplayed());
        homePage.minimizeWindow();
        assertTrue(homePage.checkNewsletterWidgetIsDisplayed());
        assertTrue(homePage.checkDriveWidgetIsDisplayed());
        assertTrue(homePage.checkMostReadQueueIsDisplayed());
        assertTrue(homePage.checkFeaturedPagesWidgetIsDisplayed());
        assertTrue(homePage.checkDigitalEditionsWidgetIsDisplayed());
        assertTrue(homePage.checkGreatDaysOutWidgetIsDisplayed());
        assertTrue(homePage.checkPlanningFinderWidgetIsDisplayed());
        assertTrue(homePage.checkLocalGuideWidgetIsDisplayed());
    }


    @Rule
    public TestRule watcher = new TestWatcher() {
        @Override
        protected void failed(Throwable throwable, Description description) {
            File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            try {
                FileUtils.copyFile(srcFile, new File("./screenshots/"
                        + description.getClassName()
                        + "_" + description.getMethodName()
                        + ".png"));
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(srcFile));
                Allure.addAttachment(description.getClassName() + "_" + description.getMethodName(), byteArrayInputStream);
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }

        @Override
        public void finished(Description description) {
            driver.quit();
        }
    };
}