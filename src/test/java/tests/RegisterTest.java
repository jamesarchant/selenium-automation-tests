package tests;

import io.qameta.allure.junit4.DisplayName;
import org.junit.*;
import pageobjects.*;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class RegisterTest extends BaseTests {

    @Test
    @DisplayName("Click the register link")
    public void clickRegisterLink() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickRegisterLink();

        Register register = new Register(driver);
        register.waitForRegisterForm();
        assertTrue(register.checkGDPRHeaderIsDisplayed());
        assertEquals(register.getGDPRHeaderText(), "Archant Community Media Limited will only use your data for the purpose of the services you are signing up to and the marketing consent you’ve given using the check boxes. We do not share you data with third parties unless you have given us consent to do so. Your data will be used in accordance with our privacy policy which can be found here. You can remove your permission for our use of your data at any time by emailing dpo@archant.co.uk or remove marketing permission by clicking on the unsubscribe link(s) in our communications.");
        assertTrue(register.checkPersonDetailsHeaderIsDisplayed());
        assertEquals(register.getPersonalDetailsHeaderText(), "Personal details");
        assertTrue(register.checkEmailAndPasswordHeaderIsDisplayed());
        assertEquals(register.getEmailAndPasswordText(), "Email and password");
        assertTrue(register.checkTitleLabelIsDisplayed());
        assertEquals(register.getTitleLabel(), "Title*");
        assertTrue(register.checkTitleIsDisplayed());
        assertTrue(register.checkFirstNameLabelIsDisplayed());
        assertEquals(register.getFirstNameLabel(), "First name*");
        assertTrue(register.checkFirstNameIsDisplayed());
        assertTrue(register.checkSurnameLabelIsDisplayed());
        assertEquals(register.getSurnameLabel(), "Surname*");
        assertTrue(register.checkSurnameIsDisplayed());
        assertTrue(register.checkScreenNameLabelIsDisplayed());
        assertEquals(register.getScreenName(), "Screen name*");
        assertTrue(register.checkScreenNameIsDisplayed());
        assertTrue(register.checkScreenNameHelperTextIsDisplayed());
        assertEquals(register.getScreeNameHelperText(), "Screen names can only contain alphanumeric characters, combined with either the _ or - character. Your screen name will be used to identify you publicly, for example next to comments");
        assertTrue(register.checkEmailAddressTextIsDisplayed());
        assertEquals(register.getEmailAddress(), "Email address*");
        assertTrue(register.checkEmailAddressIsDisplayed());
        assertTrue(register.checkPasswordTextIsDisplayed());
        assertEquals(register.getPassword(), "Password*");
        assertTrue(register.checkPasswordIsDisplayed());
        assertTrue(register.checkConfirmPasswordTextIsDisplayed());
        assertEquals(register.getConfirmPassword(), "Confirm password*");
        assertTrue(register.checkConfirmPasswordIsDisplayed());
        assertTrue(register.checkNewsletterLabelIsDisplayed());
        assertEquals(register.getNewsletterLabelText(), "Sign up to receive our regular email newsletter");
        assertTrue(register.checkNewsletterCheckboxIsDisplayed());
        assertTrue(register.checkPromotionsLabelIsDisplayed());
        assertEquals(register.getPromotionsLabelText(), "Keep me up to date on special promotions, products and services from Archant Community Media Limited");
        assertTrue(register.checkPromotionsCheckboxIsDisplayed());
        assertTrue(register.checkThirdPartiesLabelIsDisplayed());
        assertEquals(register.getThirdPartiesLabelText(), "I am happy for Archant Community Media Ltd to contact me on behalf of third parties (we will not share your information with these third parties unless you give us consent to do so)");
        assertTrue(register.checkThirdPartiesCheckboxIsDisplayed());
        assertTrue(register.checkTermsAndConditionsLabelIsDisplayed());
        assertEquals(register.getTermsAndConditionsLabelText(), "I have read and accept the terms and conditions");
        assertTrue(register.checkTermsAndConditionsCheckboxIsDisplayed());
        assertTrue(register.checkRegisterButtonIsDisplayed());
        assertEquals(register.getRegisterButtonText(), "Register now");
    }

    @Test
    @DisplayName("Verify the presence and click functionality of the privacy policy link on the register page")
    public void verifyRegisterForm_ClickPrivacy() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickRegisterLink();

        Register register = new Register(driver);
        register.waitForRegisterForm();
        assertTrue(register.checkGDPRHeaderIsDisplayed());
        assertEquals(register.getGDPRHeaderText(), "Archant Community Media Limited will only use your data for the purpose of the services you are signing up to and the marketing consent you’ve given using the check boxes. We do not share you data with third parties unless you have given us consent to do so. Your data will be used in accordance with our privacy policy which can be found here. You can remove your permission for our use of your data at any time by emailing dpo@archant.co.uk or remove marketing permission by clicking on the unsubscribe link(s) in our communications.");
        assertTrue(register.checkPersonDetailsHeaderIsDisplayed());
        assertEquals(register.getPersonalDetailsHeaderText(), "Personal details");
        assertTrue(register.checkEmailAndPasswordHeaderIsDisplayed());
        assertEquals(register.getEmailAndPasswordText(), "Email and password");
        assertTrue(register.checkTitleLabelIsDisplayed());
        assertEquals(register.getTitleLabel(), "Title*");
        assertTrue(register.checkTitleIsDisplayed());
        assertTrue(register.checkFirstNameLabelIsDisplayed());
        assertEquals(register.getFirstNameLabel(), "First name*");
        assertTrue(register.checkFirstNameIsDisplayed());
        assertTrue(register.checkSurnameLabelIsDisplayed());
        assertEquals(register.getSurnameLabel(), "Surname*");
        assertTrue(register.checkSurnameIsDisplayed());
        assertTrue(register.checkScreenNameLabelIsDisplayed());
        assertEquals(register.getScreenName(), "Screen name*");
        assertTrue(register.checkScreenNameIsDisplayed());
        assertTrue(register.checkScreenNameHelperTextIsDisplayed());
        assertEquals(register.getScreeNameHelperText(), "Screen names can only contain alphanumeric characters, combined with either the _ or - character. Your screen name will be used to identify you publicly, for example next to comments");
        assertTrue(register.checkEmailAddressTextIsDisplayed());
        assertEquals(register.getEmailAddress(), "Email address*");
        assertTrue(register.checkEmailAddressIsDisplayed());
        assertTrue(register.checkPasswordTextIsDisplayed());
        assertEquals(register.getPassword(), "Password*");
        assertTrue(register.checkPasswordIsDisplayed());
        assertTrue(register.checkConfirmPasswordTextIsDisplayed());
        assertEquals(register.getConfirmPassword(), "Confirm password*");
        assertTrue(register.checkConfirmPasswordIsDisplayed());
        assertTrue(register.checkNewsletterLabelIsDisplayed());
        assertEquals(register.getNewsletterLabelText(), "Sign up to receive our regular email newsletter");
        assertTrue(register.checkNewsletterCheckboxIsDisplayed());
        assertTrue(register.checkPromotionsLabelIsDisplayed());
        assertEquals(register.getPromotionsLabelText(), "Keep me up to date on special promotions, products and services from Archant Community Media Limited");
        assertTrue(register.checkPromotionsCheckboxIsDisplayed());
        assertTrue(register.checkThirdPartiesLabelIsDisplayed());
        assertEquals(register.getThirdPartiesLabelText(), "I am happy for Archant Community Media Ltd to contact me on behalf of third parties (we will not share your information with these third parties unless you give us consent to do so)");
        assertTrue(register.checkThirdPartiesCheckboxIsDisplayed());
        assertTrue(register.checkTermsAndConditionsLabelIsDisplayed());
        assertEquals(register.getTermsAndConditionsLabelText(), "I have read and accept the terms and conditions");
        assertTrue(register.checkTermsAndConditionsCheckboxIsDisplayed());
        assertTrue(register.checkRegisterButtonIsDisplayed());
        assertEquals(register.getRegisterButtonText(), "Register now");

        register.clickPrivacyLink("https://www.archant.co.uk/articles/privacy-policy/");
    }

    @Test
    @DisplayName("Verify the responsiveness of the register form")
    public void verifyFormResponsiveness() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickRegisterLink();

        Register register = new Register(driver);
        assertTrue(register.checkGDPRHeaderIsDisplayed());
        assertEquals(register.getGDPRHeaderText(), "Archant Community Media Limited will only use your data for the purpose of the services you are signing up to and the marketing consent you’ve given using the check boxes. We do not share you data with third parties unless you have given us consent to do so. Your data will be used in accordance with our privacy policy which can be found here. You can remove your permission for our use of your data at any time by emailing dpo@archant.co.uk or remove marketing permission by clicking on the unsubscribe link(s) in our communications.");
        assertTrue(register.checkPersonDetailsHeaderIsDisplayed());
        assertEquals(register.getPersonalDetailsHeaderText(), "Personal details");
        assertTrue(register.checkEmailAndPasswordHeaderIsDisplayed());
        assertEquals(register.getEmailAndPasswordText(), "Email and password");
        assertTrue(register.checkTitleLabelIsDisplayed());
        assertEquals(register.getTitleLabel(), "Title*");
        assertTrue(register.checkTitleIsDisplayed());
        assertTrue(register.checkFirstNameLabelIsDisplayed());
        assertEquals(register.getFirstNameLabel(), "First name*");
        assertTrue(register.checkFirstNameIsDisplayed());
        assertTrue(register.checkSurnameLabelIsDisplayed());
        assertEquals(register.getSurnameLabel(), "Surname*");
        assertTrue(register.checkSurnameIsDisplayed());
        assertTrue(register.checkScreenNameLabelIsDisplayed());
        assertEquals(register.getScreenName(), "Screen name*");
        assertTrue(register.checkScreenNameIsDisplayed());
        assertTrue(register.checkScreenNameHelperTextIsDisplayed());
        assertEquals(register.getScreeNameHelperText(), "Screen names can only contain alphanumeric characters, combined with either the _ or - character. Your screen name will be used to identify you publicly, for example next to comments");
        assertTrue(register.checkEmailAddressTextIsDisplayed());
        assertEquals(register.getEmailAddress(), "Email address*");
        assertTrue(register.checkEmailAddressIsDisplayed());
        assertTrue(register.checkPasswordTextIsDisplayed());
        assertEquals(register.getPassword(), "Password*");
        assertTrue(register.checkPasswordIsDisplayed());
        assertTrue(register.checkConfirmPasswordTextIsDisplayed());
        assertEquals(register.getConfirmPassword(), "Confirm password*");
        assertTrue(register.checkConfirmPasswordIsDisplayed());
        assertTrue(register.checkNewsletterLabelIsDisplayed());
        assertEquals(register.getNewsletterLabelText(), "Sign up to receive our regular email newsletter");
        assertTrue(register.checkNewsletterCheckboxIsDisplayed());
        assertTrue(register.checkPromotionsLabelIsDisplayed());
        assertEquals(register.getPromotionsLabelText(), "Keep me up to date on special promotions, products and services from Archant Community Media Limited");
        assertTrue(register.checkPromotionsCheckboxIsDisplayed());
        assertTrue(register.checkThirdPartiesLabelIsDisplayed());
        assertEquals(register.getThirdPartiesLabelText(), "I am happy for Archant Community Media Ltd to contact me on behalf of third parties (we will not share your information with these third parties unless you give us consent to do so)");
        assertTrue(register.checkThirdPartiesCheckboxIsDisplayed());
        assertTrue(register.checkTermsAndConditionsLabelIsDisplayed());
        assertEquals(register.getTermsAndConditionsLabelText(), "I have read and accept the terms and conditions");
        assertTrue(register.checkTermsAndConditionsCheckboxIsDisplayed());
        assertTrue(register.checkRegisterButtonIsDisplayed());
        assertEquals(register.getRegisterButtonText(), "Register now");

        register.minimizeWindow();

        assertTrue(register.checkGDPRHeaderIsDisplayed());
        assertEquals(register.getGDPRHeaderText(), "Archant Community Media Limited will only use your data for the purpose of the services you are signing up to and the marketing consent you’ve given using the check boxes. We do not share you data with third parties unless you have given us consent to do so. Your data will be used in accordance with our privacy policy which can be found here. You can remove your permission for our use of your data at any time by emailing dpo@archant.co.uk or remove marketing permission by clicking on the unsubscribe link(s) in our communications.");
        assertTrue(register.checkPersonDetailsHeaderIsDisplayed());
        assertEquals(register.getPersonalDetailsHeaderText(), "Personal details");
        assertTrue(register.checkEmailAndPasswordHeaderIsDisplayed());
        assertEquals(register.getEmailAndPasswordText(), "Email and password");
        assertTrue(register.checkTitleLabelIsDisplayed());
        assertEquals(register.getTitleLabel(), "Title*");
        assertTrue(register.checkTitleIsDisplayed());
        assertTrue(register.checkFirstNameLabelIsDisplayed());
        assertEquals(register.getFirstNameLabel(), "First name*");
        assertTrue(register.checkFirstNameIsDisplayed());
        assertTrue(register.checkSurnameLabelIsDisplayed());
        assertEquals(register.getSurnameLabel(), "Surname*");
        assertTrue(register.checkSurnameIsDisplayed());
        assertTrue(register.checkScreenNameLabelIsDisplayed());
        assertEquals(register.getScreenName(), "Screen name*");
        assertTrue(register.checkScreenNameIsDisplayed());
        assertTrue(register.checkScreenNameHelperTextIsDisplayed());
        assertEquals(register.getScreeNameHelperText(), "Screen names can only contain alphanumeric characters, combined with either the _ or - character. Your screen name will be used to identify you publicly, for example next to comments");
        assertTrue(register.checkEmailAddressTextIsDisplayed());
        assertEquals(register.getEmailAddress(), "Email address*");
        assertTrue(register.checkEmailAddressIsDisplayed());
        assertTrue(register.checkPasswordTextIsDisplayed());
        assertEquals(register.getPassword(), "Password*");
        assertTrue(register.checkPasswordIsDisplayed());
        assertTrue(register.checkConfirmPasswordTextIsDisplayed());
        assertEquals(register.getConfirmPassword(), "Confirm password*");
        assertTrue(register.checkConfirmPasswordIsDisplayed());
        assertTrue(register.checkNewsletterLabelIsDisplayed());
        assertEquals(register.getNewsletterLabelText(), "Sign up to receive our regular email newsletter");
        assertTrue(register.checkNewsletterCheckboxIsDisplayed());
        assertTrue(register.checkPromotionsLabelIsDisplayed());
        assertEquals(register.getPromotionsLabelText(), "Keep me up to date on special promotions, products and services from Archant Community Media Limited");
        assertTrue(register.checkPromotionsCheckboxIsDisplayed());
        assertTrue(register.checkThirdPartiesLabelIsDisplayed());
        assertEquals(register.getThirdPartiesLabelText(), "I am happy for Archant Community Media Ltd to contact me on behalf of third parties (we will not share your information with these third parties unless you give us consent to do so)");
        assertTrue(register.checkThirdPartiesCheckboxIsDisplayed());
        assertTrue(register.checkTermsAndConditionsLabelIsDisplayed());
        assertEquals(register.getTermsAndConditionsLabelText(), "I have read and accept the terms and conditions");
        assertTrue(register.checkTermsAndConditionsCheckboxIsDisplayed());
        assertTrue(register.checkRegisterButtonIsDisplayed());
        assertEquals(register.getRegisterButtonText(), "Register now");
    }

    @Test
    @DisplayName("Verify the presence and click functionality of the terms and conditions link on the register page")
    public void verifyRegisterForm_ClickTerms() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickRegisterLink();

        Register register = new Register(driver);
        register.waitForRegisterForm();
        assertTrue(register.checkGDPRHeaderIsDisplayed());
        assertEquals(register.getGDPRHeaderText(), "Archant Community Media Limited will only use your data for the purpose of the services you are signing up to and the marketing consent you’ve given using the check boxes. We do not share you data with third parties unless you have given us consent to do so. Your data will be used in accordance with our privacy policy which can be found here. You can remove your permission for our use of your data at any time by emailing dpo@archant.co.uk or remove marketing permission by clicking on the unsubscribe link(s) in our communications.");
        assertTrue(register.checkPersonDetailsHeaderIsDisplayed());
        assertEquals(register.getPersonalDetailsHeaderText(), "Personal details");
        assertTrue(register.checkEmailAndPasswordHeaderIsDisplayed());
        assertEquals(register.getEmailAndPasswordText(), "Email and password");
        assertTrue(register.checkTitleLabelIsDisplayed());
        assertEquals(register.getTitleLabel(), "Title*");
        assertTrue(register.checkTitleIsDisplayed());
        assertTrue(register.checkFirstNameLabelIsDisplayed());
        assertEquals(register.getFirstNameLabel(), "First name*");
        assertTrue(register.checkFirstNameIsDisplayed());
        assertTrue(register.checkSurnameLabelIsDisplayed());
        assertEquals(register.getSurnameLabel(), "Surname*");
        assertTrue(register.checkSurnameIsDisplayed());
        assertTrue(register.checkScreenNameLabelIsDisplayed());
        assertEquals(register.getScreenName(), "Screen name*");
        assertTrue(register.checkScreenNameIsDisplayed());
        assertTrue(register.checkScreenNameHelperTextIsDisplayed());
        assertEquals(register.getScreeNameHelperText(), "Screen names can only contain alphanumeric characters, combined with either the _ or - character. Your screen name will be used to identify you publicly, for example next to comments");
        assertTrue(register.checkEmailAddressTextIsDisplayed());
        assertEquals(register.getEmailAddress(), "Email address*");
        assertTrue(register.checkEmailAddressIsDisplayed());
        assertTrue(register.checkPasswordTextIsDisplayed());
        assertEquals(register.getPassword(), "Password*");
        assertTrue(register.checkPasswordIsDisplayed());
        assertTrue(register.checkConfirmPasswordTextIsDisplayed());
        assertEquals(register.getConfirmPassword(), "Confirm password*");
        assertTrue(register.checkConfirmPasswordIsDisplayed());
        assertTrue(register.checkNewsletterLabelIsDisplayed());
        assertEquals(register.getNewsletterLabelText(), "Sign up to receive our regular email newsletter");
        assertTrue(register.checkNewsletterCheckboxIsDisplayed());
        assertTrue(register.checkPromotionsLabelIsDisplayed());
        assertEquals(register.getPromotionsLabelText(), "Keep me up to date on special promotions, products and services from Archant Community Media Limited");
        assertTrue(register.checkPromotionsCheckboxIsDisplayed());
        assertTrue(register.checkThirdPartiesLabelIsDisplayed());
        assertEquals(register.getThirdPartiesLabelText(), "I am happy for Archant Community Media Ltd to contact me on behalf of third parties (we will not share your information with these third parties unless you give us consent to do so)");
        assertTrue(register.checkThirdPartiesCheckboxIsDisplayed());
        assertTrue(register.checkTermsAndConditionsLabelIsDisplayed());
        assertEquals(register.getTermsAndConditionsLabelText(), "I have read and accept the terms and conditions");
        assertTrue(register.checkTermsAndConditionsCheckboxIsDisplayed());
        assertTrue(register.checkRegisterButtonIsDisplayed());
        assertEquals(register.getRegisterButtonText(), "Register now");

        register.clickTermsLink("https://www.archant.co.uk/articles/terms-conditions/");
    }

    @Test
    @DisplayName("Verify that the relevant error fields appear when attempting to submit the form without any fields entered")
    public void submitForm_NoFieldsEntered() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();

        homePage.clickRegisterLink();

        Register register = new Register(driver);
        register.waitForRegisterForm();
        assertTrue(register.checkGDPRHeaderIsDisplayed());
        assertEquals(register.getGDPRHeaderText(), "Archant Community Media Limited will only use your data for the purpose of the services you are signing up to and the marketing consent you’ve given using the check boxes. We do not share you data with third parties unless you have given us consent to do so. Your data will be used in accordance with our privacy policy which can be found here. You can remove your permission for our use of your data at any time by emailing dpo@archant.co.uk or remove marketing permission by clicking on the unsubscribe link(s) in our communications.");
        assertTrue(register.checkPersonDetailsHeaderIsDisplayed());
        assertEquals(register.getPersonalDetailsHeaderText(), "Personal details");
        assertTrue(register.checkEmailAndPasswordHeaderIsDisplayed());
        assertEquals(register.getEmailAndPasswordText(), "Email and password");
        assertTrue(register.checkTitleLabelIsDisplayed());
        assertEquals(register.getTitleLabel(), "Title*");
        assertTrue(register.checkTitleIsDisplayed());
        assertTrue(register.checkFirstNameLabelIsDisplayed());
        assertEquals(register.getFirstNameLabel(), "First name*");
        assertTrue(register.checkFirstNameIsDisplayed());
        assertTrue(register.checkSurnameLabelIsDisplayed());
        assertEquals(register.getSurnameLabel(), "Surname*");
        assertTrue(register.checkSurnameIsDisplayed());
        assertTrue(register.checkScreenNameLabelIsDisplayed());
        assertEquals(register.getScreenName(), "Screen name*");
        assertTrue(register.checkScreenNameIsDisplayed());
        assertTrue(register.checkScreenNameHelperTextIsDisplayed());
        assertEquals(register.getScreeNameHelperText(), "Screen names can only contain alphanumeric characters, combined with either the _ or - character. Your screen name will be used to identify you publicly, for example next to comments");
        assertTrue(register.checkEmailAddressTextIsDisplayed());
        assertEquals(register.getEmailAddress(), "Email address*");
        assertTrue(register.checkEmailAddressIsDisplayed());
        assertTrue(register.checkPasswordTextIsDisplayed());
        assertEquals(register.getPassword(), "Password*");
        assertTrue(register.checkPasswordIsDisplayed());
        assertTrue(register.checkConfirmPasswordTextIsDisplayed());
        assertEquals(register.getConfirmPassword(), "Confirm password*");
        assertTrue(register.checkConfirmPasswordIsDisplayed());
        assertTrue(register.checkNewsletterLabelIsDisplayed());
        assertEquals(register.getNewsletterLabelText(), "Sign up to receive our regular email newsletter");
        assertTrue(register.checkNewsletterCheckboxIsDisplayed());
        assertTrue(register.checkPromotionsLabelIsDisplayed());
        assertEquals(register.getPromotionsLabelText(), "Keep me up to date on special promotions, products and services from Archant Community Media Limited");
        assertTrue(register.checkPromotionsCheckboxIsDisplayed());
        assertTrue(register.checkThirdPartiesLabelIsDisplayed());
        assertEquals(register.getThirdPartiesLabelText(), "I am happy for Archant Community Media Ltd to contact me on behalf of third parties (we will not share your information with these third parties unless you give us consent to do so)");
        assertTrue(register.checkThirdPartiesCheckboxIsDisplayed());
        assertTrue(register.checkTermsAndConditionsLabelIsDisplayed());
        assertEquals(register.getTermsAndConditionsLabelText(), "I have read and accept the terms and conditions");
        assertTrue(register.checkTermsAndConditionsCheckboxIsDisplayed());
        assertTrue(register.checkRegisterButtonIsDisplayed());
        assertEquals(register.getRegisterButtonText(), "Register now");

        register.clickRegisterButton();
        register.waitForErrorText();
        assertTrue(register.checkErrorHeadingTextIsDisplayed());
        assertEquals(register.getErrorHeaderText(), "Sorry, there's a problem:");
        assertTrue(register.checkFirstErrorIsDisplayed());
        assertEquals(register.getFirstError(), "You must select a title");
        assertTrue(register.checkSecondErrorIsDisplayed());
        assertEquals(register.getSecondError(), "You must provide a first name");
        assertTrue(register.checkThirdErrorIsDisplayed());
        assertEquals(register.getThirdError(), "You must provide a surname");
        assertTrue(register.checkFourthErrorIsDisplayed());
        assertEquals(register.getFourthError(), "You must provide an email address");
        assertTrue(register.checkFifthErrorIsDisplayed());
        assertEquals(register.getFifthError(), "This email address is invalid");
        assertTrue(register.checkSixthErrorIsDisplayed());
        assertEquals(register.getSixthError(), "You must provide a password");
        assertTrue(register.checkSeventhErrorIsDisplayed());
        assertEquals(register.getSeventhError(), "This password is invalid");
        assertTrue(register.checkEighthErrorIsDisplayed());
        assertEquals(register.getEightError(), "You must provide a confirmation password");
        assertTrue(register.checkNinthErrorIsDisplayed());
        assertEquals(register.getNinthError(), "The confirmation password is invalid");
        assertTrue(register.checkTenthErrorIsDisplayed());
        assertEquals(register.getTenthError(), "You need to accept the terms and conditions to register.");
    }

    @Test
    @DisplayName("Verify that an error message appears when attempting to register without ticking the terms and conditions")
    public void submitForm_NoTermsChecked() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickRegisterLink();

        Register register = new Register(driver);
        register.waitForRegisterForm();
        assertTrue(register.checkGDPRHeaderIsDisplayed());
        assertEquals(register.getGDPRHeaderText(), "Archant Community Media Limited will only use your data for the purpose of the services you are signing up to and the marketing consent you’ve given using the check boxes. We do not share you data with third parties unless you have given us consent to do so. Your data will be used in accordance with our privacy policy which can be found here. You can remove your permission for our use of your data at any time by emailing dpo@archant.co.uk or remove marketing permission by clicking on the unsubscribe link(s) in our communications.");
        assertTrue(register.checkPersonDetailsHeaderIsDisplayed());
        assertEquals(register.getPersonalDetailsHeaderText(), "Personal details");
        assertTrue(register.checkEmailAndPasswordHeaderIsDisplayed());
        assertEquals(register.getEmailAndPasswordText(), "Email and password");
        assertTrue(register.checkTitleLabelIsDisplayed());
        assertEquals(register.getTitleLabel(), "Title*");
        assertTrue(register.checkTitleIsDisplayed());
        assertTrue(register.checkFirstNameLabelIsDisplayed());
        assertEquals(register.getFirstNameLabel(), "First name*");
        assertTrue(register.checkFirstNameIsDisplayed());
        assertTrue(register.checkSurnameLabelIsDisplayed());
        assertEquals(register.getSurnameLabel(), "Surname*");
        assertTrue(register.checkSurnameIsDisplayed());
        assertTrue(register.checkScreenNameLabelIsDisplayed());
        assertEquals(register.getScreenName(), "Screen name*");
        assertTrue(register.checkScreenNameIsDisplayed());
        assertTrue(register.checkScreenNameHelperTextIsDisplayed());
        assertEquals(register.getScreeNameHelperText(), "Screen names can only contain alphanumeric characters, combined with either the _ or - character. Your screen name will be used to identify you publicly, for example next to comments");
        assertTrue(register.checkEmailAddressTextIsDisplayed());
        assertEquals(register.getEmailAddress(), "Email address*");
        assertTrue(register.checkEmailAddressIsDisplayed());
        assertTrue(register.checkPasswordTextIsDisplayed());
        assertEquals(register.getPassword(), "Password*");
        assertTrue(register.checkPasswordIsDisplayed());
        assertTrue(register.checkConfirmPasswordTextIsDisplayed());
        assertEquals(register.getConfirmPassword(), "Confirm password*");
        assertTrue(register.checkConfirmPasswordIsDisplayed());
        assertTrue(register.checkNewsletterLabelIsDisplayed());
        assertEquals(register.getNewsletterLabelText(), "Sign up to receive our regular email newsletter");
        assertTrue(register.checkNewsletterCheckboxIsDisplayed());
        assertTrue(register.checkPromotionsLabelIsDisplayed());
        assertEquals(register.getPromotionsLabelText(), "Keep me up to date on special promotions, products and services from Archant Community Media Limited");
        assertTrue(register.checkPromotionsCheckboxIsDisplayed());
        assertTrue(register.checkThirdPartiesLabelIsDisplayed());
        assertEquals(register.getThirdPartiesLabelText(), "I am happy for Archant Community Media Ltd to contact me on behalf of third parties (we will not share your information with these third parties unless you give us consent to do so)");
        assertTrue(register.checkThirdPartiesCheckboxIsDisplayed());
        assertTrue(register.checkTermsAndConditionsLabelIsDisplayed());
        assertEquals(register.getTermsAndConditionsLabelText(), "I have read and accept the terms and conditions");
        assertTrue(register.checkTermsAndConditionsCheckboxIsDisplayed());
        assertTrue(register.checkRegisterButtonIsDisplayed());
        assertEquals(register.getRegisterButtonText(), "Register now");

        register.setTitle(register.getRandomTitle());
        register.setFirstName(register.getRandomFirstName());
        register.setSurname(register.getRandomLastName());
        register.setScreenName(register.getRandomScreenName());
        register.setEmail("johnsmith@test.com");
        register.setPassword("Hellothere123");
        register.setConfirmPassword("Hellothere123");
        register.checkNewsletterCheckbox();
        register.checkPromotionsCheckbox();
        register.checkThirdPartiesCheckbox();
        register.clickRegisterButton();
        assertTrue(register.checkTermsErrorText());
        assertEquals(register.getTermsError(), "You need to accept the terms and conditions to register.");
    }

    @Test
    @DisplayName("Verify that an error message appears when attempting to register with an invalid screen name")
    public void submitForm_InvalidScreenName() {
        HomePage homePage = new HomePage(driver);
        homePage.clickRegisterLink();

        Register register = new Register(driver);
        register.waitForRegisterForm();
        assertTrue(register.checkGDPRHeaderIsDisplayed());
        assertEquals(register.getGDPRHeaderText(), "Archant Community Media Limited will only use your data for the purpose of the services you are signing up to and the marketing consent you’ve given using the check boxes. We do not share you data with third parties unless you have given us consent to do so. Your data will be used in accordance with our privacy policy which can be found here. You can remove your permission for our use of your data at any time by emailing dpo@archant.co.uk or remove marketing permission by clicking on the unsubscribe link(s) in our communications.");
        assertTrue(register.checkPersonDetailsHeaderIsDisplayed());
        assertEquals(register.getPersonalDetailsHeaderText(), "Personal details");
        assertTrue(register.checkEmailAndPasswordHeaderIsDisplayed());
        assertEquals(register.getEmailAndPasswordText(), "Email and password");
        assertTrue(register.checkTitleLabelIsDisplayed());
        assertEquals(register.getTitleLabel(), "Title*");
        assertTrue(register.checkTitleIsDisplayed());
        assertTrue(register.checkFirstNameLabelIsDisplayed());
        assertEquals(register.getFirstNameLabel(), "First name*");
        assertTrue(register.checkFirstNameIsDisplayed());
        assertTrue(register.checkSurnameLabelIsDisplayed());
        assertEquals(register.getSurnameLabel(), "Surname*");
        assertTrue(register.checkSurnameIsDisplayed());
        assertTrue(register.checkScreenNameLabelIsDisplayed());
        assertEquals(register.getScreenName(), "Screen name*");
        assertTrue(register.checkScreenNameIsDisplayed());
        assertTrue(register.checkScreenNameHelperTextIsDisplayed());
        assertEquals(register.getScreeNameHelperText(), "Screen names can only contain alphanumeric characters, combined with either the _ or - character. Your screen name will be used to identify you publicly, for example next to comments");
        assertTrue(register.checkEmailAddressTextIsDisplayed());
        assertEquals(register.getEmailAddress(), "Email address*");
        assertTrue(register.checkEmailAddressIsDisplayed());
        assertTrue(register.checkPasswordTextIsDisplayed());
        assertEquals(register.getPassword(), "Password*");
        assertTrue(register.checkPasswordIsDisplayed());
        assertTrue(register.checkConfirmPasswordTextIsDisplayed());
        assertEquals(register.getConfirmPassword(), "Confirm password*");
        assertTrue(register.checkConfirmPasswordIsDisplayed());
        assertTrue(register.checkNewsletterLabelIsDisplayed());
        assertEquals(register.getNewsletterLabelText(), "Sign up to receive our regular email newsletter");
        assertTrue(register.checkNewsletterCheckboxIsDisplayed());
        assertTrue(register.checkPromotionsLabelIsDisplayed());
        assertEquals(register.getPromotionsLabelText(), "Keep me up to date on special promotions, products and services from Archant Community Media Limited");
        assertTrue(register.checkPromotionsCheckboxIsDisplayed());
        assertTrue(register.checkThirdPartiesLabelIsDisplayed());
        assertEquals(register.getThirdPartiesLabelText(), "I am happy for Archant Community Media Ltd to contact me on behalf of third parties (we will not share your information with these third parties unless you give us consent to do so)");
        assertTrue(register.checkThirdPartiesCheckboxIsDisplayed());
        assertTrue(register.checkTermsAndConditionsLabelIsDisplayed());
        assertEquals(register.getTermsAndConditionsLabelText(), "I have read and accept the terms and conditions");
        assertTrue(register.checkTermsAndConditionsCheckboxIsDisplayed());
        assertTrue(register.checkRegisterButtonIsDisplayed());
        assertEquals(register.getRegisterButtonText(), "Register now");

        register.setTitle(register.getRandomTitle());
        register.setFirstName(register.getRandomFirstName());
        register.setSurname(register.getRandomLastName());
        register.setScreenName(register.getInvalidRandomScreenName());
        register.setEmail("johnsmith@test.com");
        register.setPassword("Hellothere123");
        register.setConfirmPassword("Hellothere123");
        register.checkNewsletterCheckbox();
        register.checkPromotionsCheckbox();
        register.checkThirdPartiesCheckbox();
        register.checkTermsCheckbox();
        register.clickRegisterButton();
        assertTrue(register.checkScreenNameErrorText());
        assertEquals(register.getScreenNameError(), "Screen name is invalid. Screen names can only contain alphanumeric characters, combined with either the _ or - character");
    }

//    @Test
//    @DisplayName("Verify that a user can succesfully register")
//    public void successfulRegister() throws InterruptedException {
//
//        TempMail tempMail = new TempMail(driver);
//        tempMail.fetchTempMailEmail();
//
//        HomePage homePage = new HomePage(driver);
//        homePage.fetch(EASTERN_DAILY_PRESS);
//        homePage.waitForElement(homePage.registrationAnd24LinksHeader);
//        homePage.click(homePage.registerLink);
//
//        Register register = new Register(driver);
//        register.fillInRegisterForm(tempMail.tempMailEmail);
//        register.click(register.registerButton);
//        try {
//            homePage.waitForElement(register.registerSuccessElement);
//            register.checkTextMatches(register.registerSuccessElement, "Thanks for registering");
//            registrationSuccessful = true;
//        } catch (NoSuchElementException e) {
//            System.out.println("Registration was not successfull");
//        }
//
//        // 3.05	Open the corresponding email inbox and confirm that the activation email has been received.
//        tempMail.fetch(TEMPMAIL);
//
//        // 3.06	Open the email and click on the activation link.
//        Thread.sleep(5000);
//        tempMail.scrollToElement(tempMail.emailSubjectLinkText);
//        tempMail.click(tempMail.emailSubjectLinkText);
//        Thread.sleep(2000);
//        tempMail.scrollToElement(tempMail.emailBody);
//        String textToParse = tempMail.getText(tempMail.emailBody);
//        tempMail.parseURL(textToParse);
//        tempMail.fetch(tempMail.tempMailUrl);
//
//        homePage.waitForElement(register.registerConfirmationElement);
//        assert (registrationSuccessful = true);
//
//        // 3.07	Re-enter the registration section.
//        homePage.waitForElement(homePage.registrationAnd24LinksHeader);
//        homePage.click(homePage.registerLink);
//
//        register.waitForElement(register.registerForm);
//    }
//
//    @Test
//    @Flaky
//    @DisplayName("Verifying that a user can login succesfully after registering")
//    public void loginWithRegisteredUser() throws InterruptedException {
//
//        TempMail tempMail = new TempMail(driver);
//        tempMail.fetchTempMailEmail();
//
//        HomePage homePage = new HomePage(driver);
//        homePage.fetch(EASTERN_DAILY_PRESS);
//        homePage.waitForElement(homePage.registrationAnd24LinksHeader);
//        homePage.click(homePage.registerLink);
//
//        Register register = new Register(driver);
//        register.waitForElement(register.registerForm);
//        register.fillInRegisterForm(tempMail.tempMailEmail);
//        register.click(register.registerButton);
//        try {
//            homePage.waitForElement(register.registerSuccessElement);
//            register.checkTextMatches(register.registerSuccessElement, "Thanks for registering");
//        } catch (NoSuchElementException e) {
//            System.out.println("Registration was not successfull");
//        }
//        tempMail.fetch(TEMPMAIL);
//        Thread.sleep(5000);
//        tempMail.scrollToElement(tempMail.emailSubjectLinkText);
//        tempMail.click(tempMail.emailSubjectLinkText);
//        Thread.sleep(2000);
//        tempMail.scrollToElement(tempMail.emailBody);
//        String textToParse = tempMail.getText(tempMail.emailBody);
//        tempMail.parseURL(textToParse);
//        tempMail.fetch(tempMail.tempMailUrl);
//
//        homePage.waitForElement(register.registerConfirmationElement);
//        assert (registrationSuccessful = true);
//
//        homePage.waitForElement(homePage.registrationAnd24LinksHeader);
//        homePage.click(homePage.loginLink);
//
//        Login login = new Login(driver);
//        login.waitForElement(login.loginForm);
//        login.fillInForm(tempMail.tempMailEmail, login.password);
//        login.click(login.loginButton);
//
//        // 4.02	Click the logout link.
//        try {
//            homePage.click(homePage.logoutLink);
//        } catch (NoSuchElementException e) {
//            System.out.println("Log out link was not visible as expected");
//        }
//    }
}