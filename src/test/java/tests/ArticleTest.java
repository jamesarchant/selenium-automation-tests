package tests;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Test;

import pageobjects.*;

import static org.testng.Assert.*;

public class ArticleTest extends BaseTests {

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Verify that an article loads correctly when clicked")
    public void verifyArticleLoads() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickRandomArticle();

        Article article = new Article(driver);
        assertTrue(article.checkHeaderIsDisplayed());
        assertTrue(article.checkLeadImageIsDisplayed());
        assertTrue(article.checkLeadImageCaptionIsDisplayed());
        assertTrue(article.checkSummaryIsDisplayed());
        assertTrue(article.checkBodyTextIsDisplayed());
    }

    @Test
    @DisplayName("Verify the presence of article titles")
    public void verifyArticleTitle() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkArticleTitle();
    }

    @Test
    @DisplayName("Verify the presence of article author names")
    public void verifyArticleAuthor() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkAuthorName();
    }

    @Test
    @DisplayName("Verify the presence of author email addresses")
    public void verifyAuthorEmail() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkAuthorEmail();
    }

    @Test
    @DisplayName("Verify the presence of article publication dates")
    public void verifyArticlePublicationDate() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkPublicationDate();
    }

    @Test
    @DisplayName("Verify the presence of article lead images")
    public void verifyLeadImage() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkLeadImage();
    }


    @Test
    @DisplayName("Verify the presence of article captions under the lead image")
    public void verifyLeadImageHasCaption() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkLeadImageCaption();
    }


    @Test
    @DisplayName("Verify the presence of article summaries")
    public void verifySummary() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkSummary();
    }

    @Test
    @DisplayName("Verify the presence of the first set of article social icons")
    public void verifyFirstSetOfSocialIcons() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkFirstSocialMediaIcons();
    }

    @Test
    @DisplayName("Verify the presence of additional article social icons")
    public void verifySecondSetOfSocialIcons() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkSecondSocialMediaIcons();
    }

    @Test
    @DisplayName("Verify the presence of article newsletter widgets")
    public void verifyNewsletterWidget() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkNewsletterWidget();
    }

    @Test
    @DisplayName("Verify the presence of article main body texts")
    public void verifyMainBody() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkMainBody();
    }

    @Test
    @DisplayName("Verify the presence additional article images")
    public void verifyAdditionalImagesAndCaptions() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkAdditionalImagesAndCaptions();
    }

    @Test
    @DisplayName("Verify the presence of additional article topic tags")
    public void verifyTopicTags() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkTopicTags();
    }

    @Test
    @DisplayName("Verify the presence of the article side widget")
    public void verifyArticleRightColumn() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkRightColumn();
    }

    @Test
    @DisplayName("Verify the presence of the article Disqus commenting boxes")
    public void verifyDisqus() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkDisqus();
    }

    @Test
    @DisplayName("Verify that article pages are responsive")
    public void verifyArticleResponsiveness() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkArticleResponsiveness();
    }

    @Test
    @DisplayName("Verify that the correct author page loads when clicked")
    public void clickAuthorQueueName() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkAuthorQueueName();
    }

    @Test
    @DisplayName("Confirm the presence of the author email address on the author page")
    public void clickAuthorQueueEmail() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkAuthorQueueEmailIsDisplayed();
    }

    @Test
    @DisplayName("Confirm the presence and click functionality of the author twitter handle on the author page")
    public void clickAuthorQueueTwitterLink() throws InterruptedException {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkAuthorQueueTwitterLinkFunctionality();
    }


    @Test
    @DisplayName("Verify the click functionality of the socials widget on the author page")
    public void clickAuthorSocials() throws InterruptedException {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkAuthorQueueSocialsFunctionality();
    }

    @Test
    @DisplayName("Confirm the presence of the author article queue on an author queue page")
    public void verifyAuthorQueuesVisible() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkAuthorQueueIsDisplayed();
    }


    @Test
    @DisplayName("Confirm that an article loads correctly when clicking an article image on an author page")
    public void verifyAuthorQueueImageClick() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickAuthorQueueImage();
    }

    @Test
    @DisplayName("Confirm that an article loads correctly when clicking an article title on an author page")
    public void verifyAuthorQueueTitleClick() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickAuthorQueueTitle();
    }

    @Test
    @DisplayName("Verify that an author page is responsive")
    public void verifyAuthorQueueResponsiveness() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.checkAuthorQueueResponsiveness();
    }
}