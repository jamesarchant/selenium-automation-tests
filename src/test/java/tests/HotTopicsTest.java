package tests;


import io.qameta.allure.junit4.DisplayName;

import org.junit.*;

import org.openqa.selenium.*;

import pageobjects.Article;
import pageobjects.HomePage;

import static org.testng.Assert.*;


public class HotTopicsTest extends BaseTests {

    @Test
    @DisplayName("Verify the presence of the hot topics bar")
    public void verifyHotTopics() {
        HomePage homePage = new HomePage(driver);
        homePage.checkHotTopicsHeaderIsDisplayed();
        assertEquals(homePage.getHotTopicsHeader(), "Hot Topics :");
    }

    @Test
    @DisplayName("Verify that the hot topics bar is responsive")
    public void verifyHotTopicsResponsiveness() {
        HomePage homePage = new HomePage(driver);
        homePage.checkHotTopicsHeaderIsDisplayed();
        assertEquals(homePage.getHotTopicsHeader(), "Hot Topics :");
        homePage.minimizeWindow();
        homePage.checkHotTopicsHeaderIsDisplayed();
    }

    @Test
    @DisplayName("Verify that each of the hot topic links load correctly when clicked")
    public void verifyHotTopicsClickFunctionality() {
        HomePage homePage = new HomePage(driver);
        homePage.checkHotTopicsHeaderIsDisplayed();
        assertEquals(homePage.getHotTopicsHeader(), "Hot Topics :");
        homePage.clickHotTopicLinks();
    }

    @Test
    @DisplayName("Verify the hot topics queues are responsive")
    public void verifyHotTopicsQueueResponsiveness() {
        HomePage homePage = new HomePage(driver);
        homePage.checkHotTopicsResponsiveness();
    }

    @Test
    @DisplayName("Verify the click functionality of the images in the hot topics queue")
    public void verifyHotTopicsClick_Header() {
        HomePage homePage = new HomePage(driver);
        homePage.clickHotTopicsHeader();
    }

    @Test
    @DisplayName("Verify the click functionality of the titles in the hot topics queue")
    public void verifyHotTopicsClick_Image() {
        HomePage homePage = new HomePage(driver);
        homePage.clickHotTopicsImage();
    }

    @Test
    @DisplayName("Verify the presence of topic tags")
    public void verifyTopicTagsAreVisible() {
        HomePage homePage = new HomePage(driver);
        homePage.waitForMainNews();
        homePage.clickRandomArticle();

        Article article = new Article(driver);
        assertTrue(article.checkHeaderIsDisplayed());
        assertTrue(article.checkLeadImageIsDisplayed());
        assertTrue(article.checkLeadImageCaptionIsDisplayed());
        assertTrue(article.checkSummaryIsDisplayed());
        assertTrue(article.checkBodyTextIsDisplayed());

        homePage.checkTopicTags();

    }

    @Test
    @DisplayName("Verify the click functionality of topic tags")
    public void verifyTopicTagsAreClickable() {
        Article article = new Article(driver);
        article.clickTopicTags();
    }
}