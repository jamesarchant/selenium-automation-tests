package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Allure;
import io.qameta.allure.Flaky;
import io.qameta.allure.junit4.DisplayName;
import org.apache.commons.io.FileUtils;
import org.junit.*;

import static pageobjects.BasePage.*;
import static org.junit.Assert.assertEquals;

import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pageobjects.CookiesPopup;
import pageobjects.HomePage;
import pageobjects.Search;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

public class SearchTest extends BaseTests {

    @Test
    @DisplayName("Verify that the search page appears when entering a search term")
    public void verifySearchFunctionality() {
        HomePage homePage = new HomePage(driver);
        homePage.chooseRandomSearchTerm();
        homePage.searchRequest();
    }

    @Test
    @DisplayName("Verify the contents of the search results page")
    public void verifyResults() {
        HomePage homePage = new HomePage(driver);
        homePage.chooseRandomSearchTerm();
        homePage.searchRequest();

        Search search = new Search(driver);
        //search.isDisplayed(search.resultsButton);
        //search.isDisplayed(search.refineYourSearchSection);
        //search.isDisplayed(search.sortByText);

        search.verifyPageNumbers();
        search.verifyCurrentPageHighlighted();
    }

    @Test
    @DisplayName("Verify the search results are in pages of 10")
    public void verifyResultsAreInPagesOf10() {
        HomePage homePage = new HomePage(driver);
        homePage.chooseRandomSearchTerm();
        homePage.searchRequest();

        Search search = new Search(driver);
        //assertEquals(search.searchResultList, 10);
    }

    @Test
    @DisplayName("Verify the article contents after being clicked from the search results page")
    public void verifyArticleContents() {
        HomePage homePage = new HomePage(driver);
        homePage.chooseRandomSearchTerm();
        homePage.searchRequest();

        Search search = new Search(driver);
        //search.checkSearchResultAsset(search.searchHeader);
        //search.checkSearchResultAsset(search.searchDate);
        //search.checkSearchResultAsset(search.searchImage);
        //search.checkSearchResultAsset(search.searchSummaryText);
    }

    @Test
    @DisplayName("Verify the responsiveness of the search results page")
    public void verifySearchResponsiveness() {
        HomePage homePage = new HomePage(driver);
        homePage.chooseRandomSearchTerm();
        homePage.searchRequest();

        Search search = new Search(driver);
        search.searchContents();
        search.minimizeWindow();
        search.searchContents();
    }

    @Test
    @DisplayName("Verify che click functionality of the date options of the search results page")
    public void clickDateOptions() {
        HomePage homePage = new HomePage(driver);
        homePage.chooseRandomSearchTerm();
        homePage.searchRequest();

        Search search = new Search(driver);
        search.clickDateFilters();
    }

    @Test
    @DisplayName("Confirm articles within the date ranges entered are matched succesfully")
    public void enterDateRanges() {
        HomePage homePage = new HomePage(driver);
        homePage.chooseRandomSearchTerm();
        homePage.searchRequest();

        Search search = new Search(driver);
        search.getDates30Days();
        //search.enterDate(search.filterFromDate, search.oneMonthAgoDate);
        search.getDates();
        //search.enterDate(search.filterToDate, search.todaysDate);
        search.clickSearchButton();
        search.checkDatesOnPage(search.thirtyDays);
    }

    @Test
    @DisplayName("Click the categories links on a search results page")
    public void clickCategories() {
        HomePage homePage = new HomePage(driver);
        homePage.chooseRandomSearchTerm();
        homePage.searchRequest();

        Search search = new Search(driver);
        search.clickCategoryOptions();
    }

    @Test
    @DisplayName("Click the sort by options on the search results page")
    public void clickSortByLinks() {
        HomePage homePage = new HomePage(driver);
        homePage.chooseRandomSearchTerm();
        homePage.searchRequest();

        Search search = new Search(driver);
        //search.clickMostRelevantLink(search.sortByMostRelevantLink);
        //search.waitForElement(search.resultsMainSection);
        //search.clickMostRelevantLink(search.sortByDateLink);
    }

    @Test
    @DisplayName("Click the page numbers on the search results page")
    public void clickPageNumbers() {
        HomePage homePage = new HomePage(driver);
        homePage.chooseRandomSearchTerm();
        homePage.searchRequest();

        Search search = new Search(driver);
        search.clickPageNumbers();
    }

    @Test
    @Flaky
    @DisplayName("Confirm that the previous bar on the search results page is disabled")
    public void verifyPreviousButtonDisabled() {
        HomePage homePage = new HomePage(driver);
        homePage.chooseRandomSearchTerm();
        homePage.searchRequest();

        Search search = new Search(driver);
        // TODO: this passes even if it's set to Enabled
        //search.isNotEnabled(search.previousLink);
    }

    @Test
    @DisplayName("Click the previous and next buttons on the search results page")
    public void clickPreviousNextButtons() {
        HomePage homePage = new HomePage(driver);
        homePage.chooseRandomSearchTerm();
        homePage.searchRequest();

        Search search = new Search(driver);
        //search.click(search.nextLink);
        //search.click(search.previousLink);
    }

    @Test
    @DisplayName("Click an article header on the search results page")
    public void clickArticleHeader() {
        HomePage homePage = new HomePage(driver);
        homePage.chooseRandomSearchTerm();
        homePage.searchRequest();
        homePage.scrollToElement(homePage.search_Header);
       //homePage.clickAssetInSearchResults(homePage.search_Header);
    }

    @Test
    @DisplayName("Click an article image on the search results page")
    public void clickArticleImage() {
        HomePage homePage = new HomePage(driver);
        homePage.chooseRandomSearchTerm();
        homePage.searchRequest();
        //homePage.clickAssetInSearchResults(homePage.search_Image);
    }

    @Test
    @DisplayName("Click an article summary on the search results page")
    public void clickArticleSummary() {
        HomePage homePage = new HomePage(driver);
        homePage.chooseRandomSearchTerm();
        homePage.searchRequest();
        //homePage.clickAssetInSearchResults(homePage.search_Summary);
    }

    @Test
    @DisplayName("Verify that an error message appears when searching for a term that does not exist")
    public void verifyMisspeltWord() {
        HomePage homePage = new HomePage(driver);
        homePage.invalidSearchRequest();
    }
}
